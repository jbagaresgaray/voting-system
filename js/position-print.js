$(document).ready(function() {
	var user = JSON.parse(window.localStorage['user'] || '{}');
    var config = JSON.parse(window.localStorage['config'] || '{}');

    if (Object.keys(user).length < 1) {
        console.log('redirect to main');
        window.location.href = "index.php";
    }


    $('#current_user').html(user.FullName+' ('+user.GroupName+')');


    Date.prototype.yyyymmdd = function() {
      var yyyy = this.getFullYear().toString();
      var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
      var dd  = this.getDate().toString();
      return yyyy + "/" + (mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]); // padding
    };

    d = new Date();
    $('#date_now').html(d.yyyymmdd());

    query_position(config.TermID);
    privilege(user);
});

function logout() {
    window.localStorage.clear();
    window.location.href = " index.php";
}

function query_position(term) {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $('#processing-modal').modal('show');
    $('#tbl_position tbody > tr').remove();
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'Print_Position',
            Term: term
        },
        success: function(response) {
            var decode = response;

            if(decode){
                if(decode.positions.length > 0){
                    for (var i = 0; i < decode.positions.length; i++) {
                        var row = decode.positions;

                        var html = '<tr class="odd">\
                                    <td>' + row[i].ElectionTerm + '</td>\
                                    <td>' + row[i].PositionCode + '</td>\
                                    <td>' + row[i].PositionName + '</td>\
                                    <td>' + row[i].ShortName + '</td>\
                                    <td>' + row[i].Allowed + '</td>\
                                    <td>' + row[i].AllowPerCourses + '</td>\
                                    <td>' + row[i].AllowPerCollege + '</td>\
                                    <td>' + row[i].AllowPerParty + '</td>\
                                    <td>' + row[i].Qualified + '</td>\
                                    <td>' + row[i].SortOrder + '</td>\
                                    <td>' + row[i].VoteForAll + '</td>\
                                    <td>' + row[i].VoteForCollege + '</td>\
                                    <td>' + row[i].VoteForCourses + '</td>\
                                    </tr>';
                        $("#tbl_position tbody").append(html);
                    }
                }
            }
            $('#processing-modal').modal('hide');
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            $('#processing-modal').modal('hide');
            return;
        }
    });
}

function privilege(user) {
    if (user._Dashboard == 1) {
        $('#LIST li').eq(0).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(1).removeClass('hidden');
    }

    // if(user._College == 1){
    //     $('#LIST li').eq(2).removeClass('hidden');
    // }

    if (user._Student == 1) {
        $('#LIST li').eq(2).removeClass('hidden');
    }

    if (user._PartyList == 1) {
        $('#LIST li').eq(3).removeClass('hidden');
    }

    if (user._Candidates == 1) {
        $('#LIST li').eq(4).removeClass('hidden');
    }

    if (user._ElectoralPosition == 1) {
        $('#LIST li').eq(5).removeClass('hidden');
    }

    if (user._AcademicProgram == 1) {
        $('#LIST li').eq(6).removeClass('hidden');
    }

    if (user._ElectionConfig == 1) {
        $('#LIST li').eq(7).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(8).removeClass('hidden');
    }
}

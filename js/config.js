$(document).ready(function() {
    var user = JSON.parse(window.localStorage['user'] || '{}');

    if (Object.keys(user).length < 1) {
        console.log('redirect to main');
        window.location.href = "index.php";
    }


    $('#current_user').html(user.FullName+' ('+user.GroupName+')');



    privilege(user);

    fetch_all_config('1');
});

$('#txtSearch').keyup(function(){
    console.log('fetching ...');
    search_configTyping();
});

function refresh() {
    fetch_all_config('1');
}

//Reset  Help-inline
function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

function logout() {
    window.localStorage.clear();
    window.location.href = "index.php";
}


function validate() {
    //Rest Help-inline
    resetHelpInLine();

    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;

    if ($('#txtElectionName').val() == '') {
        $('#txtElectionName').next('span').text('Election Name is required.');
        empty = true;
    }

    if ($('#txtSY').val() == '') {
        $('#txtSY2').next('span').text('School Year is required.');
        empty = true;
    }

    if ($('#txtSY2').val() == '') {
        $('#txtSY2').next('span').text('School Year is required.');
        empty = true;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    return true;
}

function save() {
    $('#btn-save').button('loading');
    var user = JSON.parse(window.localStorage['user'] || '{}');
    var TermID = $('#txtTermID').val();

    if (TermID == '') {
        if (validate() == true) {

            var data = new Array();

            config = new Object();
            config.ElectionName = $('#txtElectionName').val();
            config.SchoolYear = $('#txtSY').val() + '-' + $('#txtSY2').val();
            config.ElectionDate = $('#dtpElectionDate').val();
            config.ElectionStart = $('#dtpTimeStart').val();
            config.ElectionEnd = $('#dtpTimeEnd').val();
            config.InActive = '0';
            config.CreatedBy = user.FullName;

            data.push(config);

            var ipaddress = sessionStorage.getItem("ipaddress");

            $.ajax({
                url: 'http://' + ipaddress + '/voting/API/index.php',
                async: true,
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: {
                    command: 'insert_config',
                    data: data
                },
                success: function(response) {

                    var decode = response;

                    if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "config-list.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
                },
                error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
            });


        } else {
            $('#btn-save').button('reset');
        }
    } else {
        if (validate() == true) {

            var data = new Array();

            config = new Object();
            config.ElectionName = $('#txtElectionName').val();
            config.SchoolYear = $('#txtSY').val() + '-' + $('#txtSY2').val();
            config.ElectionDate = $('#dtpElectionDate').val();
            config.ElectionStart = $('#dtpTimeStart').val();
            config.ElectionEnd = $('#dtpTimeEnd').val();
            config.InActive = '0';
            config.CreatedBy = user.FullName;

            data.push(config);

            var ipaddress = sessionStorage.getItem("ipaddress");

            $.ajax({
                url: 'http://' + ipaddress + '/voting/API/index.php',
                async: true,
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: {
                    command: 'update_config',
                    TermID: TermID,
                    data: data
                },
                success: function(response) {

                    var decode = response;

                    if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "config-list.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
                },
                error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
            });


        } else {
            $('#btn-save').button('reset');
        }
    }


}

$(document).on("click", ".edit-config-icon", function() {

    var ID = $(this).data('id');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'get_config',
            TermID: ID
        },
        success: function(response) {
            var decode = response;

            if (decode.success == true) {
                $('#txtTermID').val(decode.config.TermID);
                $('#txtElectionName').val(decode.config.ElectionName);
                var dataToSplit = decode.config.SchoolYear;
                var i = dataToSplit.slice(0, 10).split('-');
                $('#txtSY').val(i[0]);
                $('#txtSY2').val(i[1]);

                var dataToSplit = decode.config.ElectionDate;
                if (dataToSplit != null) {
                    var i = dataToSplit.slice(0, 10).split('-');
                    var date = i[0] + "-" + i[1] + "-" + i[2];
                    $('#dtpElectionDate').val(date);
                }


                $('#dtpTimeStart').val(decode.config.TimeStart);
                $('#dtpTimeEnd').val(decode.config.TimeEnd);

                $('#myModal').modal('show')
            } else if (decode.success === false) {
                $('#myModal').modal('hide')
                alert(decode.msg);
                return;
            }

        }
    });
});


// on delete icon click
$(document).on("click", ".remove-config-icon", function() {
    if (confirm('Are you sure to delete this Election Configuration?')) {

        var ID = $(this).data('id');

        var ipaddress = sessionStorage.getItem("ipaddress");

        $.ajax({
            url: 'http://' + ipaddress + '/voting/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'delete_config',
                TermID: ID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "config-list.php";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});

// Activate Election Configuration
$(document).on("click", ".active-config-icon", function() {
    if (confirm('Activate this Election Configuration?')) {

        var ID = $(this).data('id');

        var ipaddress = sessionStorage.getItem("ipaddress");

        $.ajax({
            url: 'http://' + ipaddress + '/voting/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'activate_configuration',
                TermID: ID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    alert(decode.msg);
                    window.location.href = "config-list.php";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});

// SEARCH Configuration
function search_config() {

    if ($('#txtSearch').val() == '') {
        alert('Please input item to search fields correctly.');
        return false;
    }

    var value = $('#txtSearch').val();
    var ipaddress = sessionStorage.getItem("ipaddress");

    $('#processing-modal').modal('show');
    $('#tbl_config tbody > tr').remove();

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'search_config',
            value: value,
            page: '1'
        },
        success: function(response) {
            var decode = response;

            if(decode){
                if(decode.configs.length > 0){
                    for (var i = 0; i < decode.configs.length; i++) {
                        var row = decode.configs;
                        var html = '<tr class="odd">\
                                    <td>' + row[i].ElectionName + '</td>\
                                    <td>' + row[i].SchoolYear + '</td>\
                                    <td>' + row[i].ElectionDate + '</td>\
                                    <td>' + row[i].TimeStart + '</td>\
                                    <td>' + row[i].TimeEnd + '</td>\
                                    <td>\
                                      <div class="text-right">\
                                        <a class="active-config-icon btn btn-primary btn-xs" data-toggle="tooltip" title="Activate As Active Election Configuration" data-id="' + row[i].TermID + '">\
                                          <i class="icon-check"></i>\
                                        </a>\
                                        <a class="edit-config-icon btn btn-success btn-xs" data-toggle="tooltip" title="Modify Configuration" data-id="' + row[i].TermID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-config-icon btn btn-danger btn-xs" data-toggle="tooltip" title="Delete Configuration" data-id="' + row[i].TermID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_config tbody").append(html);
                    }
                    $('#pagination').html(decode.pagination);
                    $("#tbl_config").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }
            $('#processing-modal').modal('hide');
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            $('#processing-modal').modal('hide');
            return;
        }
    });
}

function search_configTyping() {
    var value = $('#txtSearch').val();
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'search_config',
            value: value,
            page: '1'
        },
        success: function(response) {
            var decode = response;
            $('#tbl_config tbody > tr').remove();
            for (var i = 0; i < decode.configs.length; i++) {
                var row = decode.configs;
                var html = '<tr class="odd">\
                            <td>' + row[i].ElectionName + '</td>\
                            <td>' + row[i].SchoolYear + '</td>\
                            <td>' + row[i].ElectionDate + '</td>\
                            <td>' + row[i].TimeStart + '</td>\
                            <td>' + row[i].TimeEnd + '</td>\
                            <td>\
                              <div class="text-right">\
                                <a class="active-config-icon btn btn-primary btn-xs" data-toggle="tooltip" title="Activate As Active Election Configuration" data-id="' + row[i].TermID + '">\
                                  <i class="icon-check"></i>\
                                </a>\
                                <a class="edit-config-icon btn btn-success btn-xs" data-toggle="tooltip" title="Modify Configuration" data-id="' + row[i].TermID + '">\
                                  <i class="icon-pencil"></i>\
                                </a>\
                                <a class="remove-config-icon btn btn-danger btn-xs" data-toggle="tooltip" title="Delete Configuration" data-id="' + row[i].TermID + '">\
                                  <i class="icon-remove"></i>\
                                </a>\
                              </div>\
                            </td>\
                        </tr>';
                $("#tbl_config tbody").append(html);
            }
            $("#tbl_config").addClass('tablesorter');

            var resort = true;
            $("table").trigger("update", [resort]);
        }
    });
}


$('#pagination').on('click', '.page-numbers', function() {
    var page = $(this).attr('data-id');

    fetch_all_config(page);
});

function fetch_all_config(page) {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $('#processing-modal').modal('show');
    $('#tbl_config tbody > tr').remove();

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select_all_config',
            page: page
        },
        success: function(response) {
            var decode = response;
            if (decode) {
                if (decode.configs.length > 0) {
                    for (var i = 0; i < decode.configs.length; i++) {
                        var row = decode.configs;
                        var html = '<tr class="odd">\
                                    <td>' + row[i].ElectionName + '</td>\
                                    <td>' + row[i].SchoolYear + '</td>\
                                    <td>' + row[i].ElectionDate + '</td>\
                                    <td>' + row[i].TimeStart + '</td>\
                                    <td>' + row[i].TimeEnd + '</td>\
                                    <td>\
                                      <div class="text-right">\
                                        <a class="active-config-icon btn btn-primary btn-xs" data-toggle="tooltip" title="Activate As Active Election Configuration" data-id="' + row[i].TermID + '">\
                                          <i class="icon-check"></i>\
                                        </a>\
                                        <a class="edit-config-icon btn btn-success btn-xs" data-toggle="tooltip" title="Modify Configuration" data-id="' + row[i].TermID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-config-icon btn btn-danger btn-xs" data-toggle="tooltip" title="Delete Configuration" data-id="' + row[i].TermID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_config tbody").append(html);
                    }
                    $('#pagination').html(decode.pagination);
                    $("#tbl_config").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }
            $('#processing-modal').modal('hide');

        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            $('#processing-modal').modal('hide');
            return;
        }
    });
}

function commandToClear() {
    $('#txtTermID').val('');
    $('#txtElectionName').val('');
    $('#txtSY').val('');
    $('#txtSY2').val('');
    $('#dtpElectionDate').val('mm/dd/yyy');
    $('#dtpTimeStart').val('');
    $('#dtpTimeEnd').val('');
}

function privilege(user) {
    if (user._Dashboard == 1) {
        $('#LIST li').eq(0).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(1).removeClass('hidden');
    }

    // if(user._College == 1){
    //     $('#LIST li').eq(2).removeClass('hidden');
    // }

    if (user._Student == 1) {
        $('#LIST li').eq(2).removeClass('hidden');
    }

    if (user._PartyList == 1) {
        $('#LIST li').eq(3).removeClass('hidden');
    }

    if (user._Candidates == 1) {
        $('#LIST li').eq(4).removeClass('hidden');
    }

    if (user._ElectoralPosition == 1) {
        $('#LIST li').eq(5).removeClass('hidden');
    }

    if (user._AcademicProgram == 1) {
        $('#LIST li').eq(6).removeClass('hidden');
    }

    if (user._ElectionConfig == 1) {
        $('#LIST li').eq(7).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(8).removeClass('hidden');
    }
}

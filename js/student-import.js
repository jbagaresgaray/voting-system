$(document).ready(function() {
  $('#CSVTable tbody > tr').remove();

  if(isAPIAvailable()) {
    $('#file_import').on('change', handleFileSelect);
  }

  select2_config();
});

function isAPIAvailable() {
    // Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
      // Great success! All the File APIs are supported.
      return true;
    } else {
      // source: File API availability - http://caniuse.com/#feat=fileapi
      // source: <output> availability - http://html5doctor.com/the-output-element/
      document.writeln('The HTML5 APIs used in this form are only available in the following browsers:<br />');
      // 6.0 File API & 13.0 <output>
      document.writeln(' - Google Chrome: 13.0 or later<br />');
      // 3.6 File API & 6.0 <output>
      document.writeln(' - Mozilla Firefox: 6.0 or later<br />');
      // 10.0 File API & 10.0 <output>
      document.writeln(' - Internet Explorer: Not supported (partial support expected in 10.0)<br />');
      // ? File API & 5.1 <output>
      document.writeln(' - Safari: Not supported<br />');
      // ? File API & 9.2 <output>
      document.writeln(' - Opera: Not supported');
      return false;
    }
}

function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
    var file = files[0];
    $('#list').empty();
    // read the file metadata
    var output = ''
        output += '<span style="font-weight:bold;">' + escape(file.name) + '</span><br />\n';
        output += ' - FileType: ' + (file.type || 'n/a') + '<br />\n';
        output += ' - FileSize: ' + file.size + ' bytes<br />\n';
        output += ' - LastModified: ' + (file.lastModifiedDate ? file.lastModifiedDate.toLocaleDateString() : 'n/a') + '<br />\n';

    // read the file contents
    printTable(file);

    // post the results
    $('#list').append(output);
}

function printTable(file) {
    var reader = new FileReader();
    reader.readAsText(file);
    $('#CSVTable').html('');
    reader.onload = function(event){
      var csv = event.target.result;
      var data = $.csv.toArrays(csv);
      var html = '';
      for(var row in data) {
          html += '<tr>\r\n';
          for(var item in data[row]) {
            html += '<td>' + data[row][item] + '</td>\r\n';
          }
          html += '</tr>\r\n';
      }
      $('#CSVTable').html(html);
    };
    reader.onerror = function(){ alert('Unable to read ' + file.fileName); };
}

function select2_config() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select2_config'
        },
        success: function(response) {
            var decode = response;
            $("#cboElectionTerm").empty();
            for (var i = 0; i < decode.configs.length; i++) {
                var row = decode.configs;
                var html = '<option id="' + row[i].TermID + '" value="' + row[i].TermID + '">' + row[i].Config + '</option>';
                $("#cboElectionTerm").append(html);
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function import_file(){
  if ($('#cboElectionTerm').val() == null) {
      alert('WARNING: Unable to import student data. Election Term is required.');
      return;
  }

  $('#processing-modal').modal('show');

  var data = new Array();

   $('#CSVTable tbody tr').not(':first').each(function() {
     var $row = $(this).children('td');
      student = new Object();
      student.Student_ID = $row.eq(0).text();
      student.Student_FirstName = $row.eq(1).text();
      student.Student_LastName = $row.eq(2).text();
      student.Student_MiddleInitial = $row.eq(3).text();
      student.Student_Gender =$row.eq(4).text();
      student.Student_ProgID =$row.eq(5).text();
      student.Student_TermID = $('#cboElectionTerm').val();
      data.push(student);
  });

  console.log(data);

  var ipaddress = sessionStorage.getItem("ipaddress");
  $.post('http://'+ipaddress+'/voting/API/index.php', {
      command: 'import_student',
      data: data
  }, function(response) {
      var decode = response;
      if (decode.success == true) {
          alert(decode.msg);
          $('#processing-modal').modal('hide');
          window.location.href="student-list.php";
      }
  });
}

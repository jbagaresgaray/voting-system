$(document).ready(function() {
    var user = JSON.parse(window.localStorage['user'] || '{}');
    if (Object.keys(user).length < 1) {
        console.log('redirect to main');
        window.location.href = "index.php";
    }

    $('#current_user').html(user.FullName+' ('+user.GroupName+')');




    privilege(user);

    fetch_all_college('1');
});

function refresh() {
    fetch_all_college('1');
}

function logout() {
    window.localStorage.clear();
    window.location.href = " index.php";
}

//Reset  Help-inline
function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}


function validate() {
    //Rest Help-inline
    resetHelpInLine();

    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;

    if ($('#txtCollegeCode').val() == '') {
        $('#txtCollegeCode').next('span').text('College Code is required.');
        empty = true;
    }

    if ($('#txtCollegeName').val() == '') {
        $('#txtCollegeName').next('span').text('College Name is required.');
        empty = true;
    }

    if ($('#txtCollegeDean').val() == '') {
        $('#txtCollegeDean').next('span').text('College Dean is required.');
        empty = true;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    return true;
}

function save() {
    $('#btn-save').button('loading');
    var college_id = $('#txtCollegeID').val();

    if (college_id == '') {

        if (validate() == true) {

            var data = new Array();


            college = new Object();
            college.college_name = $('#txtCollegeName').val();
            college.college_code = $('#txtCollegeCode').val();
            college.college_dean = $('#txtCollegeDean').val();
            college.inactive = $('#chkInActive').val();

            data.push(college);

            $.ajax({
                url: 'http://localhost/voting/API/index.php',
                async: true,
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: {
                    command: 'insert_college',
                    data: data

                },
                success: function(response) {

                    var decode = response;

                    if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "college-list.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.error);
                        return;
                    }
                },
                error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
            });


        } else {
            $('#btn-save').button('reset');
        }
    } else {
        if (validate() == true) {

            var data = new Array();


            college = new Object();
            college.college_name = $('#txtCollegeName').val();
            college.college_code = $('#txtCollegeCode').val();
            college.college_dean = $('#txtCollegeDean').val();
            college.inactive = $('#chkInActive').val();

            data.push(college);
            $.ajax({
                url: 'http://localhost/voting/API/index.php',
                async: true,
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: {
                    command: 'update_college',
                    college_id: college_id,
                    data: data
                },
                success: function(response) {

                    var decode = response;

                    if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "college-list.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.error);
                        return;
                    }
                },
                error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
            });


        } else {
            $('#btn-save').button('reset');
        }
    }
}

$(document).on("click", ".edit-college-icon", function() {

    var college_id = $(this).data('id');

    $.ajax({
        url: 'http://localhost/voting/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'get_college',
            college_id: college_id
        },
        success: function(response) {
            var decode = response;

            if (decode.success == true) {
                $('#txtCollegeName').val(decode.college.CollegeName);
                $('#txtCollegeCode').val(decode.college.CollegeCode);
                $('#txtCollegeDean').val(decode.college.CollegeDean);
                $('#txtCollegeID').val(decode.college.CollegeID);
                $('#chkInActive').val(decode.college.InActive);

                $('#myModal').modal('show')
            } else if (decode.success === false) {
                $('#myModal').modal('hide')
                alert(decode.msg);
                return;
            }

        }
    });
});


// on delete icon click
$(document).on("click", ".remove-college-icon", function() {
    if (confirm('Are you sure to delete this College?')) {

        var college_id = $(this).data('id');

        $.ajax({
            url: 'http://localhost/voting/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'delete_college',
                college_id: college_id
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "college-list.php";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});


$('#pagination').on('click', '.page-numbers', function() {
    var page = $(this).attr('data-id');

    fetch_all_college(page);
});

function fetch_all_college(page) {
    $('#processing-modal').modal('show');
    $('#tbl_college tbody > tr').remove();
    $.ajax({
        url: 'http://localhost/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select_all_college',
            page: page
        },
        success: function(response) {
            var decode = response;

            if (decode) {
                if (decode.colleges.length > 0) {
                    for (var i = 0; i < decode.colleges.length; i++) {
                        var row = decode.colleges;
                        var status;

                        if (row[i].InActive == 0) {
                            status = 'Active';
                        } else {
                            status = 'InActive';
                        }


                        var html = '<tr class="odd">\
                                    <td>' + row[i].CollegeName + '</td>\
                                    <td>' + row[i].CollegeCode + '</td>\
                                    <td>' + row[i].CollegeDean + '</td>\
                                    <td>' + status + '</td>\
                                    <td class=" ">\
                                      <div class="text-right">\
                                        <a class="edit-college-icon btn btn-success btn-xs" data-id="' + row[i].CollegeID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-college-icon btn btn-danger btn-xs" data-id="' + row[i].CollegeID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_college tbody").append(html);
                    }
                    $('#pagination').html(decode.pagination);
                    $("#tbl_student").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }
            $('#processing-modal').modal('hide');

        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            $('#processing-modal').modal('hide');
            return;
        }
    });
}


function search_college() {
    var value = $('#searchValue').val();
    $.ajax({
        url: 'http://localhost/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'search_college',
            value: value
        },
        success: function(response) {
            var decode = response;
            $('#tbl_college tbody > tr').remove();
            for (var i = 0; i < decode.colleges.length; i++) {
                var row = decode.colleges;

                var status;

                if (row[i].InActive == 0) {
                    status = 'Active';
                } else {
                    status = 'InActive';
                }

                var html = '<tr class="odd">\
                            <td>' + row[i].CollegeName + '</td>\
                            <td>' + row[i].CollegeCode + '</td>\
                            <td>' + row[i].CollegeDean + '</td>\
                            <td>' + status + '</td>\
                            <td class=" ">\
                              <div class="text-right">\
                                <a class="edit-college-icon btn btn-success btn-xs" data-id="' + row[i].CollegeID + '">\
                                  <i class="icon-pencil"></i>\
                                </a>\
                                <a class="remove-college-icon btn btn-danger btn-xs" data-id="' + row[i].CollegeID + '">\
                                  <i class="icon-remove"></i>\
                                </a>\
                              </div>\
                            </td>\
                        </tr>';
                $("#tbl_college tbody").append(html);
            }
            $("#tbl_student").addClass('tablesorter');

            var resort = true;
            $("table").trigger("update", [resort]);
        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function commandToClear() {
    $('#txtCollegeCode').val('');
    $('#txtCollegeName').val('');
    $('#txtCollegeDean').val('');
}

function privilege(user) {
    if (user._Dashboard == 1) {
        $('#LIST li').eq(0).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(1).removeClass('hidden');
    }

    // if(user._College == 1){
    //     $('#LIST li').eq(2).removeClass('hidden');
    // }

    if (user._Student == 1) {
        $('#LIST li').eq(2).removeClass('hidden');
    }

    if (user._PartyList == 1) {
        $('#LIST li').eq(3).removeClass('hidden');
    }

    if (user._Candidates == 1) {
        $('#LIST li').eq(4).removeClass('hidden');
    }

    if (user._ElectoralPosition == 1) {
        $('#LIST li').eq(5).removeClass('hidden');
    }

    if (user._AcademicProgram == 1) {
        $('#LIST li').eq(6).removeClass('hidden');
    }

    if (user._ElectionConfig == 1) {
        $('#LIST li').eq(7).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(8).removeClass('hidden');
    }
}

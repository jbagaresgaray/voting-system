var selected_candidate = new Array();


$(document).ready(function() {
    $.mobile.loading("show");
    console.log('document ready');

    var user = JSON.parse(window.localStorage['voting_student'] || '{}');

    if (Object.keys(user).length < 1) {
        console.log('redirect to main');
        window.location.href = "index.php";
    }

    $('#current_user').html(user.Fullname);
    $('#current_user_course').html(user.ProgName);

    getCandidacyPosition();

    $('#candidate_list').empty();
    $('#my_ballot').empty();

    window.localStorage.removeItem("ballot");
    window.localStorage.removeItem("voting_candidates");

    $.mobile.loading("hide");
});


function logout() {

    window.localStorage.removeItem("ballot");
    window.localStorage.removeItem("voting_candidates");
    window.localStorage.removeItem("config");
    window.localStorage.removeItem("voting_positions");
    window.localStorage.removeItem("voting_student");

    window.location.href = "index.php";
}


function getCandidacyPosition() {

    $.mobile.loading("show");

    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getCandidacyPosition'
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                window.localStorage['voting_positions'] = JSON.stringify(decode.positions);
                var len = decode.positions.length;
                $('#lstPositions').empty();
                if (len > 0) {

                    for (var i = 0; i < len; i++) {
                        var data = decode.positions[i];
                        var row = '<li data-id="' + data.PositionID + '">\
                                <a href="#"><i class="fa fa-angle-double-right"></i> ' + data.PositionName + '</a></li>';
                        $('#lstPositions').append(row);
                    }
                    var $the_ul = $('#lstPositions');
                    if ($the_ul.hasClass('ui-listview')) {
                        $the_ul.listview('refresh');
                    } else {
                        $the_ul.trigger('create');
                    }
                }
                $.mobile.loading("hide");
            } else {
                $.mobile.loading("hide");
                alert(decode.msg);
                return false;
            }
        },
        error: function(error) {
            $.mobile.loading("hide");
            console.log("Error:");
            console.log(error);
            return false;
        }
    });
}

function getCandidatesByPositionID(positionID, TermID) {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getCandidatesByPositionID',
            positionID: positionID,
            TermID: TermID
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {

                window.localStorage['voting_candidates'] = JSON.stringify(decode.candidates);

                var len = decode.candidates.length;
                $('#candidate_list').empty();
                $('#candidate_list').html('');
                if (len > 0) {;
                    $.mobile.loading("show");

                    for (var i = 0; i < len; i++) {
                        var data = decode.candidates[i];
                        var img = data.Photo;
                        var new_img;

                        if (img === 'W0JJTkFSWV0=' || img === '') {
                            new_img = '../../img/photo.jpg';
                        } else {
                            new_img = 'data:image/x-xbitmap;base64,' + img;
                        }

                        var row = '<div class="ui-block-b">\
                                        <div class="jqm-block-content text-center">\
                                            <h3>' + data.CandidateName + '</h3>\
                                            <a class="placeholder ">\
                                                <img src="' + new_img + '">\
                                                <h4>' + data.Partylist + '</h4>\
                                                <input type="hidden" id="candidate_id" name="candidate_id" value="' + data.CandidateID + '">\
                                                <input type="hidden" id="candidate_name" name="candidate_name" value="' + data.CandidateName + '">\
                                            </a>\
                                        </div>\
                                    </div>';

                        $('#candidate_list').append(row).trigger('create');
                    }
                    $.mobile.loading("hide");
                }

            } else {
                alert(decode.msg);
                return false;
            }
        },
        error: function(error) {
            $.mobile.loading("hide");
            console.log("Error:");
            console.log(error);
            return false;
        }
    });
}

function getCandidatesByPositionIDByCollegeID(positionID, CollegeID, TermID) {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getCandidatesByPositionIDByCollegeID',
            positionID: positionID,
            TermID: TermID,
            CollegeID: CollegeID
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {

                window.localStorage['voting_candidates'] = JSON.stringify(decode.candidates);

                var len = decode.candidates.length;
                $('#candidate_list').empty();
                $('#candidate_list').html('');
                if (len > 0) {;
                    $.mobile.loading("show");

                    for (var i = 0; i < len; i++) {
                        var data = decode.candidates[i];
                        var img = data.Photo;
                        var new_img;

                        if (img === 'W0JJTkFSWV0=' || img === '') {
                            new_img = '../../img/photo.jpg';
                        } else {
                            new_img = 'data:image/x-xbitmap;base64,' + img;
                        }

                        var row = '<div class="ui-block-b">\
                                        <div class="jqm-block-content text-center">\
                                            <h3>' + data.CandidateName + '</h3>\
                                            <a class="placeholder ">\
                                                <img src="' + new_img + '">\
                                                <h4>' + data.Partylist + '</h4>\
                                                <input type="hidden" id="candidate_id" name="candidate_id" value="' + data.CandidateID + '">\
                                                <input type="hidden" id="candidate_name" name="candidate_name" value="' + data.CandidateName + '">\
                                            </a>\
                                        </div>\
                                    </div>';

                        $('#candidate_list').append(row).trigger('create');
                    }
                    $.mobile.loading("hide");
                }

            } else {
                alert(decode.msg);
                return false;
            }
        },
        error: function(error) {
            $.mobile.loading("hide");
            console.log("Error:");
            console.log(error);
            return false;
        }
    });
}

function getCandidatesByPositionIDByCourseID(positionID, CourseID, TermID) {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getCandidatesByPositionIDByCourseID',
            positionID: positionID,
            TermID: TermID,
            ProgramID: CourseID
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {

                window.localStorage['voting_candidates'] = JSON.stringify(decode.candidates);

                var len = decode.candidates.length;
                $('#candidate_list').empty();
                $('#candidate_list').html('');
                if (len > 0) {;
                    $.mobile.loading("show");

                    for (var i = 0; i < len; i++) {
                        var data = decode.candidates[i];
                        var img = data.Photo;
                        var new_img;

                        if (img === 'W0JJTkFSWV0=' || img === '') {
                            new_img = '../../img/photo.jpg';
                        } else {
                            new_img = 'data:image/x-xbitmap;base64,' + img;
                        }

                        var row = '<div class="ui-block-b">\
                                        <div class="jqm-block-content text-center">\
                                            <h3>' + data.CandidateName + '</h3>\
                                            <a class="placeholder ">\
                                                <img src="' + new_img + '">\
                                                <h4>' + data.Partylist + '</h4>\
                                                <input type="hidden" id="candidate_id" name="candidate_id" value="' + data.CandidateID + '">\
                                                <input type="hidden" id="candidate_name" name="candidate_name" value="' + data.CandidateName + '">\
                                            </a>\
                                        </div>\
                                    </div>';

                        $('#candidate_list').append(row).trigger('create');
                    }
                    $.mobile.loading("hide");
                }

            } else {
                alert(decode.msg);
                return false;
            }
        },
        error: function(error) {
            $.mobile.loading("hide");
            console.log("Error:");
            console.log(error);
            return false;
        }
    });
}


$(document).on("click", "#lstPositions li", function() {
    var config = JSON.parse(window.localStorage['config'] || '{}');
    var candidate = JSON.parse(window.localStorage['voting_candidates'] || '{}');
    var positions = JSON.parse(window.localStorage['voting_positions'] || '{}');
    var student = JSON.parse(window.localStorage['voting_student'] || '{}');

    $('#lstPositions li').removeClass('active');
    $(this).addClass("active");
    $('#lblPosition').html($(this).text());

    $('#candidate_list').empty();
    $('#candidate_list').html('');

    var position_info;
    for (var i = 0; i < positions.length; i++) {
        if (positions[i].PositionID === $(this).attr("data-id")) {
            position_info = positions[i];
        }
    }

    if (position_info.VoteForAll == '1') {
        getCandidatesByPositionID($(this).attr("data-id"), config.TermID);
    } else if (position_info.VoteForCollege == '1') {
        getCandidatesByPositionIDByCollegeID($(this).attr("data-id"), student.CollegeID, config.TermID);
    } else if (position_info.VoteForCourses == '1') {
        getCandidatesByPositionIDByCourseID($(this).attr("data-id"), student.ProgID, config.TermID);
    }

    $("#panel-right").panel("close");
});


$(document).on("click", ".placeholder", function() {

    console.log($(this).find('input[name="candidate_id"]').val());
    console.log($(this).find('input[name="candidate_name"]').val());

    var config = JSON.parse(window.localStorage['config'] || '{}');
    var student = JSON.parse(window.localStorage['voting_student'] || '{}');
    var ballot = JSON.parse(window.localStorage['ballot'] || '{}');
    var candidates = JSON.parse(window.localStorage['voting_candidates'] || '{}');
    var positions = JSON.parse(window.localStorage['voting_positions'] || '{}');

    var candidate_id = $(this).find('input[name="candidate_id"]').val();

    candidate = new Object();
    var candidate_info;
    var position_info;

    candidate.CandidateID = candidate_id;
    candidate.StudentID = student.StudentID;
    candidate.TermID = config.TermID;

    // GET Candidate Information on Localstorage
    for (var i = 0; i < candidates.length; i++) {
        if (candidates[i].CandidateID === candidate_id) {
            candidate_info = candidates[i];
        }
    }

    candidate.CandidateName = candidate_info.CandidateName;
    candidate.Position = candidate_info.Position;
    candidate.PositionID = candidate_info.PositionID;
    candidate.Photo = candidate_info.Photo;
    candidate.PartyID = candidate_info.PartyID;
    candidate.Partylist = candidate_info.Partylist;
    candidate.CollegeID = candidate_info.CollegeID;
    candidate.College = candidate_info.College;

    for (var i = 0; i < positions.length; i++) {
        if (positions[i].PositionID === candidate_info.PositionID) {
            position_info = positions[i];
        }
    }

    if (ballot.length > 0) {
        var vCounter = 0
        for (var i = 0; i < ballot.length; i++) {
            if (ballot[i]["CandidateID"] == candidate.CandidateID && ballot[i]["StudentID"] == candidate.StudentID && ballot[i]["TermID"] == candidate.TermID) {
                console.log('true');
                $('#error_message').html('Selected candidate(s) was already in your list. Please choose another candidate');
                // $('#errModal').popup('open');
                $('#errModal').popup("open", {
                    transition: "pop"
                });
                return;
            }

            if (ballot[i].PositionID === position_info.PositionID) {
                vCounter = vCounter + 1;
            }

        }

        if (vCounter >= position_info.Allowed) {
            $('#error_message').html('You are only allowed to vote ' + position_info.Allowed + ' candidate(s) for ' + position_info.PositionName);
            $('#errModal').popup("open", {
                transition: "pop"
            });
            return;
        }
    }



    selected_candidate.push(candidate);
    window.localStorage['ballot'] = JSON.stringify(selected_candidate);

    $('#selected_candidate').html($(this).find('input[name="candidate_name"]').val());

    // $('#popupBasic').popup("open", {
    //     transition: "pop"
    // });

    toast($(this).find('input[name="candidate_name"]').val() + ' is added to your ballot!..')

    get_my_ballot();

    $("#panel-left").panel("open");

});


function get_my_ballot() {
    var ballot = JSON.parse(window.localStorage['ballot'] || '{}');

    $('#my_ballot').empty();
    // $('#my_ballot2').empty();
    if (ballot.length > 0) {
        for (var i = 0; i < ballot.length; i++) {

            var img = ballot[i].Photo;
            var new_img;
            if (img === 'W0JJTkFSWV0=' || img === '') {
                new_img = '../img/photo.jpg';
            } else {
                new_img = 'data:image/x-xbitmap;base64,' + img;
            }

            var row = '<li class="selected-panel" data-icon="home">\
                        <div class="image">\
                             <img src="' + new_img + '" alt="" />\
                        </div>\
                        <div class="info">\
                            <span>' + ballot[i].CandidateName + '</span>\
                            <small>' + ballot[i].Position + ' / ' + ballot[i].Partylist + '</small>\
                        </div>\
                        <div class="state-link">\
                            <a href="#" data-id="' + ballot[i].CandidateID + '" class="remove_candidate">Remove <i class="fa fa-times"></i></a>\
                        </div>\
                    </li>';
            $('#my_ballot').append(row);
            // $('#my_ballot2').append(row);
        }
        var $the_ul = $('#my_ballot');
        if ($the_ul.hasClass('ui-listview')) {
            $the_ul.listview('refresh');
        } else {
            $the_ul.trigger('create');
        }

        var $the_ul = $('#my_ballot2');
        if ($the_ul.hasClass('ui-listview')) {
            $the_ul.listview('refresh');
        } else {
            $the_ul.trigger('create');
        }
    }
}

$(document).on("click", ".remove_candidate", function() {
    console.log($(this).attr("data-id"));

    var ballot = JSON.parse(window.localStorage['ballot'] || '{}');
    var candidate_id = $(this).attr("data-id");

    if (ballot.length > 0) {
        for (var i = 0; i < ballot.length; i++) {
            if (ballot[i].CandidateID === candidate_id) {
                ballot.splice(i, 1);
                window.localStorage['ballot'] = JSON.stringify(ballot);
            }
        }

        for (var i = 0; i < selected_candidate.length; i++) {
            if (selected_candidate[i].CandidateID === candidate_id) {
                selected_candidate.splice(i, 1);
            }
        }

    } else {
        console.log('removeItem');
        window.localStorage.removeItem("ballot");
    }
    console.log(ballot);


    get_my_ballot();
});

function clear_ballot() {
    window.localStorage.removeItem("ballot");
    selected_candidate.splice(0, selected_candidate.length);
    get_my_ballot();

    $('#errModal').popup('close');
}

function review_ballot() {
    $('#myModal').popup('close');
    $("#myModal").on({
        popupafterclose: function() {
            setTimeout(function() {
                $('#panel-left').panel('open');
            }, 100);
        }
    });
}


function submit_ballot1() {
    $('#myModal').popup('close');
    $("#myModal").on({
        popupafterclose: function() {
            $('#error_message').html('WARNING: You are not allowed to submit empty ballot. Please try again');
            setTimeout(function() {
                $("#errModal").popup("open", {
                    transition: "pop"
                })
            }, 100);
        }
    });
    return;
}

function submit_ballot() {
    var ballot = JSON.parse(window.localStorage['ballot'] || '{}');
    var config = JSON.parse(window.localStorage['config'] || '{}');
    var student = JSON.parse(window.localStorage['voting_student'] || '{}');

    var my_ballot = new Array();
    for (var i = 0; i < ballot.length; i++) {
        myballot = new Object();

        myballot.CandidateID = ballot[i].CandidateID;
        myballot.StudentID = ballot[i].StudentID;
        myballot.TermID = ballot[i].TermID;
        my_ballot.push(myballot);
    }

    $('#myModal').popup('close');
    if (Object.keys(ballot).length < 1) {
        // alert('WARNING: You are not allowed to submit empty ballot. Please try again');
        $("#myModal").on({
            popupafterclose: function() {
                $('#success_message').html('WARNING: You are not allowed to submit empty ballot. Please try again');
                setTimeout(function() {
                    $("#errModal").popup("open", {
                        transition: "pop"
                    })
                }, 100);
            }
        });
        return;
    }

    var ipaddress = sessionStorage.getItem("ipaddress");
    $.mobile.loading("show");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'Insert_To_Ballot',
            data: my_ballot
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {

                $.mobile.loading("hide");

                $('#success_message').html(decode.msg);

                $("#myModal").on({
                    popupafterclose: function() {
                        setTimeout(function() {
                            $("#successModal").popup("open")
                        }, 100);
                    }
                });
                /*$('#successModal').popup("open", {
                    transition: "pop"
                });*/
            } else {
                $.mobile.loading("hide");

                $('#error_message').html(decode.msg);
                /*$('#errModal').popup("open", {
                    transition: "pop"
                });*/
                $("#myModal").on({
                    popupafterclose: function() {
                        setTimeout(function() {
                            $("#errModal").popup("open")
                        }, 100);
                    }
                });
                return;
            }
        },
        error: function(error) {
            $.mobile.loading("hide");
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}

function success_vote() {
    console.log('redirect to main');
    $('#successModal').popup('close');
    logout();
}

function toast(message) {
    var $toast = $('<div class="ui-loader ui-overlay-shadow ui-body-e ui-corner-all"><h4>' + message + '</h4></div>');

    $toast.css({
        display: 'block',
        background: '#2a2a2a',
        opacity: 0.90,
        position: 'fixed',
        padding: '7px',
        'text-align': 'center',
        width: '270px',
        color: '#fff',
        left: ($(window).width() - 284) / 2,
        top: $(window).height() / 2 - 20
    });

    var removeToast = function() {
        $(this).remove();
    };

    $toast.click(removeToast);

    $toast.appendTo($.mobile.pageContainer).delay(1500);
    $toast.fadeOut(400, removeToast);
}

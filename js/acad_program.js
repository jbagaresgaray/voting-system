$(document).ready(function() {
    var user = JSON.parse(window.localStorage['user'] || '{}');

    if (Object.keys(user).length < 1) {
        console.log('redirect to main');
        window.location.href = "index.php";
    }

    $('#current_user').html(user.FullName+' ('+user.GroupName+')');

    privilege(user);

    fetch_all_acadPrograms('1');
    fetch_college();

});

$('#searchValue').keyup(function(){
    console.log('fetching ...');
    search_CourseTyping();
});

function refresh() {
    fetch_all_acadPrograms('1');
    fetch_college();
}

//Reset  Help-inline
function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

function logout() {
    window.localStorage.clear();
    window.location.href = "index.php";
}

function validate() {
    //Rest Help-inline
    resetHelpInLine();

    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;

    if ($('#txtProgramName').val() == '') {
        $('#txtProgramName').next('span').text('Program Name Code is required.');
        empty = true;
    }

    if ($('#txtShortName').val() == '') {
        $('#txtShortName').next('span').text('Short Name is required.');
        empty = true;
    }

    if ($('#txtProgramCode').val() == '') {
        $('#txtProgramCode').next('span').text('Program Code is required.');
        empty = true;
    }

    if ($('#chckCourse').val() == '') {
        $('#chckCourse').next('span').text('College is required.');
        empty = true;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    return true;
}

function save() {
    var Prog_ID = $('#txtProgID').val();

    if (Prog_ID == '') {
        if (validate() == true) {
            if(checkCourses($('#txtProgramName').val()) == true){
                alert('WARNING: Data already exist!');
            }else{
                var data = new Array();


                    course = new Object();
                    course.program_Name = $('#txtProgramName').val();
                    course.short_Name = $('#txtShortName').val();
                    course.program_Code = $('#txtProgramCode').val();
                    course.college_ID = $('#chckCourse').val();


                    data.push(course);

                    var ipaddress = sessionStorage.getItem("ipaddress");

                    $.ajax({
                        url: 'http://' + ipaddress + '/voting/API/index.php',
                        async: true,
                        type: 'POST',
                        crossDomain: true,
                        dataType: 'json',
                        data: {
                            command: 'insert_acad_programs',
                            data: data

                        },
                        success: function(response) {

                            var decode = response;

                            if (decode.success == true) {
                                $('#btn-save').button('reset');
                                window.location.href = "academic-program-list.php";
                            } else if (decode.success === false) {
                                $('#btn-save').button('reset');
                                alert(decode.error);
                                return;
                            }
                        },
                        error: function(error) {
                            console.log("Error:");
                            console.log(error.responseText);
                            console.log(error.message);
                            return;
                        }
                    });


                }
            }else {
                    $('#btn-save').button('reset');
                }

    } else {
        if (validate() == true) {

            var data = new Array();


            course = new Object();
            course.prog_ID = $('#txtProgID').val();
            course.program_Name = $('#txtProgramName').val();
            course.short_Name = $('#txtShortName').val();
            course.program_Code = $('#txtProgramCode').val();
            course.college_ID = $('#chckCourse').val();

            data.push(course);

            var ipaddress = sessionStorage.getItem("ipaddress");

            $.ajax({
                url: 'http://' + ipaddress + '/voting/API/index.php',
                async: true,
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: {
                    command: 'update_acad_programs',
                    ProgID: Prog_ID,
                    data: data
                },
                success: function(response) {

                    var decode = response;

                    if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "academic-program-list.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.error);
                        return;
                    }
                },
                error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
            });


        } else {
            $('#btn-save').button('reset');
        }
    }
}

function checkCourses(Progname){
    var ipaddress = sessionStorage.getItem("ipaddress");
    var value  =false;
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'checkCourses',
            ProgName: Progname
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                value = true;
            } else if (decode.success == false) {
                value = false;
            }
        }
    });

    return value;

/*    $.post('http://' + ipaddress + '/voting/API/index.php', {
        command: 'checkCourses',
        ProgName: Progname
    }, function(response) {

    });*/
}


$(document).on("click", ".edit-college-icon", function() {

    var Prog_ID = $(this).data('id');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'get_acad_programs',
            ProgID: Prog_ID
        },
        success: function(response) {
            var decode = response;

            if (decode.success == true) {
                $('#txtProgramName').val(decode.program.ProgName);
                $('#txtShortName').val(decode.program.ShortName);
                $('#txtProgramCode').val(decode.program.ProgCode);
                $('#txtProgID').val(decode.program.ProgID);
                $('#chckCourse').val(decode.program.CollegeID);

                $('#myModal').modal('show')
            } else if (decode.success === false) {
                $('#myModal').modal('hide')
                alert(decode.msg);
                return;
            }

        }
    });
});


// on delete icon click
$(document).on("click", ".remove-college-icon", function() {
    if (confirm('Are you sure to delete this Academic Program?')) {

        var Prog_ID = $(this).data('id');

        var ipaddress = sessionStorage.getItem("ipaddress");

        $.ajax({
            url: 'http://' + ipaddress + '/voting/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'delete_acad_programs',
                ProgID: Prog_ID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "academic-program-list.php";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});

$('#pagination').on('click', '.page-numbers', function() {
    var page = $(this).attr('data-id');

    fetch_all_acadPrograms(page);
});


function fetch_all_acadPrograms(page) {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $('#processing-modal').modal('show');
    $('#tbl_courses tbody > tr').remove();

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select_all_acad_programs',
            page: page
        },
        success: function(response) {
            var decode = response;

            if (decode) {
                if (decode.programs.length > 0) {
                    for (var i = 0; i < decode.programs.length; i++) {
                        var row = decode.programs;


                        var html = '<tr class="odd">\
                                    <td class=" sorting_1">' + row[i].ProgName + '</td>\
                                    <!-- <td class=" ">' + row[i].ShortName + '</td> --> \
                                    <td class=" ">' + row[i].ProgCode + '</td>\
                                    <!-- <td class=" ">' + row[i].CollegeName + '</td> -->\
                                    <td class=" ">\
                                      <div class="text-right">\
                                        <a class="edit-college-icon btn btn-success btn-xs" data-id="' + row[i].ProgID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-college-icon btn btn-danger btn-xs" data-id="' + row[i].ProgID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_courses tbody").append(html);
                    }
                    $('#pagination').html(decode.pagination);
                    $("#tbl_courses").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }
            $('#processing-modal').modal('hide');

        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            $('#processing-modal').modal('hide');
            return;
        }
    });
}

function search_Course() {
    var value = $('#searchValue').val();
    var ipaddress = sessionStorage.getItem("ipaddress");

    $('#processing-modal').modal('show');
    $('#tbl_courses tbody > tr').remove();

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'search_acad_programs',
            value: value,
            page: '1'
        },
        success: function(response) {
            var decode = response;
            if (decode) {
                if (decode.programs.length > 0) {
                    for (var i = 0; i < decode.programs.length; i++) {
                        var row = decode.programs;

                        var html = '<tr class="odd">\
                                    <td>' + row[i].ProgName + '</td>\
                                    <td>' + row[i].ShortName + '</td>\
                                    <td>' + row[i].ProgCode + '</td>\
                                    <!-- <td>' + row[i].CollegeName + '</td> -->\
                                    <td class=" ">\
                                      <div class="text-right">\
                                        <a class="edit-college-icon btn btn-success btn-xs" data-id="' + row[i].ProgID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-college-icon btn btn-danger btn-xs" data-id="' + row[i].ProgID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_courses tbody").append(html);
                    }
                    $('#pagination').html(decode.pagination);
                    $("#tbl_courses").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }
            $('#processing-modal').modal('hide');
        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            $('#processing-modal').modal('hide');
            return;
        }
    });
}

function search_CourseTyping() {
    var value = $('#searchValue').val();
    var ipaddress = sessionStorage.getItem("ipaddress");

    $('#tbl_courses tbody > tr').remove();

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'search_acad_programs',
            value: value,
            page: '1'
        },
        success: function(response) {
            var decode = response;
            if (decode) {
                if (decode.programs.length > 0) {
                    for (var i = 0; i < decode.programs.length; i++) {
                        var row = decode.programs;

                        var html = '<tr class="odd">\
                                    <td>' + row[i].ProgName + '</td>\
                                    <td>' + row[i].ShortName + '</td>\
                                    <td>' + row[i].ProgCode + '</td>\
                                    <!-- <td>' + row[i].CollegeName + '</td> -->\
                                    <td class=" ">\
                                      <div class="text-right">\
                                        <a class="edit-college-icon btn btn-success btn-xs" data-id="' + row[i].ProgID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-college-icon btn btn-danger btn-xs" data-id="' + row[i].ProgID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_courses tbody").append(html);
                    }
                    $('#pagination').html(decode.pagination);
                    $("#tbl_courses").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }
        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function fetch_college() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select_For_Combo'
        },
        success: function(response) {
            var decode = response;
            $('#chckCourse').empty();
            for (var i = 0; i < decode.colleges.length; i++) {
                var row = decode.colleges;


                var html = '<option id="' + row[i].CollegeID + '" value="' + row[i].CollegeID + '">' + row[i].CollegeName + '</option>';
                $("#chckCourse").append(html);
            }
        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function commandToClear() {
    $('#txtProgID').val('');
    $('#txtProgramName').val('');
    $('#txtShortName').val('');
    $('#txtProgramCode').val('');
    $('#chckCourse').val('');
}

function privilege(user) {
    if (user._Dashboard == 1) {
        $('#LIST li').eq(0).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(1).removeClass('hidden');
    }

    // if(user._College == 1){
    //     $('#LIST li').eq(2).removeClass('hidden');
    // }

    if (user._Student == 1) {
        $('#LIST li').eq(2).removeClass('hidden');
    }

    if (user._PartyList == 1) {
        $('#LIST li').eq(3).removeClass('hidden');
    }

    if (user._Candidates == 1) {
        $('#LIST li').eq(4).removeClass('hidden');
    }

    if (user._ElectoralPosition == 1) {
        $('#LIST li').eq(5).removeClass('hidden');
    }

    if (user._AcademicProgram == 1) {
        $('#LIST li').eq(6).removeClass('hidden');
    }

    if (user._ElectionConfig == 1) {
        $('#LIST li').eq(7).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(8).removeClass('hidden');
    }
}

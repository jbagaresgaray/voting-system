$(document).ready(function() {
    var user = JSON.parse(window.localStorage['user'] || '{}');

    if (Object.keys(user).length < 1) {
        console.log('redirect to main');
        window.location.href = "index.php";
    }


    $('#current_user').html(user.FullName+' ('+user.GroupName+')');


    privilege(user);

    fetch_all_position('1');
    select2_config();
});

$('#txtSearch').keyup(function(){
    console.log('fetching ...');
    search_positionTying();
});

function refresh() {
    fetch_all_position('1');
    select2_config();
}

//Reset  Help-inline
function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

function logout() {
    window.localStorage.clear();
    window.location.href = "index.php";
}


function validate() {
    //Rest Help-inline
    resetHelpInLine();

    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;

    if ($('#cboElectionTerm').val() == '') {
        $('#cboElectionTerm').next('span').text('Election Term is required.');
        empty = true;
    }

    if ($('#txtName').val() == '') {
        $('#txtName').next('span').text('Position Name is required.');
        empty = true;
    }

    if ($('#txtCode').val() == '') {
        $('#txtCode').next('span').text('Position Code is required.');
        empty = true;
    }

    if ($('#txtShortName').val() == '') {
        $('#txtShortName').next('span').text('Position Shortname is required.');
        empty = true;
    }

    if ($('#txtLimit').val() == '') {
        $('#txtLimit').next('span').text('Position Limit is required.');
        empty = true;
    }

    if ($('#txtRanking').val() == '') {
        $('#txtRanking').next('span').text('Position Ranking is required.');
        empty = true;
    }

    if ($('#txtPerBallot').val() == '') {
        $('#txtPerBallot').next('span').text('No. of candidates on this position allow per ballot is required.');
        empty = true;
    }

    if ($('#txtPerCourse').val() == '') {
        $('#txtPerCourse').next('span').text('Allow per Course to register on this position is required.');
        empty = true;
    }

    if ($('#txtPerCollege').val() == '') {
        $('#txtPerCollege').next('span').text('Allow per College to register on this position is required.');
        empty = true;
    }

    if ($('#txtPerParty').val() == '') {
        $('#txtPerParty').next('span').text('Allow per Partylist to register on this position is required.');
        empty = true;
    }

    if ($('input[name="chkVote"]').val() == '') {
        $('input[name="chkVote"]').next('span').text('Voter filtration is required.');
        empty = true;
    }


    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    return true;
}

function save() {
    $('#btn-save').button('loading');
    var position_id = $('#authenticity_token').val();

    if (position_id == '') {
        if (validate() == true) {

            var data = new Array();

            position = new Object();

            position.ElectionID = $('#cboElectionTerm').val();
            position.Position_Code = $('#txtPositionCode').val();
            position.Position_Name = $('#txtPosition').val();
            position.Short_Name = $('#txtShortName').val();
            position.Qualified = $('#txtLimit').val();
            position.SortOrder = $('#txtRanking').val();
            position.Allowed = $('#txtPerBallot').val();
            position.AllowPerCourse = $('#txtPerCourse').val();
            position.AllowPerCollege = $('#txtPerCollege').val();
            position.AllowPerParty = $('#txtPerParty').val();

            if ($('#chkVoteForAll').prop("checked") == true) {
                position.VoteForAll = '1';
            } else {
                position.VoteForAll = '0';
            }

            if ($('#chkVoteForCourse').prop("checked") == true) {
                position.VoteForCourse = '1';
            } else {
                position.VoteForCourse = '0';
            }

            if ($('#chkVoteForCollege').prop("checked") == true) {
                position.VoteForCollege = '1';
            } else {
                position.VoteForCollege = '0'
            }


            data.push(position);


            var ipaddress = sessionStorage.getItem("ipaddress");

            $.ajax({
                url: 'http://' + ipaddress + '/voting/API/index.php',
                async: true,
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: {
                    command: 'insert_position',
                    data: data
                },
                success: function(response) {

                    var decode = response;
                    if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "position-list.php"
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
                },
                error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
            });
        } else {
            $('#btn-save').button('reset');
        }
    } else {
        if (validate() == true) {

            var data = new Array();

            position = new Object();

            position.ElectionID = $('#cboElectionTerm').val();
            position.Position_Code = $('#txtPositionCode').val();
            position.Position_Name = $('#txtPosition').val();
            position.Short_Name = $('#txtShortName').val();
            position.Qualified = $('#txtLimit').val();
            position.SortOrder = $('#txtRanking').val();
            position.Allowed = $('#txtPerBallot').val();
            position.AllowPerCourse = $('#txtPerCourse').val();
            position.AllowPerCollege = $('#txtPerCollege').val();
            position.AllowPerParty = $('#txtPerParty').val();

            if ($('#chkVoteForAll').prop("checked") == true) {
                position.VoteForAll = '1';
            } else {
                position.VoteForAll = '0';
            }

            if ($('#chkVoteForCourse').prop("checked") == true) {
                position.VoteForCourse = '1';
            } else {
                position.VoteForCourse = '0';
            }

            if ($('#chkVoteForCollege').prop("checked") == true) {
                position.VoteForCollege = '1';
            } else {
                position.VoteForCollege = '0'
            }

            data.push(position);


            var ipaddress = sessionStorage.getItem("ipaddress");

            $.ajax({
                url: 'http://' + ipaddress + '/voting/API/index.php',
                async: true,
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: {
                    command: 'update_position',
                    position_id: position_id,
                    data: data
                },
                success: function(response) {

                    var decode = response;
                    if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "position-list.php"
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
                },
                error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
            });
        } else {
            $('#btn-save').button('reset');
        }
    }


}

$('#pagination').on('click', '.page-numbers', function() {
    var page = $(this).attr('data-id');

    fetch_all_position(page);
});

$(document).on("click", ".edit-Position-icon", function() {

    var position_id = $(this).data('id');

    $.ajax({
        url: 'http://localhost/voting/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'get_position',
            position_id: position_id
        },
        success: function(response) {
            var decode = response;

            if (decode.success == true) {

                $('#authenticity_token').val(decode.position.PositionID);
                $('#cboElectionTerm').val(decode.position.TermID);
                $('#txtPositionCode').val(decode.position.PositionCode);
                $('#txtPosition').val(decode.position.PositionName);
                $('#txtShortName').val(decode.position.ShortName);
                $('#txtLimit').val(decode.position.Qualified);
                $('#txtRanking').val(decode.position.SortOrder);
                $('#txtPerBallot').val(decode.position.Allowed);
                $('#txtPerCourse').val(decode.position.AllowPerCourses);
                $('#txtPerCollege').val(decode.position.AllowPerCollege);
                $('#txtPerParty').val(decode.position.AllowPerParty);

                if (decode.position.VoteForAll == 1) {
                    $('#chkVoteForAll').prop("checked", true);
                } else {
                    $('#chkVoteForAll').prop("checked", false);
                }

                if (decode.position.VoteForCollege == 1) {
                    $('#chkVoteForCourse').prop("checked", true);
                } else {
                    $('#chkVoteForCourse').prop("checked", false);
                }

                if (decode.position.VoteForCourses == 1) {
                    $('#chkVoteForCollege').prop("checked", true);
                } else {
                    $('#chkVoteForCollege').prop("checked", false);
                }

                $('#myModal').modal('show')
            } else if (decode.success === false) {
                $('#myModal').modal('hide')
                alert(decode.msg);
                return;
            }

        }
    });
});

// on delete icon click
$(document).on("click", ".remove-Position-icon", function() {
    if (confirm('Are you sure to delete this Position?')) {

        var position_id = $(this).data('id');

        $.ajax({
            url: 'http://localhost/voting/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'delete_position',
                position_id: position_id
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "position-list.php";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});


function fetch_all_position(page) {
    var config = JSON.parse(window.localStorage['config'] || '{}');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $('#processing-modal').modal('show');
    $('#tbl_position tbody > tr').remove();
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select_all_position',
            TermID: config.TermID,
            page: page
        },
        success: function(response) {
            var decode = response;

            if (decode) {
                if (decode.positions.length > 0) {
                    for (var i = 0; i < decode.positions.length; i++) {
                        var row = decode.positions;
                        var html = '<tr class="odd">\
                                    <td>' + row[i].ElectionTerm + '</td>\
                                    <td>' + row[i].PositionCode + '</td>\
                                    <td>' + row[i].PositionName + '</td>\
                                    <td>' + row[i].ShortName + '</td>\
                                    <td>' + row[i].Allowed + '</td>\
                                    <!-- <td>' + row[i].AllowPerCourses + '</td>\
                                    <td>' + row[i].AllowPerCollege + '</td>\
                                    <td>' + row[i].AllowPerParty + '</td> -->\
                                    <td>' + row[i].Qualified + '</td>\
                                    <td>' + row[i].SortOrder + '</td>\
                                    <td>' + row[i].VoteForAll + '</td>\
                                    <!-- <td>' + row[i].VoteForCollege + '</td> -->\
                                    <td>' + row[i].VoteForCourses + '</td>\
                                    <td class=" ">\
                                      <div class="text-right">\
                                        <a class="edit-Position-icon btn btn-success btn-xs" data-id="' + row[i].PositionID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-Position-icon btn btn-danger btn-xs" data-id="' + row[i].PositionID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_position tbody").append(html);
                    }
                    $('#pagination').html(decode.pagination);
                    $("#tbl_position").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }
            $('#processing-modal').modal('hide');

        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            $('#processing-modal').modal('hide');
            return;
        }
    });
}

function search_position() {

    if ($('#txtSearch').val() == '') {
        alert('Please input item to search fields correctly.');
        return false;
    }

    var config = JSON.parse(window.localStorage['config'] || '{}');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $('#processing-modal').modal('show');
    $('#tbl_position tbody > tr').remove();
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'search_position',
            TermID: config.TermID,
            value: $('#txtSearch').val(),
            page: '1'
        },
        success: function(response) {
            var decode = response;

            if (decode) {
                if (decode.positions.length > 0) {
                    for (var i = 0; i < decode.positions.length; i++) {
                        var row = decode.positions;
                        var html = '<tr class="odd">\
                                    <td>' + row[i].ElectionTerm + '</td>\
                                    <td>' + row[i].PositionCode + '</td>\
                                    <td>' + row[i].PositionName + '</td>\
                                    <td>' + row[i].ShortName + '</td>\
                                    <td>' + row[i].Allowed + '</td>\
                                    <!-- <td>' + row[i].AllowPerCourses + '</td>\
                                    <td>' + row[i].AllowPerCollege + '</td>\
                                    <td>' + row[i].AllowPerParty + '</td> -->\
                                    <td>' + row[i].Qualified + '</td>\
                                    <td>' + row[i].SortOrder + '</td>\
                                    <td>' + row[i].VoteForAll + '</td>\
                                    <!-- <td>' + row[i].VoteForCollege + '</td> -->\
                                    <td>' + row[i].VoteForCourses + '</td>\
                                    <td class=" ">\
                                      <div class="text-right">\
                                        <a class="edit-Position-icon btn btn-success btn-xs" data-id="' + row[i].PositionID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-Position-icon btn btn-danger btn-xs" data-id="' + row[i].PositionID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_position tbody").append(html);
                    }
                    $('#pagination').html(decode.pagination);
                    $("#tbl_position").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }
            $('#processing-modal').modal('hide');

        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            $('#processing-modal').modal('hide');
            return;
        }
    });
}

function search_positionTying() {
    var config = JSON.parse(window.localStorage['config'] || '{}');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $('#tbl_position tbody > tr').remove();
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'search_position',
            TermID: config.TermID,
            value: $('#txtSearch').val(),
            page: '1'
        },
        success: function(response) {
            var decode = response;

            if (decode) {
                if (decode.positions.length > 0) {
                    for (var i = 0; i < decode.positions.length; i++) {
                        var row = decode.positions;
                        var html = '<tr class="odd">\
                                    <td>' + row[i].ElectionTerm + '</td>\
                                    <td>' + row[i].PositionCode + '</td>\
                                    <td>' + row[i].PositionName + '</td>\
                                    <td>' + row[i].ShortName + '</td>\
                                    <td>' + row[i].Allowed + '</td>\
                                    <!-- <td>' + row[i].AllowPerCourses + '</td>\
                                    <td>' + row[i].AllowPerCollege + '</td>\
                                    <td>' + row[i].AllowPerParty + '</td> -->\
                                    <td>' + row[i].Qualified + '</td>\
                                    <td>' + row[i].SortOrder + '</td>\
                                    <td>' + row[i].VoteForAll + '</td>\
                                    <!-- <td>' + row[i].VoteForCollege + '</td> -->\
                                    <td>' + row[i].VoteForCourses + '</td>\
                                    <td class=" ">\
                                      <div class="text-right">\
                                        <a class="edit-Position-icon btn btn-success btn-xs" data-id="' + row[i].PositionID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-Position-icon btn btn-danger btn-xs" data-id="' + row[i].PositionID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_position tbody").append(html);
                    }
                    $('#pagination').html(decode.pagination);
                    $("#tbl_position").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }

        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function select2_config() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select_For_ComboConfig'
        },
        success: function(response) {
            var decode = response;
            $("#cboElectionTerm").empty();
            for (var i = 0; i < decode.configs.length; i++) {
                var row = decode.configs;
                var html = '<option id="' + row[i].TermID + '" value="' + row[i].TermID + '">' + row[i].Config + '</option>';
                $("#cboElectionTerm").append(html);
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function commandToClear() {
    $('#authenticity_token').val('');
    $('#cboElectionTerm').val('');
    $('#txtPositionCode').val('');
    $('#txtPosition').val('');
    $('#txtShortName').val('');
    $('#txtLimit').val('');
    $('#txtRanking').val('');
    $('#txtPerBallot').val('');
    $('#txtPerCourse').val('');
    $('#txtPerCollege').val('');
    $('#txtPerParty').val('');
    $('#chkVoteForAll').prop("checked", false);
    $('#chkVoteForCourse').prop("checked", false);
    $('#chkVoteForCollege').prop("checked", false);
}

function privilege(user) {
    if (user._Dashboard == 1) {
        $('#LIST li').eq(0).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(1).removeClass('hidden');
    }

    // if(user._College == 1){
    //     $('#LIST li').eq(2).removeClass('hidden');
    // }

    if (user._Student == 1) {
        $('#LIST li').eq(2).removeClass('hidden');
    }

    if (user._PartyList == 1) {
        $('#LIST li').eq(3).removeClass('hidden');
    }

    if (user._Candidates == 1) {
        $('#LIST li').eq(4).removeClass('hidden');
    }

    if (user._ElectoralPosition == 1) {
        $('#LIST li').eq(5).removeClass('hidden');
    }

    if (user._AcademicProgram == 1) {
        $('#LIST li').eq(6).removeClass('hidden');
    }

    if (user._ElectionConfig == 1) {
        $('#LIST li').eq(7).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(8).removeClass('hidden');
    }
}

$(document).ready(function() {
    var user = JSON.parse(window.localStorage['user'] || '{}');
    if (Object.keys(user).length < 1) {
        console.log('redirect to main');
        window.location.href = "index.php";
    }


    $('#current_user').html(user.FullName+' ('+user.GroupName+')');


    privilege(user);

    get_active_config();

    var config = JSON.parse(window.localStorage['config'] || '{}');

    $('#lblElectionTerm').html(config.SchoolYear + ' - ' + config.ElectionName);

    getStatisticsOverview();
    select2_config();
});

function logout() {
    window.localStorage.clear();
    window.location.href = "index.php";
}



function get_active_config() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'get_active_config'
        },
        success: function(response) {
            var decode = response;
            if (decode.success == true) {
                window.localStorage['config'] = JSON.stringify(decode.config);
            } else {
                alert(decode.msg);
                return;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}


function getStatisticsOverview() {
    var config = JSON.parse(window.localStorage['config'] || '{}');

    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getStatisticsOverview',
            TermID: config.TermID
        },
        success: function(response) {
            var decode = response;
            if (decode.success == true) {
                window.localStorage['totalVoters'] = decode.totalVoters;
                window.localStorage['totalNotVoted'] = decode.totalNotVoted;
                window.localStorage['totalVoted'] = decode.totalVoted;

                var totalVoted = (window.localStorage['totalVoted'] || '{}');
                var totalVoters = (window.localStorage['totalVoters'] || '{}');
                var totalNotVoted = (window.localStorage['totalNotVoted'] || '{}');

                $('#announcement-voted-heading').html(totalVoted);
                $('#announcement-voters-heading').html(totalVoters);
                $('#announcement-notvoted-heading').html(totalNotVoted);

            } else {
                alert(decode.msg);
                return;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}

function select2_config() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select2_config'
        },
        success: function(response) {
            var decode = response;
            $("#cboElectionTerm").empty();
            for (var i = 0; i < decode.configs.length; i++) {
                var row = decode.configs;
                var html = '<option id="' + row[i].TermID + '" value="' + row[i].TermID + '">' + row[i].Config + '</option>';
                $("#cboElectionTerm").append(html);
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function activate_term() {
    if (confirm('Activate this Election Configuration?')) {

        var ID = $('#cboElectionTerm').val();

        var ipaddress = sessionStorage.getItem("ipaddress");

        $.ajax({
            url: 'http://' + ipaddress + '/voting/API/index.php',
            async: false,
            type: 'POST',
            data: {
                command: 'activate_configuration',
                TermID: ID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    alert(decode.msg);
                    window.location.href = "main.php";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
}

function privilege(user) {

    if(user.GroupName == 'Administrator'){
        $('#Configure').removeClass('hidden');
    }else{
        $('#Configure').addClass('hidden');
    }

    if (user._Dashboard == 1) {
        $('#LIST li').eq(0).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(1).removeClass('hidden');
    }

    // if(user._College == 1){
    //     $('#LIST li').eq(2).removeClass('hidden');
    // }

    if (user._Student == 1) {
        $('#LIST li').eq(2).removeClass('hidden');
    }

    if (user._PartyList == 1) {
        $('#LIST li').eq(3).removeClass('hidden');
    }

    if (user._Candidates == 1) {
        $('#LIST li').eq(4).removeClass('hidden');
    }

    if (user._ElectoralPosition == 1) {
        $('#LIST li').eq(5).removeClass('hidden');
    }

    if (user._AcademicProgram == 1) {
        $('#LIST li').eq(6).removeClass('hidden');
    }

    if (user._ElectionConfig == 1) {
        $('#LIST li').eq(7).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(8).removeClass('hidden');
    }
}

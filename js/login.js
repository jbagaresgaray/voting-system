$(document).ready(function() {
    var user = JSON.parse(window.localStorage['student'] || '{}');

    if (Object.keys(user).length > 0) {
        console.log('redirect to main');
        window.location.href = "ballot.php";
    }

    $('#server').val(sessionStorage.getItem("ipaddress"));
});


function get_active_config() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    $.mobile.loading("show");
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'get_active_config'
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                window.localStorage['config'] = JSON.stringify(decode.config);
                $.mobile.loading("hide");
            } else {
                $.mobile.loading("hide");
                alert(decode.msg);
                return;
            }
        },
        error: function(error) {
            $.mobile.loading("hide");
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}

function validate() {
    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;

    if ($('#username').val() == '') {
        alert('Username is required.');
        empty = true;
        return false;
    }

    if ($('#password').val() == '') {
        alert('Password is required.');
        empty = true;
        return false;
    }

    if ($('#server').val() == '') {
        alert('IP Address is required.');
        empty = true;
        return false;
    }

    sessionStorage.setItem("ipaddress", $('#server').val());

    get_active_config();


    console.log('return true');
    return true;
}

function login() {
    if (validate() == true) {

        var config = JSON.parse(window.localStorage['config'] || '{}');

        var ipaddress = sessionStorage.getItem("ipaddress");

        $.mobile.loading("show");

        $.ajax({
            url: 'http://' + ipaddress + '/voting/API/index.php',
            async: true,
            type: 'POST',
            crossDomain: true,
            dataType: 'json',
            data: {
                command: 'checklogin',
                username: $('#username').val(),
                TermID: config.TermID
            },
            success: function(response) {
                var decode = response;
                console.log(decode);
                if (decode.success == true) {
                    $.mobile.loading("hide");
                    $('#error_message').html(decode.msg);
                    $('#errModal').popup('open');
                    return false;
                } else {

                    $.ajax({
                        url: 'http://' + ipaddress + '/voting/API/index.php',
                        async: false,
                        type: 'POST',
                        crossDomain: true,
                        dataType: 'json',
                        data: {
                            command: 'login',
                            username: $('#username').val(),
                            password: $('#password').val(),
                            TermID: config.TermID
                        },
                        success: function(response) {
                            var decode = response;
                            console.log(decode);
                            if (decode.success == true) {
                                $.mobile.loading("hide");

                                window.localStorage['voting_student'] = JSON.stringify(decode.student);
                                console.log('ballot.html');
                                window.location.href = "ballot.php";
                            } else {
                                $.mobile.loading("hide");
                                $('#error_message').html(decode.msg);
                                $('#errModal').popup('open');
                                return;
                            }
                        },
                        error: function(error) {
                            console.log("Error:");
                            console.log(error);
                            $.mobile.loading("hide");
                            $('#error_message').html(error);
                            $('#errModal').popup('open');
                            return;
                        }
                    });
                }
            },
            error: function(error) {
                $.mobile.loading("hide");
                console.log("Error:");
                console.log(error);
                empty = false;
                return empty;
            }
        });
    }
}


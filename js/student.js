$(document).ready(function() {
    var user = JSON.parse(window.localStorage['user'] || '{}');

    if (Object.keys(user).length < 1) {
        console.log('redirect to main');
        window.location.href = "index.php";
    }

    $('#current_user').html(user.FullName+' ('+user.GroupName+')');

    privilege(user);

    get_active_config();
    fetch_all_student('1');
    fetch_Term();
    fetch_AcadProg();
    // fetch_college();
});

$('#txtSearch').keyup(function() {
    console.log('fetching ...');
    search_studentTyping();
});

function refresh() {
    get_active_config();
    fetch_all_student('1');
    fetch_Term();
    fetch_AcadProg();
    fetch_college();
}

function logout() {
    window.localStorage.clear();
    window.location.href = "index.php";
}

//Reset  Help-inline
function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

$('#myModal').on('hidden.bs.modal', function (e) {
    resetHelpInLine();

    $('#authenticity_token').val('');
    $('#txtStudentID').val('');
    $('#txtPassword').val('');
    $('#txtLastName').val('');
    $('#txtFirstName').val('');
    $('#txtMiddleName').val('');
    $('#chkGender').val('Male');
    $('#chckProgID').val('');
    $('#chckCollege').val('');
    $('#chckTerm').val('');
})

function validate() {
    //Rest Help-inline
    resetHelpInLine();

    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;

    if ($('#txtStudentID').val() == '') {
        $('#txtStudentID').next('span').text('Student ID is required.');
        empty = true;
    }

    if ($('#txtPassword').val() == '') {
        $('#txtPassword').next('span').text('Password is required.');
        empty = true;
    }

    if ($('#txtFirstName').val() == '') {
        $('#txtFirstName').next('span').text('First Name is required.');
        empty = true;
    }

    if ($('#txtMiddleName').val() == '') {
        $('#txtMiddleName').next('span').text('Middle Name / Initial is required.');
        empty = true;
    }

    if ($('#txtLastName').val() == '') {
        $('#txtLastName').next('span').text('Last Name is required.');
        empty = true;
    }

    if ($('#chckProgID').val() == null) {
        $('#chckProgID').next('span').text('Course is required.');
        empty = true;
    }

    if ($('#txtMiddleName').val() == '') {
        $('#txtMiddleName').next('span').text('Middle Name / Initial is required.');
        empty = true;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }


    return true;
}

function save() {
    var student_id = $('#authenticity_token').val();

    if (student_id == '') {
        if (validate() == true) {

            if(checkStudent($('#txtFullname').val()) == true){
                alert('WARNING: Data already exist!');
            }else{
                var data = new Array();

                student = new Object();

                student.Student_ID = $('#txtStudentID').val();
                student.Student_Password = $('#txtPassword').val();
                student.Student_FirstName = $('#txtFirstName').val();
                student.Student_MiddleInitial = $('#txtMiddleName').val();
                student.Student_LastName = $('#txtLastName').val();
                student.Student_ProgID = $('#chckProgID').val();
                // student.Student_CollegeID = $('#chckCollege').val();
                student.Student_TermID = $('#chckTerm').val();


                if ($('#chkGender').val() == 1) {
                    student.Student_Gender = "Male";
                } else {
                    student.Student_Gender = "Female";
                }

                data.push(student);

                var ipaddress = sessionStorage.getItem("ipaddress");

                $.ajax({
                    url: 'http://' + ipaddress + '/voting/API/index.php',
                    async: true,
                    type: 'POST',
                    crossDomain: true,
                    dataType: 'json',
                    data: {
                        command: 'insert_student',
                        data: data
                    },
                    success: function(response) {
                        var decode = response;

                        if (decode.success == true) {
                            $('#btn-save').button('reset');
                            alert(decode.msg);
                            window.location.href = 'student-list.php';
                        } else if (decode.success === false) {
                            $('#btn-save').button('reset');
                            alert(decode.error);
                            return;
                        }
                    },
                    error: function(error) {
                        $('#btn-save').button('reset');
                        console.log("Error:");
                        console.log(error.responseText);
                        console.log(error.message);
                        return;
                    }
                });
            }
        } else {
            $('#btn-save').button('reset');
        }
    } else {
        if (validate() == true) {
            var data = new Array();

            student = new Object();
            student.Student_ID = $('#txtStudentID').val();
            student.Student_Password = $('#txtPassword').val();
            student.Student_FirstName = $('#txtFirstName').val();
            student.Student_MiddleInitial = $('#txtMiddleName').val();
            student.Student_LastName = $('#txtLastName').val();
            student.Student_ProgID = $('#chckProgID').val();
            // student.Student_CollegeID = $('#chckCollege').val();
            student.Student_TermID = $('#chckTerm').val();

            if ($('#chkGender').val() == 1) {
                student.Student_Gender = "Male";
            } else {
                student.Student_Gender = "Female";
            }

            data.push(student);

            var ipaddress = sessionStorage.getItem("ipaddress");

            $.ajax({
                url: 'http://' + ipaddress + '/voting/API/index.php',
                async: true,
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: {
                    command: 'update_student',
                    student_id: $('#txtStudentID').val(),
                    data: data
                },
                success: function(response) {
                    var decode = response;
                    console.log(decode);
                    if (decode.success == true) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        window.location.href = 'student-list.php';
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.error);
                        return;
                    }
                },
                error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
            });
        } else {
            $('#btn-save').button('reset');
        }
    }
}

function checkStudent(fullname){
    var ipaddress = sessionStorage.getItem("ipaddress");
    var value  =false;
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'checkStudent',
            Fullname: fullname
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                value = true;
            } else if (decode.success == false) {
                value = false;
            }
        }
    });

    return value;
}

//edit student
$(document).on("click", ".edit-student", function() {
    var student_id = $(this).data('id');
    var TermID = $(this).data('termid');
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'get_student',
            student_id: student_id,
            TermID: TermID
        },
        success: function(response) {
            var decode = response;

            if (decode.success == true) {
                $('#authenticity_token').val(decode.student.ID);
                $('#txtStudentID').val(decode.student.StudentID);
                $('#txtPassword').val(decode.student.Password);
                $('#txtLastName').val(decode.student.LastName);
                $('#txtFirstName').val(decode.student.FirstName);
                $('#txtMiddleName').val(decode.student.MiddleInitial);
                if (decode.student.Gender == 'Male') {
                    $('#chkGender').val(1);
                } else {
                    $('#chkGender').val(2);
                }
                $('#chckProgID').val(decode.student.ProgID);
                $('#chckCollege').val(decode.student.CollegeID);
                $('#chckTerm').val(decode.student.TermID);

                $('#myModal').modal('show')
            } else if (decode.success === false) {
                $('#myModal').modal('hide');
                alert(decode.msg);
                return;
            }
        }
    });
});


//delete student
$(document).on("click", ".remove-student", function() {
    if (confirm("Are you sure to Delete this Student?")) {

        var student_id = $(this).data('id');
        var TermID = $(this).data('termid');


        var ipaddress = sessionStorage.getItem("ipaddress");

        $.ajax({
            url: 'http://' + ipaddress + '/voting/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'delete_student',
                student_id: student_id,
                TermID: TermID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = 'student-list.php';
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }
            }
        });
    }
});

function get_active_config() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        // url: 'http://localhost/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'get_active_config'
        },
        success: function(response) {
            var decode = response;
            if (decode.success == true) {
                window.localStorage['config'] = JSON.stringify(decode.config);
            } else {
                alert(decode.msg);
                return;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}

$('#pagination').on('click', '.page-numbers', function() {
    var page = $(this).attr('data-id');

    fetch_all_student(page);
});


function fetch_all_student(page) {
    var ipaddress = sessionStorage.getItem("ipaddress");
    var config = JSON.parse(window.localStorage['config'] || '{}');

    $('#processing-modal').modal('show');
    $('#tbl_student tbody > tr').remove();
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        // url: 'http://localhost/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select_all_student',
            TermID: config.TermID,
            page: page
        },
        success: function(response) {
            var decode = response;
            if (decode) {
                if (decode.students.length > 0) {
                    for (var i = 0; i < decode.students.length; i++) {
                        var row = decode.students;

                        var html = '<tr class="odd">\
                                <td>' + row[i].StudentID + '</td>\
                                <td>' + row[i].Fullname + '</td>\
                                <!-- <td>\
                                    <span class="label label-important">' + row[i].Gender + '</span>\
                                </td> -->\
                                <td>' + row[i].Course + '</td>\
                                <!-- <td>' + row[i].College + '</td> -->\
                               <td>' + row[i].ElectionTerm + '</td>\
                                <td>' + row[i].Password + '</td>\
                                <td class=" ">\
                                    <div class="text-right">\
                                        <a class="edit-student btn btn-success btn-xs" data-id="' + row[i].StudentID + '" data-termID="' + row[i].TermID + '">\
                                            <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-student btn btn-danger btn-xs" data-id="' + row[i].StudentID + '" data-termID="' + row[i].TermID + '">\
                                            <i class="icon-remove"></i>\
                                        </a>\
                                    </div>\
                                </td>\
                            </tr>';

                        $("#tbl_student tbody").append(html);
                    }
                    $('#pagination').html(decode.pagination);

                    $("#tbl_student").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }
            $('#processing-modal').modal('hide');
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            $('#processing-modal').modal('hide');
            return;
        }
    });
}

function search_student() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    var config = JSON.parse(window.localStorage['config'] || '{}');

    $('#processing-modal').modal('show');
    $('#tbl_student tbody > tr').remove();
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        // url: 'http://localhost/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'search_student',
            TermID: config.TermID,
            value: $('#txtSearch').val(),
            page: '1'
        },
        success: function(response) {
            var decode = response;
            for (var i = 0; i < decode.students.length; i++) {
                var row = decode.students;

                var html = '<tr class="odd">\
                                <td>' + row[i].StudentID + '</td>\
                                <td>' + row[i].Fullname + '</td>\
                                <!-- <td>\
                                    <span class="label label-important">' + row[i].Gender + '</span>\
                                </td> -->\
                                <td>' + row[i].Course + '</td>\
                                <!-- <td>' + row[i].College + '</td> -->\
                                <td>' + row[i].ElectionTerm + '</td>\
                                <td>' + row[i].Password + '</td>\
                                <td class=" ">\
                                    <div class="text-right">\
                                        <a class="edit-student btn btn-success btn-xs" data-id="' + row[i].StudentID + '" data-termID="' + row[i].TermID + '">\
                                            <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-student btn btn-danger btn-xs" data-id="' + row[i].StudentID + '" data-termID="' + row[i].TermID + '">\
                                            <i class="icon-remove"></i>\
                                        </a>\
                                    </div>\
                                </td>\
                            </tr>';

                $("#tbl_student tbody").append(html);
            }
            $('#pagination').html(decode.pagination);
            $("#tbl_student").addClass('tablesorter');

            var resort = true;
            $("table").trigger("update", [resort]);

            $('#processing-modal').modal('hide');
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            $('#processing-modal').modal('hide');
            return;
        }
    });
}

function search_studentTyping() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    var config = JSON.parse(window.localStorage['config'] || '{}');

    $('#tbl_student tbody > tr').remove();
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        // url: 'http://localhost/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'search_student',
            TermID: config.TermID,
            value: $('#txtSearch').val(),
            page: '1'
        },
        success: function(response) {
            var decode = response;
            for (var i = 0; i < decode.students.length; i++) {
                var row = decode.students;

                var html = '<tr class="odd">\
                                <td>' + row[i].StudentID + '</td>\
                                <td>' + row[i].Fullname + '</td>\
                                <!-- <td>\
                                    <span class="label label-important">' + row[i].Gender + '</span>\
                                </td> -->\
                                <td>' + row[i].Course + '</td>\
                                <!-- <td>' + row[i].College + '</td> -->\
                                <td>' + row[i].ElectionTerm + '</td>\
                                <td>' + row[i].Password + '</td>\
                                <td class=" ">\
                                    <div class="text-right">\
                                        <a class="edit-student btn btn-success btn-xs" data-id="' + row[i].StudentID + '" data-termID="' + row[i].TermID + '">\
                                            <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-student btn btn-danger btn-xs" data-id="' + row[i].StudentID + '" data-termID="' + row[i].TermID + '">\
                                            <i class="icon-remove"></i>\
                                        </a>\
                                    </div>\
                                </td>\
                            </tr>';
                $("#tbl_student tbody").append(html);
            }

            $('#pagination').html(decode.pagination);
            $("#tbl_student").addClass('tablesorter');

            var resort = true;
            $("table").trigger("update", [resort]);
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}

// function fetch_college() {
//     var ipaddress = sessionStorage.getItem("ipaddress");

//     $.ajax({
//         url: 'http://' + ipaddress + '/voting/API/index.php',
//         async: true,
//         type: 'POST',
//         crossDomain: true,
//         dataType: 'json',
//         data: {
//             command: 'select_For_Combo'
//         },
//         success: function(response) {
//             var decode = response;
//             $('#chckCollege').empty();
//             for (var i = 0; i < decode.colleges.length; i++) {
//                 var row = decode.colleges;


//                 var html = '<option id="' + row[i].CollegeID + '" value="' + row[i].CollegeID + '">' + row[i].CollegeName + '</option>';
//                 $("#chckCollege").append(html);
//             }
//         },
//         error: function(error) {
//             $('#btn-save').button('reset');
//             console.log("Error:");
//             console.log(error.responseText);
//             console.log(error.message);
//             return;
//         }
//     });
// }

function fetch_AcadProg() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select_For_ComboCourse'
        },
        success: function(response) {
            var decode = response;
            $('#chckProgID').empty();
            for (var i = 0; i < decode.programs.length; i++) {
                var row = decode.programs;


                var html = '<option id="' + row[i].ProgID + '" value="' + row[i].ProgID + '">' + row[i].ProgName + '</option>';
                $("#chckProgID").append(html);
            }
        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function fetch_Term() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select2_config'
        },
        success: function(response) {
            var decode = response;
            $('#chckTerm').empty();
            for (var i = 0; i < decode.configs.length; i++) {
                var row = decode.configs;


                var html = '<option id="' + row[i].TermID + '" value="' + row[i].TermID + '">' + row[i].Config + '</option>';
                $("#chckTerm").append(html);
            }
        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function commandtoClear() {
    $('#txtStudentID').val('');
    $('#txtPassword').val('');
    $('#txtFullname').val('');
    $('#chkGender').val(1);
}

function generate() {
    if (confirm('Are you sure to generate password for all students?')) {
        $('#generator').removeClass('hidden');
        $('#result').addClass('hidden');
        $('#btn-generate').button('loading');
        var ipaddress = sessionStorage.getItem("ipaddress");
        var config = JSON.parse(window.localStorage['config'] || '{}');
        $.ajax({
            url: 'http://' + ipaddress + '/voting/API/index.php',
            type: 'POST',
            crossDomain: true,
            dataType: 'json',
            data: {
                command: 'generate_password',
                TermID: config.TermID,
            },
            success: function(response) {
                var decode = response;
                if (decode.success == true) {
                    $('#btn-generate').button('reset');
                    $('#lblgenerated').html(decode.student);
                    $('#generator').addClass('hidden');
                    $('#result').removeClass('hidden');
                    alert(decode.msg);
                } else {
                    alert(decode.msg);
                    $('#btn-generate').button('reset');
                    $('#generator').addClass('hidden');
                    $('#result').removeClass('hidden');
                    return;
                }
            },
            error: function(error) {
                $('#btn-generate').button('reset');
                $('#generator').addClass('hidden');
                $('#result').removeClass('hidden');
                console.log("Error:");
                console.log(error.responseText);
                console.log(error.message);
                return;
            }
        });
    } else {
        $('#generator').addClass('hidden');
        $('#result').removeClass('hidden');
    }
}

function stop_generate() {
    $('#generator').addClass('hidden');
    $('#result').removeClass('hidden');
    $('#btn-generate').button('reset');
    $('#lblgenerated').html('0');
    $('#passwordModal').modal('hide');

    refresh();
}

function privilege(user) {
    if (user._Dashboard == 1) {
        $('#LIST li').eq(0).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(1).removeClass('hidden');
    }

    // if(user._College == 1){
    //     $('#LIST li').eq(2).removeClass('hidden');
    // }

    if (user._Student == 1) {
        $('#LIST li').eq(2).removeClass('hidden');
    }

    if (user._PartyList == 1) {
        $('#LIST li').eq(3).removeClass('hidden');
    }

    if (user._Candidates == 1) {
        $('#LIST li').eq(4).removeClass('hidden');
    }

    if (user._ElectoralPosition == 1) {
        $('#LIST li').eq(5).removeClass('hidden');
    }

    if (user._AcademicProgram == 1) {
        $('#LIST li').eq(6).removeClass('hidden');
    }

    if (user._ElectionConfig == 1) {
        $('#LIST li').eq(7).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(8).removeClass('hidden');
    }
}

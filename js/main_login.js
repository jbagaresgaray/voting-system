$(document).ready(function() {
    $('#server').val(sessionStorage.getItem("ipaddress"));

    var user = JSON.parse(window.localStorage['user'] || '{}');

    if (Object.keys(user).length > 0) {
        console.log('redirect to main');
        window.location.href = "main.php";
    }

});

function validate() {
    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;

    if ($('#username').val() == '') {
        $('#username').next('span').text('Username is required.');
        alert('Username is required.');
        empty = true;
        return false;
    }

    if ($('#password').val() == '') {
        $('#password').next('span').text('Password is required.');
        alert('Password is required.');
        empty = true;
        return false;
    }

    if ($('#server').val() == '') {
        $('#server').next('span').text('IP Address is required.');
        alert('IP Address is required.');
        empty = true;
        return false;
    }

    sessionStorage.setItem("ipaddress", $('#server').val());

    return true;
}

function login() {
    if (validate() == true) {
        var ipaddress = sessionStorage.getItem("ipaddress");
        $('#processing-modal').modal('show');
        $.ajax({
            url: 'http://' + ipaddress + '/voting/API/index.php',
            async: false,
            type: 'POST',
            crossDomain: true,
            dataType: 'json',
            data: {
                command: 'main_login',
                username: $('#username').val(),
                password: $('#password').val()
            },
            success: function(response) {
                var decode = response;
                if (decode.success == false) {
                    $('#processing-modal').modal('hide');
                    $('#error_message').html(decode.msg);
                    $('#errModal').modal('show');
                    return false;
                } else {
                    $('#processing-modal').modal('hide');
                    window.localStorage['user'] = JSON.stringify(decode.user);
                    window.location.href = "main.php";
                }
            },
            error: function(error) {
                console.log("Error:");
                console.log(error);
                empty = false;
                $('#processing-modal').modal('hide');
                return empty;
            }
        });


    }
}

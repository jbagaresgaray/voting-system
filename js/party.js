$(document).ready(function() {
    var user = JSON.parse(window.localStorage['user'] || '{}');

    if (Object.keys(user).length < 1) {
        console.log('redirect to main');
        window.location.href = "index.php";
    }


    $('#current_user').html(user.FullName+' ('+user.GroupName+')');


    privilege(user);

    fetch_all_party('1');
    select2_config();
});

$('#txtSearch').keyup(function(){
    console.log('fetching ...');
    search_partytyping();
});

function refresh() {
    fetch_all_party('1');
    select2_config();
}

//Reset  Help-inline
function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

function logout() {
    window.localStorage.clear();
    window.location.href = "index.php";
}


function validate() {
    //Rest Help-inline
    resetHelpInLine();

    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;

    if ($('#cboElectionTerm').val() == null) {
        $('#cboElectionTerm').next('span').text('Election Term is required.');
        empty = true;
    }

    if ($('#txtName').val() == '') {
        $('#txtName').next('span').text('Partylist Name is required.');
        empty = true;
    }

    if ($('#txtShortName').val() == '') {
        $('#txtShortName').next('span').text('Partylist Shortname is required.');
        empty = true;
    }

    if ($('#txtExpName').val() == '') {
        $('#txtExpName').next('span').text('Partylist Expound Name is required.');
        empty = true;
    }

    if ($('#txtGoals').val() == '') {
        $('#txtGoals').next('span').text('Partylist Goals and Objectives is required.');
        empty = true;
    }

    if ($('#txtProjects').val() == '') {
        $('#txtProjects').next('span').text('Partylist Projects is required.');
        empty = true;
    }

    if ($('#dtpDateSubmitted').val() == '') {
        $('#dtpDateSubmitted').next('span').text('Date Submission is required.');
        empty = true;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    return true;
}

function save() {
    $('#btn-save').button('loading');
    var user = JSON.parse(window.localStorage['user'] || '{}');
    var party_id = $('#authenticity_token').val();

    if (party_id == '') {
        if (validate() == true) {

            var data = new Array();

            party = new Object();
            party.party_name = $('#txtName').val();
            party.party_ExpName = $('#txtExpName').val();
            party.party_logo = '';
            party.party_goals = $('#txtGoals').val();
            party.party_projects = $('#txtProjects').val();
            party.party_submission = $('#dtpDateSubmitted').val();
            party.created_by = user.FullName;
            party.modify_by = user.FullName;
            party.party_TermID = $('#cboElectionTerm').val();


            data.push(party);

            var ipaddress = sessionStorage.getItem("ipaddress");

            $.ajax({
                url: 'http://' + ipaddress + '/voting/API/index.php',
                async: true,
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: {
                    command: 'insert_partylist',
                    data: data
                },
                success: function(response) {

                    var decode = response;
                    if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "partylist-list.php"
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
                },
                error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
            });
        } else {
            $('#btn-save').button('reset');
        }
    } else {
        if (validate() == true) {

            var data = new Array();

            party = new Object();
            party.party_name = $('#txtName').val();
            party.party_ExpName = $('#txtExpName').val();
            party.party_logo = '';
            party.party_goals = $('#txtGoals').val();
            party.party_projects = $('#txtProjects').val();
            party.party_submission = $('#dtpDateSubmitted').val();
            party.created_by = user.FullName;
            party.modify_by = user.FullName;
            party.party_TermID = $('#cboElectionTerm').val();


            data.push(party);

            var ipaddress = sessionStorage.getItem("ipaddress");

            $.ajax({
                url: 'http://' + ipaddress + '/voting/API/index.php',
                async: true,
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: {
                    command: 'update_party',
                    party_id: party_id,
                    data: data
                },
                success: function(response) {

                    var decode = response;
                    if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "partylist-list.php"
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.msg);
                        return;
                    }
                },
                error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
            });
        } else {
            $('#btn-save').button('reset');
        }
    }

}

$(document).on("click", ".edit-party-icon", function() {

    var party_id = $(this).data('id');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'get_party',
            party_id: party_id
        },
        success: function(response) {
            var decode = response;

            if (decode.success == true) {
                $('#authenticity_token').val(decode.party.PartyID);
                $('#cboElectionTerm').val(decode.party.TermID);
                $('#txtName').val(decode.party.PartyName);
                $('#txtExpName').val(decode.party.ExpName);
                $('#txtGoals').val(decode.party.Goals);
                $('#txtProjects').val(decode.party.Projects);

                var dataToSplit = decode.party.DateSubmitted;
                var i = dataToSplit.slice(0, 10).split('-');
                var date = i[0] + "-" + i[1] + "-" + i[2];

                $('#dtpDateSubmitted').val(date);


                $('#myModal').modal('show')
            } else if (decode.success === false) {
                alert(decode.msg);
                return;
            }

        }
    });
});


// on delete icon click
$(document).on("click", ".remove-party-icon", function() {
    if (confirm('Are you sure to delete this Partylist?')) {

        var party_id = $(this).data('id');

        var ipaddress = sessionStorage.getItem("ipaddress");

        $.ajax({
            url: 'http://' + ipaddress + '/voting/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'delete_party',
                party_id: party_id
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    alert(decode.msg);
                    window.location.href = "partylist-list.php";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});

$('#pagination').on('click', '.page-numbers', function() {
    var page = $(this).attr('data-id');

    fetch_all_party(page);
});


function fetch_all_party(page) {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $('#processing-modal').modal('show');
    $('#tbl_party tbody > tr').remove();
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select_all_party',
            page: page
        },
        success: function(response) {
            var decode = response;

            if (decode) {
                if (decode.partylist.length > 0) {
                    for (var i = 0; i < decode.partylist.length; i++) {
                        var row = decode.partylist;
                        var html = '<tr class="odd">\
                                    <td>' + row[i].PartyName + '</td>\
                                    <td>' + row[i].ExpName + '</td>\
                                    <!-- <td>' + row[i].ShortName + '</td> -->\
                                    <td>' + row[i].DateSubmitted + '</td>\
                                    <td>' + row[i].Config + '</td>\
                                    <td class=" ">\
                                      <div class="text-right">\
                                        <a class="edit-party-icon btn btn-success btn-xs" data-id="' + row[i].PartyID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-party-icon btn btn-danger btn-xs" data-id="' + row[i].PartyID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_party tbody").append(html);
                    }
                    $('#pagination').html(decode.pagination);
                    $("#tbl_party").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }
            $('#processing-modal').modal('hide');

        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            $('#processing-modal').modal('hide');
            return;
        }
    });
}

function search_party() {

    if ($('#txtSearch').val() == '') {
        alert('Please input item to search fields correctly.');
        return false;
    }

    var ipaddress = sessionStorage.getItem("ipaddress");

    $('#processing-modal').modal('show');
    $('#tbl_party tbody > tr').remove();

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'search_party',
            value: $('#txtSearch').val(),
            page: '1'
        },
        success: function(response) {
            var decode = response;
            if (decode) {
                if (decode.partylist.length > 0) {
                    for (var i = 0; i < decode.partylist.length; i++) {
                        var row = decode.partylist;
                        var html = '<tr class="odd">\
                                    <td>' + row[i].PartyName + '</td>\
                                    <td>' + row[i].ExpName + '</td>\
                                    <!-- <td>' + row[i].ShortName + '</td> -->\
                                    <td>' + row[i].DateSubmitted + '</td>\
                                    <td>' + row[i].Config + '</td>\
                                    <td class=" ">\
                                      <div class="text-right">\
                                        <a class="edit-party-icon btn btn-success btn-xs" data-id="' + row[i].PartyID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-party-icon btn btn-danger btn-xs" data-id="' + row[i].PartyID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_party tbody").append(html);
                    }
                    $('#pagination').html(decode.pagination);
                    $("#tbl_party").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }
            $('#processing-modal').modal('hide');
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            $('#processing-modal').modal('hide');
            return;
        }
    });
}

function search_partytyping() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $('#tbl_party tbody > tr').remove();

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'search_party',
            value: $('#txtSearch').val(),
            page: '1'
        },
        success: function(response) {
            var decode = response;
            if (decode) {
                if (decode.partylist.length > 0) {
                    console.log(decode.partylist);
                    for (var i = 0; i < decode.partylist.length; i++) {
                        var row = decode.partylist;
                        var html = '<tr class="odd">\
                                    <td>' + row[i].PartyName + '</td>\
                                    <td>' + row[i].ExpName + '</td>\
                                    <!-- <td>' + row[i].ShortName + '</td> -->\
                                    <td>' + row[i].DateSubmitted + '</td>\
                                    <td>' + row[i].Config + '</td>\
                                    <td class=" ">\
                                      <div class="text-right">\
                                        <a class="edit-party-icon btn btn-success btn-xs" data-id="' + row[i].PartyID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-party-icon btn btn-danger btn-xs" data-id="' + row[i].PartyID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_party tbody").append(html);
                    }
                    $('#pagination').html(decode.pagination);
                    $("#tbl_party").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}

function select2_config() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select2_config'
        },
        success: function(response) {
            var decode = response;
            $("#cboElectionTerm").empty();
            for (var i = 0; i < decode.configs.length; i++) {
                var row = decode.configs;
                var html = '<option id="' + row[i].TermID + '" value="' + row[i].TermID + '">' + row[i].Config + '</option>';
                $("#cboElectionTerm").append(html);
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function commandtoClear() {
    $('#authenticity_token').val('');
    $('#txtName').val('');
    $('#txtExpName').val('');
    $('#txtGoals').val('');
    $('#txtProjects').val('');
    $('#dtpDateSubmitted').val('');
}

function privilege(user) {
    if (user._Dashboard == 1) {
        $('#LIST li').eq(0).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(1).removeClass('hidden');
    }

    // if(user._College == 1){
    //     $('#LIST li').eq(2).removeClass('hidden');
    // }

    if (user._Student == 1) {
        $('#LIST li').eq(2).removeClass('hidden');
    }

    if (user._PartyList == 1) {
        $('#LIST li').eq(3).removeClass('hidden');
    }

    if (user._Candidates == 1) {
        $('#LIST li').eq(4).removeClass('hidden');
    }

    if (user._ElectoralPosition == 1) {
        $('#LIST li').eq(5).removeClass('hidden');
    }

    if (user._AcademicProgram == 1) {
        $('#LIST li').eq(6).removeClass('hidden');
    }

    if (user._ElectionConfig == 1) {
        $('#LIST li').eq(7).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(8).removeClass('hidden');
    }
}

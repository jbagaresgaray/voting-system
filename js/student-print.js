$(document).ready(function() {
	var user = JSON.parse(window.localStorage['user'] || '{}');

    if (Object.keys(user).length < 1) {
        console.log('redirect to main');
        window.location.href = "index.php";
    }


    $('#current_user').html(user.FullName+' ('+user.GroupName+')');


    Date.prototype.yyyymmdd = function() {
      var yyyy = this.getFullYear().toString();
      var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
      var dd  = this.getDate().toString();
      return yyyy + "/" + (mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]); // padding
    };

    d = new Date();
    $('#date_now').html(d.yyyymmdd());

    query_student();
    privilege(user);
});

function logout() {
    window.localStorage.clear();
    window.location.href = " index.php";
}

function query_student() {
    var ipaddress = sessionStorage.getItem("ipaddress");

   $('#processing-modal').modal('show');
   $('#tbl_student tbody > tr').remove();
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'Print_Student'
        },
        success: function(response) {
            var decode = response;
             if (decode) {
                if (decode.students.length > 0) {
                    for (var i = 0; i < decode.students.length; i++) {
                        var row = decode.students;

                        console.log(decode.students);

                        var html = '<tr class="odd">\
                                    <td>' + row[i].StudentID + '</td>\
                                    <td>' + row[i].Fullname + '</td>\
                                    <!-- <td>'+ row[i].Gender + '</td> -->\
                                    <td>' + row[i].Course + '</td>\
                                    <!-- <td>' + row[i].College + '</td> -->\
                                    <td>' + row[i].ElectionTerm + '</td>\
                                     <td>' + row[i].Password + '</td> \
                                    </tr>';
                        $("#tbl_student tbody").append(html);
                    }

                }
            }
              $('#processing-modal').modal('hide');
        },
        error: function(error) {
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            $('#processing-modal').modal('hide');
            return;
        }
    });
}

function privilege(user) {
    if (user._Dashboard == 1) {
        $('#LIST li').eq(0).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(1).removeClass('hidden');
    }

    // if(user._College == 1){
    //     $('#LIST li').eq(2).removeClass('hidden');
    // }

    if (user._Student == 1) {
        $('#LIST li').eq(2).removeClass('hidden');
    }

    if (user._PartyList == 1) {
        $('#LIST li').eq(3).removeClass('hidden');
    }

    if (user._Candidates == 1) {
        $('#LIST li').eq(4).removeClass('hidden');
    }

    if (user._ElectoralPosition == 1) {
        $('#LIST li').eq(5).removeClass('hidden');
    }

    if (user._AcademicProgram == 1) {
        $('#LIST li').eq(6).removeClass('hidden');
    }

    if (user._ElectionConfig == 1) {
        $('#LIST li').eq(7).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(8).removeClass('hidden');
    }
}s

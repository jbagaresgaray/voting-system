$(document).ready(function() {
    var user = JSON.parse(window.localStorage['canvass_user'] || '{}');


    $('#current_user').html(user.FullName+' ('+user.GroupName+')');


    Date.prototype.yyyymmdd = function() {
      var yyyy = this.getFullYear().toString();
      var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
      var dd  = this.getDate().toString();
      return yyyy + "/" + (mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]); // padding
    };

    d = new Date();
    $('#date_now').html(d.yyyymmdd());

    get_active_config();
    getStatisticsOverview();
    GenerateRankingResult();
});


function get_active_config() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        // url: 'http://localhost/voting/API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'get_active_config'
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                window.localStorage['config'] = JSON.stringify(decode.config);
            } else {
                alert(decode.msg);
                return;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}

function getStatisticsOverview() {
    var config = JSON.parse(window.localStorage['config'] || '{}');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getStatisticsOverview',
            TermID: config.TermID
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                window.localStorage['totalVoters'] = decode.totalVoters;
                window.localStorage['totalNotVoted'] = decode.totalNotVoted;
                window.localStorage['totalVoted'] = decode.totalVoted;

                var totalVoted = (window.localStorage['totalVoted'] || '{}');
                var totalVoters = (window.localStorage['totalVoters'] || '{}');
                var totalNotVoted = (window.localStorage['totalNotVoted'] || '{}');

                $('#announcement-voted-heading').html(totalVoted);
                $('#announcement-voteds-heading').html(totalVoted);
                $('#announcement-voters-heading').html(totalVoters);

            } else {
                alert(decode.msg);
                return;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}

function GenerateRankingResult() {

    var positions = JSON.parse(window.localStorage['canvass_positions'] || '{}');
    var config = JSON.parse(window.localStorage['config'] || '{}');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getCandidatesFinalCanvassing',
            TermID: config.TermID
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            var html = '';

            $('#content2').empty();
            if (decode.success == true) {
                if (decode.candidates.length > 0) {

                    for (var i = 0; i < positions.length; i++) {
                        var data = positions[i];
                        html += '<div class="panel panel-default">\
                                    <div class="panel-heading">\
                                        <h3 class="text-center"><strong>' + data.PositionName + '</strong></h3>\
                                    </div>\
                                    <div class="table-responsive">\
                                        <table class="table table-condensed" id="tbl_candidate">\
                                            <thead>\
                                                <tr>\
                                                    <td class="text-center"><strong>Name of Candidates</strong>\
                                                    </td>\
                                                    <td class="text-center"><strong>Votes Garnered</strong>\
                                                    </td>\
                                                </tr>\
                                            </thead>\
                                            <tbody>';

                        for (var a = 0; a < decode.candidates.length; a++) {
                            var row = decode.candidates[a];
                            if (data.PositionID == row.PositionID) {
                                html += '<tr>\
                                            <td> ' + row.CandidateName + ' </td>\
                                            <td class="text-center">' + row.VoteCount + '</td>\
                                        </tr>';
                                $("#tbl_candidate tbody").append(html);
                            }
                        }
                        html += '</tbody>\
                            </table>\
                        </div>\
                    </div>';
                    }
                }
            } else {
                alert(decode.msg);
                return false;
            }
            $('#print_content').append(html);
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return false;
        }
    });

}

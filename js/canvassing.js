var myVar;
var task;

$(document).ready(function() {
    var user = JSON.parse(window.localStorage['canvass_user'] || '{}');

    if (Object.keys(user).length < 1) {
        console.log('redirect to main');
        window.location.href = "index.php";
    }

    $('#current_user').html(user.FullName);

    get_active_config();
    getCandidacyPosition();

    $('#lst_dashboard li').removeClass('active');
    $('#lst_dashboard li').eq(1).addClass("active");
    $('#lst_dashboard li').eq(1).text();

    GenerateDashboard();

    myStopFunction();

    createTask($('#lst_dashboard li').eq(1).text(), 'getStatisticsOverview()');
});



function logout() {
    window.localStorage.clear();
    window.location.href = " index.php";
}

function createTask(tasks, functions) {
    console.log('createTask');

    var data = new Array();
    task = new Object();
    task.task_name = tasks.trim();
    task.task_function = functions;

    data.push(task)

    window.localStorage['task'] = JSON.stringify(data);

    setInterval(function() {
        console.log('Interval for task');
        task = JSON.parse(window.localStorage['task'] || '{}');
    }, 1000);

    if (task) {
        if (task.task_name === 'Statistics Overview') {
            myVar = setInterval(task.task_function, 5000);
        } else if (task.task_name === 'Ranking Result') {
            myVar = setInterval(task.task_function, 5000);
        } else if (task.task_name === 'Position') {
            myVar = setInterval(task.task_function, 5000);
        }
    }
}

function myStopFunction() {
    console.log('clearInterval');
    clearInterval(myVar);
    window.localStorage.removeItem("task");
}


function get_active_config() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'get_active_config'
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                window.localStorage['config'] = JSON.stringify(decode.config);
            } else {
                alert(decode.msg);
                return;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}

function getStatisticsOverview() {
    var config = JSON.parse(window.localStorage['config'] || '{}');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getStatisticsOverview',
            TermID: config.TermID
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                window.localStorage['totalVoters'] = decode.totalVoters;
                window.localStorage['totalNotVoted'] = decode.totalNotVoted;
                window.localStorage['totalVoted'] = decode.totalVoted;

                var totalVoted = (window.localStorage['totalVoted'] || '{}');
                var totalVoters = (window.localStorage['totalVoters'] || '{}');
                var totalNotVoted = (window.localStorage['totalNotVoted'] || '{}');

                $('#announcement-voted-heading').html(totalVoted);
                $('#announcement-voters-heading').html(totalVoters);
                $('#announcement-notvoted-heading').html(totalNotVoted);

            } else {
                alert(decode.msg);
                return;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}

function getCandidacyPosition() {

    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getCandidacyPosition'
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                window.localStorage['canvass_positions'] = JSON.stringify(decode.positions);
                var len = decode.positions.length;
                $('#lstPositions').empty();
                if (len > 0) {

                    for (var i = 0; i < len; i++) {
                        var data = decode.positions[i];
                        var row = '<li data-id="' + data.PositionID + '"><a href="#"><i class="fa fa-angle-double-right"></i> ' + data.PositionName + '</a>';
                        $('#lstPositions').append(row);
                    }

                }

            } else {
                alert(decode.msg);
                return false;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return false;
        }
    });
}

function GenerateDashboard() {
    $('#content').empty();

    var html = '<h1>Dashboard<small>Statistics Overview</small></h1>\
            <div class="row">\
                <div class="col-lg-3">\
                    <div class="panel panel-info">\
                        <div class="panel-heading">\
                            <div class="row">\
                                <div class="col-xs-6">\
                                    <i class="fa fa-thumbs-o-up fa-5x"></i>\
                                </div>\
                                <div class="col-xs-6 text-right">\
                                    <p class="announcement-heading" id="announcement-voted-heading">0</p>\
                                    <p class="announcement-text">Voted!</p>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
                <div class="col-lg-3">\
                    <div class="panel panel-danger">\
                        <div class="panel-heading">\
                            <div class="row">\
                                <div class="col-xs-6">\
                                    <i class="fa fa-thumbs-down fa-5x"></i>\
                                </div>\
                                <div class="col-xs-6 text-right">\
                                    <p class="announcement-heading" id="announcement-notvoted-heading">0</p>\
                                    <p class="announcement-text">Not Voted</p>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
                <div class="col-lg-3">\
                    <div class="panel panel-success">\
                        <div class="panel-heading">\
                            <div class="row">\
                                <div class="col-xs-6">\
                                    <i class="fa fa-users fa-5x"></i>\
                                </div>\
                                <div class="col-xs-6 text-right">\
                                    <p class="announcement-heading" id="announcement-voters-heading">0</p>\
                                    <p class="announcement-text">Total Voters</p>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
            </div>\
            <div class="row">\
              <div class="btn-group btn-group-lg">\
                <a href="print2.php" class="btn btn-primary"><span class="glyphicon glyphicon-print"></span>  Print Voters Actually Voted</a>\
                <a href="print3.php" class="btn btn-primary"><span class="glyphicon glyphicon-print"></span> Print Voters Not Voted</a>\
              </div>\
            </div>';
    $('#content').append(html);

    getStatisticsOverview();
}

$(document).on("click", "#lst_dashboard li", function() {
    $('#lstPositions li').removeClass('active');
    $('#lst_dashboard li').removeClass('active');
    $(this).addClass("active");

    myStopFunction();

    if ($(this).text().trim() == 'Statistics Overview') {
        createTask($(this).text(), 'getStatisticsOverview()');
    } else if ($(this).text().trim() == 'Ranking Result') {

        LayoutRankingResult();

        createTask($(this).text(), 'GenerateRankingResult()');
    }

    if (task.length > 0) {
        if (task[0].task_name === 'Statistics Overview') {
            myVar = setInterval(task[0].task_function, 3000);
        } else if (task[0].task_name === 'Ranking Result') {
            myVar = setInterval(task[0].task_function, 8000);
        } else if (task[0].task_name === 'Position') {
            myVar = setInterval(task[0].task_function, 5000);
        }
    }

    console.log($(this).text());
});


$(document).on("click", "#lstPositions li", function() {

    var config = JSON.parse(window.localStorage['config'] || '{}');

    $('#lst_dashboard li').removeClass('active');
    $('#lstPositions li').removeClass('active');
    $(this).addClass("active");

    getCandidatesByPositionID($(this).attr("data-id"), config.TermID, $(this).text());

    myStopFunction();

    createTask($(this).text(), 'getCandidatesByPositionID(' + $(this).attr("data-id") + ',' + config.TermID + ')');
});

function getCandidatesByPositionID(positionID, TermID, Position) {

    $('#content').empty();

    var html = '<h1>\
                <span id="lblPosition"></span>\
                <small>Statistics Overview</small>\
            </h1>\
            <div class="row">\
                <div class="col-lg-12">\
                    <div class="table-responsive">\
                        <table class="table" id="tbl_candidate">\
                            <thead>\
                                <tr>\
                                    <th style="width:60%">Candidate</th>\
                                    <th style="width:20%">Total Votes</th>\
                                    <th style="width:20%">% Votes</th>\
                                </tr>\
                            </thead>\
                            <tbody>\
                            </tbody>\
                        </table>\
                    </div>\
                </div>\
            </div>';
    $('#content').append(html);

    $('#lblPosition').html(Position);


    var ipaddress = sessionStorage.getItem("ipaddress");
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getCandidatesCanvassingByPositionID',
            positionID: positionID,
            TermID: TermID
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                window.localStorage['candidates'] = JSON.stringify(decode.candidates);

                var len = decode.candidates.length;
                $('#tbl_candidate tbody > tr').remove();
                if (len > 0) {;
                    $('#processing-modal').modal('show');

                    for (var i = 0; i < len; i++) {
                        var data = decode.candidates[i];
                        var img = data.Photo;
                        var new_img;
                        if (img === 'W0JJTkFSWV0=' || img === '') {
                            new_img = '../../img/photo.jpg';
                        } else {
                            new_img = 'data:image/x-xbitmap;base64,' + img;
                        }

                        var totalVoters = (window.localStorage['totalVoters'] || '{}');
                        var percent = data.VoteCount / totalVoters * 100;

                        var row = '<tr>\
                                    <td class="table-profile">\
                                        <div class="table-image">\
                                            <img src="' + new_img + '" width="120px" class="img-responsive img-circle" alt="Generic placeholder thumbnail">\
                                        </div>\
                                        <div class="table-info">\
                                            <h4>' + data.CandidateName + '</h4>\
                                            <small>' + data.Program + '</small>\
                                            <small>' + data.Partylist + '</small>\
                                        </div>\
                                    </td>\
                                    <td class="table-score">' + data.VoteCount + '</td>\
                                    <td class="table-score">' + Number(percent).toFixed(2) + ' %</td>\
                                </tr>';
                        $("#tbl_candidate tbody").append(row);
                    }
                    $('#processing-modal').modal('hide');

                }

            } else {
                alert(decode.msg);
                return false;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return false;
        }
    });
}

function LayoutRankingResult() {
    $('#content').empty();
    var html = '<div class="row">\
                      <div class="col-lg-12">\
                        <h1 class="text-center">FINAL CANVASSING RESULT</h1>\
                      </div>\
                    </div>\
                    <div class="row">\
                    <div class="col-lg-6">\
                          <p>\
                              <a href="print.php" type="button" class="btn btn-primary"><i class="fa fa-print fa-lg"></i> Print Canvassing</a>\
                              <a href="print-winner.php" type="button" class="btn btn-primary"><i class="fa fa-print fa-lg"></i> Print Winners</a>\
                          </p>\
                      </div>\
                    </div>\
                    <div id="content2"></div>';
    $('#content').append(html);
}

function GenerateRankingResult() {

    var positions = JSON.parse(window.localStorage['canvass_positions'] || '{}');
    var config = JSON.parse(window.localStorage['config'] || '{}');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getCandidatesFinalCanvassing',
            TermID: config.TermID
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            var html = '';

            $('#content2').empty();
            if (decode.success == true) {
                if (decode.candidates.length > 0) {

                    for (var i = 0; i < positions.length; i++) {
                        var data = positions[i];
                        html += '<h1>' + data.PositionName + '</h1>\
                                <div class="row">\
                                    <div class="col-lg-12">\
                                        <div class="table-responsive">\
                                            <table class="table" id="tbl_candidate">\
                                                <thead>\
                                                    <tr>\
                                                        <th style="width:60%">Candidate</th>\
                                                        <th style="width:20%">Total Votes</th>\
                                                    </tr>\
                                                </thead>\
                                                <tbody>';

                        for (var a = 0; a < decode.candidates.length; a++) {
                            var row = decode.candidates[a];
                            if (data.PositionID == row.PositionID) {

                                var img = row.Photo;
                                var new_img;
                                if (img === 'W0JJTkFSWV0=' || img === '') {
                                    new_img = '../../img/photo.jpg';
                                } else {
                                    new_img = 'data:image/x-xbitmap;base64,' + img;
                                }
                                html += '<tr>\
                                        <td class="table-profile">\
                                            <div class="table-image">\
                                                <img src="' + new_img + '" width="120px" class="img-responsive img-circle" alt="Generic placeholder thumbnail">\
                                            </div>\
                                            <div class="table-info">\
                                                <h4>' + row.CandidateName + '</h4>\
                                                <small>' + row.Program + '</small>\
                                                <small>' + row.Partylist + '</small>\
                                            </div>\
                                        </td>\
                                        <td class="table-score">' + row.VoteCount + '</td>\
                                    </tr>';
                                $("#tbl_candidate tbody").append(html);
                            }
                        }
                        html += '</tbody>\
                                </table>\
                            </div>\
                        </div>\
                    </div>';
                    }
                }
            } else {
                alert(decode.msg);
                return false;
            }
            $('#content2').append(html);
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return false;
        }
    });

}

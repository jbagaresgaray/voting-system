$(document).ready(function() {
    var user = JSON.parse(window.localStorage['user'] || '{}');

    if (Object.keys(user).length < 1) {
        console.log('redirect to main');
        window.location.href = "index.php";
    }


    $('#current_user').html(user.FullName+' ('+user.GroupName+')');


    privilege(user);

    fetch_all_user('1');
    fetch_all_user_group('1');
});

$('#searchValue1').keyup(function(){
    search_user_groupTyping();
});

$('#searchValue').keyup(function(){
    search_userTyping();
});

function refresh() {
    fetch_all_user('1');
    fetch_all_user_group('1');
}

//Reset  Help-inline
function resetHelpInLine() {
    $('span.help-inline').each(function() {
        $(this).text('');
    });
}

function logout() {
    window.localStorage.clear();
    window.location.href = " index.php";
}

function validate() {
    //Rest Help-inline
    resetHelpInLine();

    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;

    if ($('#cboUserGroup').val() == null) {
        $('#cboUserGroup').next('span').text('User Group is required.');
        empty = true;
    }

    if ($('#txtFirstName').val() == '') {
        $('#txtFirstName').next('span').text('First Name is required.');
        empty = true;
    }

    if ($('#txtMiddleName').val() == '') {
        $('#txtMiddleName').next('span').text('Middle Name is required.');
        empty = true;
    }

    if ($('#txtLastName').val() == '') {
        $('#txtLastName').next('span').text('Last Name is required.');
        empty = true;
    }

    if ($('#txtUsername').val() == '') {
        $('#txtUsername').next('span').text('Username is required.');
        empty = true;
    }

    if ($('#txtPassword').val().length < 6) {
        $('#txtPassword').next('span').text('Password is too short.');
        empty = true;
    }

    if ($('#txtPassword2').val() == '') {
        $('#txtPassword2').next('span').text('Username is required.');
        empty = true;
    }

    if ($('#txtPassword').val() !== $('#txtPassword2').val()) {
        $('#txtPassword2').next('span').text('Password did not match.');
        empty = true;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    return true;
}

function validateUserGroup() {
    resetHelpInLine();

    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;


    if ($('#txtGroupName').val() == '') {
        $('#txtGroupName').next('span').text('Group Name is required.')
        empty = true;
    }

    if (empty == true) {
        alert('Please input all the required fields correctly.');
        return false;
    }

    return true;
}

function saveUserAccount() {
    $('#btn-save').button('loding');
    var UserAccountID = $('#authenticity_token').val();
    var user = JSON.parse(window.localStorage['user'] || '{}');
    if (UserAccountID == '') {
        if (validate() == true) {

            var data = new Array();

            usersAcc = new Object();
            usersAcc.User_Group = $('#cboUserGroup').val();
            usersAcc.First_Name = $('#txtFirstName').val();
            usersAcc.Middle_Name = $('#txtMiddleName').val();
            usersAcc.Last_Name = $('#txtLastName').val();
            usersAcc.User_Username = $('#txtUsername').val();
            usersAcc.User_Password = $('#txtPassword').val();

            usersAcc.CreatedBy = user.UserAccountID;

            data.push(usersAcc);


            var ipaddress = sessionStorage.getItem("ipaddress");

            $.ajax({
                url: 'http://' + ipaddress + '/voting/API/index.php',
                async: true,
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: {
                    command: 'insert_user',
                    data: data
                },
                success: function(response) {
                    var decode = response;

                    if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "user-account-list.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.error);
                        return;
                    }
                },
                error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
            });
        } else {
            $('#btn-save').button('reset');
        }
    } else {

        if (validate() == true) {

            var data = new Array();

            usersAcc = new Object();
            usersAcc.User_Group = $('#cboUserGroup').val();
            usersAcc.First_Name = $('#txtFirstName').val();
            usersAcc.Middle_Name = $('#txtMiddleName').val();
            usersAcc.Last_Name = $('#txtLastName').val();
            usersAcc.User_Username = $('#txtUsername').val();
            usersAcc.User_Password = $('#txtPassword').val();
            usersAcc.ModifiedBy = user.UserAccountID;

            data.push(usersAcc);

            var ipaddress = sessionStorage.getItem("ipaddress");

            $.ajax({
                url: 'http://' + ipaddress + '/voting/API/index.php',
                async: true,
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: {
                    command: 'update_user',
                    UserAccountID: UserAccountID,
                    data: data
                },
                success: function(response) {
                    var decode = response;

                    if (decode.success == true) {
                        $('#btn-save').button('reset');
                        window.location.href = "user-account-list.php";
                    } else if (decode.success === false) {
                        $('#btn-save').button('reset');
                        alert(decode.error);
                        return;
                    }
                },
                error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
            });
        } else {
            $('#btn-save').button('reset');
        }
    }
}

function saveUserGroup() {
    $('#btn-saveUserGroup').button('loading');
    var GroupID = $('#authenticationGroupID').val();

    if (GroupID == '') {

        if (validateUserGroup() == true) {
            var data = new Array();

            UserGroup = new Object();
            UserGroup.Group_Name = $('#txtGroupName').val();
            UserGroup.Group_Desc = $('#txtDesc').val();

            if ($('#chckDashBoard').prop("checked") == true) {
                UserGroup.DashBoard = '1';
            } else {
                UserGroup.DashBoard = '0';
            }

            if ($('#chckUserAcc').prop("checked") == true) {
                UserGroup.UserAcc = '1';
            } else {
                UserGroup.UserAcc = '0';
            }

            if ($('#chckCollege').prop("checked") == true) {
                UserGroup.College = '1';
            } else {
                UserGroup.College = '0';
            }

            if ($('#chckStudent').prop("checked") == true) {
                UserGroup.Student = '1';
            } else {
                UserGroup.Student = '0';
            }

            if ($('#chckParty').prop("checked") == true) {
                UserGroup.Party = '1';
            } else {
                UserGroup.Party = '0';
            }

            if ($('#chckCandidates').prop("checked") == true) {
                UserGroup.Candidates = '1';
            } else {
                UserGroup.Candidates = '0';
            }

            if ($('#chckElectoral').prop("checked") == true) {
                UserGroup.Electoral = '1';
            } else {
                UserGroup.Electoral = '0';
            }

            if ($('#chckAcademic').prop("checked") == true) {
                UserGroup.Academic = '1';
            } else {
                UserGroup.Academic = '0';
            }

            if ($('#chckElection').prop("checked") == true) {
                UserGroup.Election = '1';
            } else {
                UserGroup.Election = '0';
            }


            data.push(UserGroup);

            var ipaddress = sessionStorage.getItem("ipaddress");

            $.ajax({
                url: 'http://' + ipaddress + '/voting/API/index.php',
                async: true,
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: {
                    command: 'insert_user_group',
                    data: data
                },
                success: function(response) {
                    var decode = response;

                    if (decode.success == true) {
                        $('#btn-saveUserGroup').button('reset');
                        window.location.href = 'user-account-list.php';
                    } else if (decode.success == false) {
                        $('#btn-saveUserGroup').button('reset');
                        alert(decode.error);
                        return;
                    }
                },
                error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
            });
        } else {
            $('#btn-saveUserGroup').button('reset');
        }
    } else {
        if (validateUserGroup() == true) {
            var data = new Array();

            UserGroup = new Object();
            UserGroup.Group_ID = $('#authenticationGroupID').val();
            UserGroup.Group_Name = $('#txtGroupName').val();
            UserGroup.Group_Desc = $('#txtDesc').val();

            if ($('#chckDashBoard').prop("checked") == true) {
                UserGroup.DashBoard = '1';
            } else {
                UserGroup.DashBoard = '0';
            }

            if ($('#chckUserAcc').prop("checked") == true) {
                UserGroup.UserAcc = '1';
            } else {
                UserGroup.UserAcc = '0';
            }

            if ($('#chckCollege').prop("checked") == true) {
                UserGroup.College = '1';
            } else {
                UserGroup.College = '0';
            }

            if ($('#chckStudent').prop("checked") == true) {
                UserGroup.Student = '1';
            } else {
                UserGroup.Student = '0';
            }

            if ($('#chckParty').prop("checked") == true) {
                UserGroup.Party = '1';
            } else {
                UserGroup.Party = '0';
            }

            if ($('#chckCandidates').prop("checked") == true) {
                UserGroup.Candidates = '1';
            } else {
                UserGroup.Candidates = '0';
            }

            if ($('#chckElectoral').prop("checked") == true) {
                UserGroup.Electoral = '1';
            } else {
                UserGroup.Electoral = '0';
            }

            if ($('#chckAcademic').prop("checked") == true) {
                UserGroup.Academic = '1';
            } else {
                UserGroup.Academic = '0';
            }

            if ($('#chckElection').prop("checked") == true) {
                UserGroup.Election = '1';
            } else {
                UserGroup.Election = '0';
            }


            data.push(UserGroup);

            var ipaddress = sessionStorage.getItem("ipaddress");

            $.ajax({
                url: 'http://' + ipaddress + '/voting/API/index.php',
                async: true,
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                data: {
                    command: 'update_user_group',
                    GroupID: GroupID,
                    data: data
                },
                success: function(response) {
                    var decode = response;

                    if (decode.success == true) {
                        $('#btn-saveUserGroup').button('reset');
                        window.location.href = 'user-account-list.php';
                    } else if (decode.success == false) {
                        $('#btn-saveUserGroup').button('reset');
                        alert(decode.error);
                        return;
                    }
                },
                error: function(error) {
                    $('#btn-save').button('reset');
                    console.log("Error:");
                    console.log(error.responseText);
                    console.log(error.message);
                    return;
                }
            });
        } else {
            $('#btn-saveUserGroup').button('reset');
        }
    }
}

//edit user account
$(document).on("click", ".edit-usersaccount-icon", function() {

    var UserAccountID = $(this).data('id');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'get_userForEdit',
            user_id: UserAccountID
        },
        success: function(response) {
            var decode = response;

            if (decode.success == true) {
                $('#authenticity_token').val(decode.user.UserAccountID);
                $('#cboUserGroup').val(decode.user.GroupID);
                $('#txtFirstName').val(decode.user.FirstName);
                $('#txtMiddleName').val(decode.user.MiddleName);
                $('#txtLastName').val(decode.user.LastName);
                $('#txtUsername').val(decode.user.UserName);
                $('#txtPassword').val(decode.user.Password);

                $('#usersModal').modal('show')
            } else if (decode.success === false) {
                $('#usersModal').modal('hide')
                alert(decode.msg);
                return;
            }

        }
    });
});

//edit user group

$(document).on("click", ".edit-group-icon", function() {
    var GroupID = $(this).data('id');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        data: {
            command: 'get_user_group',
            GroupID: GroupID
        },
        success: function(response) {
            var decode = response;

            if (decode.success == true) {
                $('#authenticationGroupID').val(decode.group.GroupID);
                $('#txtGroupName').val(decode.group.GroupName);
                $('#txtDesc').val(decode.group.Desc);

                if (decode.group._Dashboard == 1) {
                    $('#chckDashBoard').prop("checked", true);
                } else {
                    $('#chckDashBoard').prop("checked", false);
                }

                if (decode.group._UsersAccount == 1) {
                    $('#chckUserAcc').prop("checked", true);
                } else {
                    $('#chckUserAcc').prop("checked", false);
                }

                if (decode.group._College == 1) {
                    $('#chckCollege').prop("checked", true);
                } else {
                    $('#chckCollege').prop("checked", false);
                }

                if (decode.group._Student == 1) {
                    $('#chckStudent').prop("checked", true);
                } else {
                    $('#chckStudent').prop("checked", false);
                }

                if (decode.group._PartyList == 1) {
                    $('#chckParty').prop("checked", true);
                } else {
                    $('#chckParty').prop("checked", false);
                }

                if (decode.group._Candidates == 1) {
                    $('#chckCandidates').prop("checked", true);
                } else {
                    $('#chckCandidates').prop("checked", false);
                }

                if (decode.group._ElectoralPosition == 1) {
                    $('#chckElectoral').prop("checked", true);
                } else {
                    $('#chckElectoral').prop("checked", false);
                }

                if (decode.group._AcademicProgram == 1) {
                    $('#chckAcademic').prop("checked", true);
                } else {
                    $('#chckAcademic').prop("checked", false);
                }

                if (decode.group._ElectionConfig == 1) {
                    $('#chckElection').prop("checked", true);
                } else {
                    $('#chckElection').prop("checked", false);
                }

                $('#userGroupModal').modal('show');
            } else {
                $('#userGroupModal').modal('hide');
                alert(decode.msg);
                return;
            }
        }
    });
});

// delete user account
$(document).on("click", ".remove-usersaccount-icon", function() {
    if (confirm('Are you sure to delete this User?')) {

        var UserAccountID = $(this).data('id');

        var ipaddress = sessionStorage.getItem("ipaddress");

        $.ajax({
            url: 'http://' + ipaddress + '/voting/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'delete_user',
                UserAccountID: UserAccountID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = "user-account-list.php";
                } else if (decode.success === false) {
                    alert(decode.msg);
                    return;
                }

            }
        });

    }
});


//delete user group
$(document).on("click", ".remove-group-icon", function() {
    if (confirm('Are you sure to delete  this User Group?')) {
        var GroupID = $(this).data('id');

        var ipaddress = sessionStorage.getItem('ipaddress');

        $.ajax({
            url: 'http://' + ipaddress + '/voting/API/index.php',
            async: true,
            type: 'POST',
            data: {
                command: 'delete_user_group',
                GroupID: GroupID
            },
            success: function(response) {
                var decode = response;

                if (decode.success == true) {
                    window.location.href = 'user-account-list.php';
                } else if (decode.success == false) {
                    alert(decode.msg);
                    return;
                }
            }

        });
    }
});

function search_user() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    var value = $('#searchValue').val();

    $('#processing-modal').modal('show');
    $('#tbl_user_account tbody > tr').remove();
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'search_user',
            value: value
        },
        success: function(response) {
            var decode = response;

           if(decode){
                if(decode.users.length > 0){
                    for (var i = 0; i < decode.users.length; i++) {
                        var row = decode.users;

                        var html = '<tr class="odd">\
                                    <td>' + row[i].FullName + '</td>\
                                    <td>' + row[i].UserName + '</td>\
                                    <td>' + row[i].GroupName + '</td>\
                                    <td class=" ">\
                                      <div class="text-right">\
                                        <a class="edit-usersaccount-icon btn btn-success btn-xs" data-id="' + row[i].UserAccountID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-usersaccount-icon btn btn-danger btn-xs" data-id="' + row[i].UserAccountID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_user_account tbody").append(html);
                    }
                    $('#pagination').html(decode.pagination);
                    $("#tbl_user_account").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
           }
           $('#processing-modal').modal('hide');
        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
             $('#processing-modal').modal('hide');
            return;
        }
    });
}

function search_userTyping() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    var value = $('#searchValue').val();
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'search_user',
            value: value
        },
        success: function(response) {
            var decode = response;
            $('#tbl_user_account tbody > tr').remove();
            for (var i = 0; i < decode.users.length; i++) {
                var row = decode.users;

                var html = '<tr class="odd">\
                            <td>' + row[i].FullName + '</td>\
                            <td>' + row[i].UserName + '</td>\
                            <td>' + row[i].GroupName + '</td>\
                            <td class=" ">\
                              <div class="text-right">\
                                <a class="edit-usersaccount-icon btn btn-success btn-xs" data-id="' + row[i].UserAccountID + '">\
                                  <i class="icon-pencil"></i>\
                                </a>\
                                <a class="remove-usersaccount-icon btn btn-danger btn-xs" data-id="' + row[i].UserAccountID + '">\
                                  <i class="icon-remove"></i>\
                                </a>\
                              </div>\
                            </td>\
                        </tr>';
                $("#tbl_user_account tbody").append(html);
            }
            $("#tbl_user_account").addClass('tablesorter');

            var resort = true;
            $("table").trigger("update", [resort]);
        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}

function search_user_group() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    var value = $('#searchValue1').val();

    $('#processing-modal').modal('show');
    $('#tbl_user_group tbody > tr').remove();
    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'search_user_group',
            value: value
        },
        success: function(response) {
            var decode = response;

            if(decode){
                if(decode.groups.length > 0){
                    $("#cboUserGroup").empty();
                    for (var i = 0; i < decode.groups.length; i++) {
                        var row = decode.groups;

                        var html = '<tr>\
                                    <td>' + row[i].GroupName + '</td>\
                                    <td>' + row[i].Desc + '</td>\
                                    <td class=" ">\
                                      <div class="text-right">\
                                        <a class="edit-group-icon btn btn-success btn-xs" data-id="' + row[i].GroupID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-group-icon btn btn-danger btn-xs" data-id="' + row[i].GroupID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_user_group tbody").append(html);

                        var html2 = '<option id="' + row[i].GroupID + '" value="' + row[i].GroupID + '">' + row[i].GroupName + '</option>';
                        $("#cboUserGroup").append(html2);
                    }
                     $('#pagination').html(decode.pagination);
                     $("#tbl_user_group").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }
            $('#processing-modal').modal('hide');
        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            $('#processing-modal').modal('hide');
            return;
        }
    });
}

function search_user_groupTyping() {
    var ipaddress = sessionStorage.getItem("ipaddress");
    var value = $('#searchValue1').val();

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'search_user_group',
            value: value
        },
        success: function(response) {
            var decode = response;
            $('#tbl_user_group tbody > tr').remove();
            $("#cboUserGroup").empty();
            for (var i = 0; i < decode.groups.length; i++) {
                var row = decode.groups;

                var html = '<tr>\
                            <td>' + row[i].GroupName + '</td>\
                            <td>' + row[i].Desc + '</td>\
                            <td class=" ">\
                              <div class="text-right">\
                                <a class="edit-group-icon btn btn-success btn-xs" data-id="' + row[i].GroupID + '">\
                                  <i class="icon-pencil"></i>\
                                </a>\
                                <a class="remove-group-icon btn btn-danger btn-xs" data-id="' + row[i].GroupID + '">\
                                  <i class="icon-remove"></i>\
                                </a>\
                              </div>\
                            </td>\
                        </tr>';
                $("#tbl_user_group tbody").append(html);

                var html2 = '<option id="' + row[i].GroupID + '" value="' + row[i].GroupID + '">' + row[i].GroupName + '</option>';
                $("#cboUserGroup").append(html2);
            }
            $("#tbl_user_group").addClass('tablesorter');

            var resort = true;
            $("table").trigger("update", [resort]);
        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}


$('#Userpagination').on('click', '.page-numbers', function() {
    var page = $(this).attr('data-id');

    fetch_all_student(page);
});

$('#Grouppagination').on('click', '.page-numbers', function() {
    var page = $(this).attr('data-id');

    fetch_all_user_group(page);
});


function fetch_all_user(page) {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $('#processing-modal').modal('show');
    $('#tbl_user_account tbody > tr').remove();

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select_all_user',
            page: page
        },
        success: function(response) {
            var decode = response;



            if (decode) {

                if (decode.users.length > 0) {
                    for (var i = 0; i < decode.users.length; i++) {
                        var row = decode.users;

                        var html = '<tr class="odd">\
                                    <td>' + row[i].FullName + '</td>\
                                    <td>' + row[i].UserName + '</td>\
                                    <td>' + row[i].GroupName + '</td>\
                                    <td class=" ">\
                                      <div class="text-right">\
                                        <a class="edit-usersaccount-icon btn btn-success btn-xs" data-id="' + row[i].UserAccountID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-usersaccount-icon btn btn-danger btn-xs" data-id="' + row[i].UserAccountID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_user_account tbody").append(html);
                    }
                    $('#Userpagination').html(decode.pagination);
                    $("#tbl_user_account").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }
            $('#processing-modal').modal('hide');
        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            $('#processing-modal').modal('hide');
            return;
        }
    });
}

function fetch_all_user_group(page) {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'select_all_user_group',
            page: page
        },
        success: function(response) {
            var decode = response;
            $('#tbl_user_group tbody > tr').remove();
            $("#cboUserGroup").empty();

            if (decode) {
                if (decode.groups.length > 0) {
                    for (var i = 0; i < decode.groups.length; i++) {
                        var row = decode.groups;

                        var html = '<tr>\
                                    <td>' + row[i].GroupName + '</td>\
                                    <td>' + row[i].Desc + '</td>\
                                    <td class=" ">\
                                      <div class="text-right">\
                                        <a class="edit-group-icon btn btn-success btn-xs" data-id="' + row[i].GroupID + '">\
                                          <i class="icon-pencil"></i>\
                                        </a>\
                                        <a class="remove-group-icon btn btn-danger btn-xs" data-id="' + row[i].GroupID + '">\
                                          <i class="icon-remove"></i>\
                                        </a>\
                                      </div>\
                                    </td>\
                                </tr>';
                        $("#tbl_user_group tbody").append(html);

                        var html2 = '<option id="' + row[i].GroupID + '" value="' + row[i].GroupID + '">' + row[i].GroupName + '</option>';
                        $("#cboUserGroup").append(html2);
                    }
                    $('#Grouppagination').html(decode.pagination);
                    $("#tbl_user_group").addClass('tablesorter');

                    var resort = true;
                    $("table").trigger("update", [resort]);
                }
            }


        },
        error: function(error) {
            $('#btn-save').button('reset');
            console.log("Error:");
            console.log(error.responseText);
            console.log(error.message);
            return;
        }
    });
}



function commandtoClearUserAccount() {
    $('#authenticity_token').val('');
    $('#txtFirstName').val('');
    $('#txtMiddleName').val('');
    $('#txtLastName').val('');
    $('#txtUsername').val('');
    $('#txtPassword').val('');
    $('#txtPassword2').val('');
}

function commandToClearUserGroup() {
    $('#authenticationGroupID').val('');
    $('#txtGroupName').val('');
    $('#txtDesc').val('');
    $('#chckDashBoard').prop("checked", false);
    $('#chckUserAcc').prop("checked", false);
    $('#chckCollege').prop("checked", false);
    $('#chckStudent').prop("checked", false);
    $('#chckParty').prop("checked", false);
    $('#chckCandidates').prop("checked", false);
    $('#chckElectoral').prop("checked", false);
    $('#chckAcademic').prop("checked", false);
    $('#chckElection').prop("checked", false);
}


function privilege(user) {
    if (user._Dashboard == 1) {
        $('#LIST li').eq(0).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(1).removeClass('hidden');
    }

    // if(user._College == 1){
    //     $('#LIST li').eq(2).removeClass('hidden');
    // }

    if (user._Student == 1) {
        $('#LIST li').eq(2).removeClass('hidden');
    }

    if (user._PartyList == 1) {
        $('#LIST li').eq(3).removeClass('hidden');
    }

    if (user._Candidates == 1) {
        $('#LIST li').eq(4).removeClass('hidden');
    }

    if (user._ElectoralPosition == 1) {
        $('#LIST li').eq(5).removeClass('hidden');
    }

    if (user._AcademicProgram == 1) {
        $('#LIST li').eq(6).removeClass('hidden');
    }

    if (user._ElectionConfig == 1) {
        $('#LIST li').eq(7).removeClass('hidden');
    }

    if (user._UsersAccount == 1) {
        $('#LIST li').eq(8).removeClass('hidden');
    }
}

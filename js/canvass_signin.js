$(document).ready(function() {
    var user = JSON.parse(window.localStorage['canvass_user'] || '{}');

    if (Object.keys(user).length > 0) {
        console.log('redirect to main');
        window.location.href = "canvassing.php";
    }

    $('#server').val(sessionStorage.getItem("ipaddress"));
});


function get_active_config() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'get_active_config'
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                window.localStorage['config'] = JSON.stringify(decode.config);
            } else {
                alert(decode.msg);
                return;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}


function login() {
    if (validate() == true) {

        var config = JSON.parse(window.localStorage['config'] || '{}');

        var ipaddress = sessionStorage.getItem("ipaddress");

        $('#processing-modal').modal('show');

        $.ajax({
            url: 'http://' + ipaddress + '/voting/API/index.php',
            async: true,
            type: 'POST',
            crossDomain: true,
            dataType: 'json',
            data: {
                command: 'checklogin',
                username: $('#username').val(),
                TermID: config.TermID
            },
            success: function(response) {
                var decode = response;
                console.log(decode);
                if (decode.success == true) {
                    $('#error_message').html(decode.msg);
                    $('#errModal').modal('show');
                    return false;
                } else {
                    $.ajax({
                        url: 'http://' + ipaddress + '/voting/API/index.php',
                        async: false,
                        type: 'POST',
                        crossDomain: true,
                        dataType: 'json',
                        data: {
                            command: 'main_login',
                            username: $('#username').val(),
                            password: $('#password').val()
                        },
                        success: function(response) {
                            var decode = response;
                            console.log(decode);
                            if (decode.success == true) {

                                $('#processing-modal').modal('hide');

                                window.localStorage['canvass_user'] = JSON.stringify(decode.user);

                                window.location.href = "canvassing.php";
                            } else {
                                $('#processing-modal').modal('hide');
                                $('#error_message').html(decode.msg);
                                $('#errModal').modal('show');
                                return;
                            }
                        },
                        error: function(error) {
                            console.log("Error:");
                            console.log(error);
                            $('#processing-modal').modal('hide');
                            $('#error_message').html(error);
                            $('#errModal').modal('show');
                            return;
                        }
                    });
                }
            },
            error: function(error) {
                console.log("Error:");
                console.log(error);
                empty = false;
                $('#processing-modal').modal('hide');
                return empty;
            }
        });


    }
}

function validate() {
    //Trim
    $('input[type="text"]').each(function() {
        $(this).val($(this).val().trim());
    });

    // Check if required field is empty
    var empty = false;

    if ($('#username').val() == '') {
        $('#password').next('span').text('Username is required.');
        alert('Username is required.');
        empty = true;
        return false;
    }

    if ($('#password').val() == '') {
        $('#password').next('span').text('Password is required.');
        alert('Password is required.');
        empty = true;
        return false;
    }

    if ($('#server').val() == '') {
        $('#server').next('span').text('IP Address is required.');
        alert('IP Address is required.');
        empty = true;
        return false;
    }

    sessionStorage.setItem("ipaddress", $('#server').val());

    get_active_config();


    console.log('return true');
    return true;
}

$(document).ready(function() {

    var user = JSON.parse(window.localStorage['canvass_user'] || '{}');

    $('#current_user').html(user.FullName+' ('+user.GroupName+')');

    get_active_config();
    getCandidacyPosition();
    getStatisticsOverview();

    $('#lstPositions li').removeClass('active');
    $('#lstPositions li:first').addClass("active");
});


function get_active_config() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'get_active_config'
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                window.localStorage['config'] = JSON.stringify(decode.config);
            } else {
                alert(decode.msg);
                return;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}

function getStatisticsOverview() {
    var config = JSON.parse(window.localStorage['config'] || '{}');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        // url: 'http://localhost/voting/API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getStatisticsOverview',
            TermID: config.TermID
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                window.localStorage['totalVoters'] = decode.totalVoters;
                window.localStorage['totalNotVoted'] = decode.totalNotVoted;
                window.localStorage['totalVoted'] = decode.totalVoted;

                var totalVoted = (window.localStorage['totalVoted'] || '{}');
                var totalVoters = (window.localStorage['totalVoters'] || '{}');
                var totalNotVoted = (window.localStorage['totalNotVoted'] || '{}');

                $('#announcement-voted-heading').html(totalVoted);
                $('#announcement-voters-heading').html(totalVoters);
                $('#announcement-notvoted-heading').html(totalNotVoted);

            } else {
                alert(decode.msg);
                return;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}


function getCandidacyPosition() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        // url: 'http://localhost/voting/API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getCandidacyPosition'
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                window.localStorage['positions'] = JSON.stringify(decode.positions);
                var len = decode.positions.length;
                $('#lstPositions').empty();
                if (len > 0) {

                    for (var i = 0; i < len; i++) {
                        var data = decode.positions[i];
                        var row = '<li data-id="' + data.PositionID + '"><a href="#"><i class="fa fa-angle-double-right"></i> ' + data.PositionName + '</a>';
                        $('#lstPositions').append(row);
                    }

                }

            } else {
                alert(decode.msg);
                return false;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return false;
        }
    });
}

$(document).on("click", "#lstPositions li", function() {

    var config = JSON.parse(window.localStorage['config'] || '{}');

    $('#lstPositions li').removeClass('active');
    $(this).addClass("active");
    $('#lblPosition').html($(this).text());

    getCandidatesByPositionID($(this).attr("data-id"), config.TermID);
});


function getCandidatesByPositionID(positionID, TermID) {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        // url: 'http://localhost/voting/API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getCandidatesCanvassingByPositionID',
            positionID: positionID,
            TermID: TermID
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {

                window.localStorage['candidates'] = JSON.stringify(decode.candidates);

                var len = decode.candidates.length;
                $('#tbl_candidate tbody > tr').remove();
                if (len > 0) {;
                    $('#processing-modal').modal('show');

                    for (var i = 0; i < len; i++) {
                        var data = decode.candidates[i];
                        var img = data.Photo;
                        var new_img;
                        if (img === 'W0JJTkFSWV0=' || img === '') {
                            new_img = '../../img/photo.jpg';
                        } else {
                            new_img = 'data:image/x-xbitmap;base64,' + img;
                        }
                        var row = '<tr>\
                                        <td class="table-profile">\
                                            <div class="table-image">\
                                                <img src="' + new_img + '" width="120px" class="img-responsive img-circle" alt="Generic placeholder thumbnail">\
                                            </div>\
                                            <div class="table-info">\
                                                <h4>' + data.CandidateName + '</h4>\
                                                <small>' + data.Program + '</small>\
                                                <small>' + data.Partylist + '</small>\
                                            </div>\
                                        </td>\
                                        <td class="table-score">' + data.VoteCount + '</td>\
                                        <td class="table-score">32.3%</td>\
                                    </tr>';
                        $("#tbl_candidate tbody").append(row);
                    }
                    $('#processing-modal').modal('hide');

                }

            } else {
                alert(decode.msg);
                return false;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return false;
        }
    });
}

$(document).ready(function() {
    var user = JSON.parse(window.localStorage['canvass_user'] || '{}');


    $('#current_user').html(user.FullName+' ('+user.GroupName+')');


    Date.prototype.yyyymmdd = function() {
      var yyyy = this.getFullYear().toString();
      var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
      var dd  = this.getDate().toString();
      return yyyy + "/" + (mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]); // padding
    };

    d = new Date();
    $('#date_now').html(d.yyyymmdd());

    get_active_config();
    getStatisticsOverview();
    getVotersActuallyVoted();
    getVotersNotVoted();
});


function get_active_config() {
    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        // url: 'http://localhost/voting/API/index.php',
        async: false,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'get_active_config'
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                window.localStorage['config'] = JSON.stringify(decode.config);
            } else {
                alert(decode.msg);
                return;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}

function getStatisticsOverview() {
    var config = JSON.parse(window.localStorage['config'] || '{}');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getStatisticsOverview',
            TermID: config.TermID
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                window.localStorage['totalVoters'] = decode.totalVoters;
                window.localStorage['totalNotVoted'] = decode.totalNotVoted;
                window.localStorage['totalVoted'] = decode.totalVoted;

                var totalVoted = (window.localStorage['totalVoted'] || '{}');
                var totalVoters = (window.localStorage['totalVoters'] || '{}');
                var totalNotVoted = (window.localStorage['totalNotVoted'] || '{}');

                $('#announcement-voted-heading').html(totalVoted);
                $('#announcement-voteds-heading').html(totalVoted);
                $('#announcement-voters-heading').html(totalVoters);

            } else {
                alert(decode.msg);
                return;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return;
        }
    });
}

function getVotersActuallyVoted() {
    var config = JSON.parse(window.localStorage['config'] || '{}');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $("#tbl_voters tbody > tr").remove();

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getVotersActuallyVoted',
            TermID: config.TermID
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                if (decode.students) {
                    for (var a = 0; a < decode.students.length; a++) {
                        var row = decode.students[a];
                        var html = '<tr>\
                                        <td> ' + row.StudentID + ' </td>\
                                        <td> ' + row.LastName + ' </td>\
                                        <td> ' + row.FirstName + ' </td>\
                                        <td> ' + row.MiddleInitial + ' </td>\
                                        <td>' + row.Course + '</td>\
                                    </tr>';
                            $("#tbl_voters tbody").append(html);
                    }
                }
            } else {
                alert(decode.msg);
                return false;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return false;
        }
    });

}

function getVotersNotVoted() {
    var config = JSON.parse(window.localStorage['config'] || '{}');

    var ipaddress = sessionStorage.getItem("ipaddress");

    $("#tbl_voters2 tbody > tr").remove();

    $.ajax({
        url: 'http://' + ipaddress + '/voting/API/index.php',
        async: true,
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        data: {
            command: 'getVotersNotVoted',
            TermID: config.TermID
        },
        success: function(response) {
            var decode = response;
            console.log(decode);
            if (decode.success == true) {
                if (decode.students) {
                    for (var a = 0; a < decode.students.length; a++) {
                        var row = decode.students[a];
                        var html = '<tr>\
                                        <td> ' + row.StudentID + ' </td>\
                                        <td> ' + row.LastName + ' </td>\
                                        <td> ' + row.FirstName + ' </td>\
                                        <td> ' + row.MiddleInitial + ' </td>\
                                        <td>' + row.Course + '</td>\
                                    </tr>';
                            $("#tbl_voters2 tbody").append(html);
                    }
                }
            } else {
                alert(decode.msg);
                return false;
            }
        },
        error: function(error) {
            console.log("Error:");
            console.log(error);
            return false;
        }
    });

}

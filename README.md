# WEB BASE UNIVERSITY AND COLLEGE VOTING SYSTEM #

*A web based Voting System that supports University, College, High School and etc.*


## Git Workflow ##

**Clone the repo**

```
#!javascript

git clone url_repo
```


**Get the latest development branch**


```
#!javascript

git fetch
git checkout master
git pull origin master
```

**Create a local branch - This will be used to merge, pull, and push to the remote branch**


```
#!javascript

git checkout -b <your_name_branch> origin/master
```
**Create a feature branch. Please name your branch like "feature/login_form"**


```
#!javascript

git checkout -b origin/<your_name_branch>
```

**After you develop your feature branch (be sure you have commit all the code) checkout your local branch and pull from remote. Then merge you feature  branch **


```
#!javascript

git checkout <your_name_branch>
git pull origin development
git merge --no-ff <feature_branch>
```


Note: Be sure that check the code. If in case there is conflict please resolve before pushing it.

**Push your code to remote **


```
#!javascript

git push origin <your_name_branch>
```

**Create Merge/Pull request to master branch**

* Please Do Not Approve or merge your create merge request. It needs to review first before it will be merge

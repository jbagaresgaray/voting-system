<?php

function Insert_To_Ballot($data){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success'=>false,'msg'=>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	}else{
		$sql = array();
		foreach($data as $row){
			 $sql[] = '("'.$row['StudentID'].'","'.$row['CandidateID'].'","'.$row['TermID'].'")';
		}

		$query = 'INSERT INTO tbl_ballots(StudentID,CandidateID,TermID) VALUES '.implode(',', $sql);

		if ($result = $mysqli->query($query)) {
			print json_encode(array('success'=>true,'msg'=>'Thank you for participating the election'));
		}else{
		   	print json_encode(array('success'=>false,'msg'=>'Error while submitting your ballot. Please try again!'));
		}
	}
}

function login($username,$password,$TermID){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success'=>false,'msg'=>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	}else{
		$query = "SELECT S.StudentID,CONCAT(IFNULL(S.LastName,''),', ',IFNULL(S.FirstName,''),' ',IFNULL(S.MiddleInitial,''),'.') AS Fullname,S.Gender,P.ProgName,P.ShortName,P.ProgID,C.CollegeCode,C.CollegeName,C.CollegeID,S.TermID,S.Password
				FROM tbl_student S INNER JOIN tbl_acad_programs P ON P.ProgID = S.ProgID LEFT JOIN tbl_college C ON C.CollegeID= S.CollegeID WHERE S.StudentID='$username' AND S.Password='$password' AND TermID='$TermID' LIMIT 1;";
		$data = array();
		if ($result = $mysqli->query($query)) {
		    if($row = $result->fetch_array(MYSQLI_ASSOC)){
		    	print json_encode(array('success'=>true,'msg'=>'','student'=>$row));
		    }else{
		    	print json_encode(array('success'=>false,'msg'=>'Invalid Username and Password or Student ID is currently not enrolled during this semester'));
		    }
		    $result->free();
		}else{
			print json_encode(array('success'=>false,'msg'=>'Invalid Username and Password or Student ID is currently not enrolled during this semester'));
		}
	}
}

function checklogin($StudentID,$TermID) {
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success'=>false,'msg'=>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	}else{
		$query = "SELECT * FROM tbl_ballots WHERE StudentID ='$StudentID' AND TermID='$TermID' LIMIT 1;";
		if ($result = $mysqli->query($query)) {
		    if($row = $result->fetch_array(MYSQLI_ASSOC)){
		    	print json_encode(array('success'=>true,'msg'=>'Sorry You Voted Already!'));
		    }else{
		    	print json_encode(array('success'=>false,'msg'=>''));
		    }
		}else{
			print json_encode(array('success'=>false,'msg'=>''));
		}
	}
}

function getCandidatesByPositionID($positionID,$TermID){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success'=>false,'msg'=>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	}else{
		// $query = "SELECT fnCandidateName(CandidateID) AS CandidateName,CandidateID,fnPositionName(PositionID) AS Position,PositionID,fnCollegeCode(CollegeID) AS College,CollegeID,fnPartyName(PartyID) AS Partylist,PartyID, Photo FROM candidates WHERE PositionID='$positionID' AND AcademicYear='$TermID' ORDER BY fnCandidateName(CandidateID);";
		$query = "SELECT CONCAT(IFNULL(C.LastName,''),', ',IFNULL(C.FirstName,''),' ',IFNULL(C.MiddleInitial,'')) AS CandidateName,C.CandidateID,
		    (SELECT PositionName FROM tbl_positions WHERE PositionID=C.PositionID LIMIT 1) AS Position,C.PositionID,
		    (SELECT CollegeName FROM tbl_college WHERE CollegeID=C.CollegeID LIMIT 1) AS College,C.CollegeID,
		    (SELECT PartyName FROM tbl_party WHERE PartyID=C.PartyID LIMIT 1) AS Partylist,C.PartyID,
			(SELECT ProgName  FROM tbl_acad_programs WHERE ProgID=C.ProgramID LIMIT 1) AS Course,C.ProgramID,C.Photo
			FROM tbl_candidates C WHERE PositionID='$positionID' AND TermID='$TermID' ORDER BY C.LastName;";
		$data = array();
		$data2 = array();
		if ($result = $mysqli->query($query)) {
		    while($row = $result->fetch_array(MYSQLI_ASSOC)){

		    	$data2= array('CandidateID'=>$row['CandidateID'],
		    		'Photo'=> base64_encode($row['Photo']),
		    		'CandidateName'=>$row['CandidateName'],
		    		'Position'=>$row['Position'],
		    		'PositionID' =>$row['PositionID'],
		    		'Partylist'=>$row['Partylist'],
		    		'PartyID'=>$row['PartyID'],
		    		'College'=>$row['College'],
		    		'CollegeID'=>$row['CollegeID']);

		    	array_push($data, $data2);
		    }
		    print json_encode(array('success'=>true,'msg'=>'','candidates'=>$data));
		    $result->free();
		}
	}
}

function getCandidatesByPositionIDByCollegeID($positionID,$CollegeID,$TermID){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success'=>false,'msg'=>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	}else{
		$query = "SELECT CONCAT(IFNULL(C.LastName,''),', ',IFNULL(C.FirstName,''),' ',IFNULL(C.MiddleInitial,''))  AS CandidateName,C.CandidateID,
		    (SELECT PositionName FROM tbl_positions WHERE PositionID=C.PositionID LIMIT 1) AS Position,C.PositionID,
		    (SELECT CollegeName FROM tbl_college WHERE CollegeID=C.CollegeID LIMIT 1) AS College,C.CollegeID,
		    (SELECT PartyName FROM tbl_party WHERE PartyID=C.PartyID LIMIT 1) AS Partylist,C.PartyID,
			(SELECT ProgName  FROM tbl_acad_programs WHERE ProgID=C.ProgramID LIMIT 1) AS Course,C.ProgramID,C.Photo
			FROM tbl_candidates C WHERE PositionID='$positionID' AND C.CollegeID='$CollegeID' AND TermID='$TermID' ORDER BY C.LastName;";
		$data = array();
		$data2 = array();
		if ($result = $mysqli->query($query)) {
		    while($row = $result->fetch_array(MYSQLI_ASSOC)){

		    	$data2= array('CandidateID'=>$row['CandidateID'],
		    		'Photo'=> base64_encode($row['Photo']),
		    		'CandidateName'=>$row['CandidateName'],
		    		'Position'=>$row['Position'],
		    		'PositionID' =>$row['PositionID'],
		    		'Partylist'=>$row['Partylist'],
		    		'PartyID'=>$row['PartyID'],
		    		'College'=>$row['College'],
		    		'CollegeID'=>$row['CollegeID']);

		    	array_push($data, $data2);
		    }
		    print json_encode(array('success'=>true,'msg'=>'','candidates'=>$data));
		    $result->free();
		}
	}
}

function getCandidatesByPositionIDByCourseID($positionID,$ProgramID,$TermID){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success'=>false,'msg'=>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	}else{
		$query = "SELECT CONCAT(IFNULL(C.LastName,''),', ',IFNULL(C.FirstName,''),' ',IFNULL(C.MiddleInitial,''))  AS CandidateName,C.CandidateID,
		    (SELECT PositionName FROM tbl_positions WHERE PositionID=C.PositionID LIMIT 1) AS Position,C.PositionID,
		    (SELECT CollegeName FROM tbl_college WHERE CollegeID=C.CollegeID LIMIT 1) AS College,C.CollegeID,
		    (SELECT PartyName FROM tbl_party WHERE PartyID=C.PartyID LIMIT 1) AS Partylist,C.PartyID,
			(SELECT ProgName  FROM tbl_acad_programs WHERE ProgID=C.ProgramID LIMIT 1) AS Course,C.ProgramID,C.Photo
			FROM tbl_candidates C WHERE PositionID='$positionID' AND C.ProgramID='$ProgramID' AND TermID='$TermID' ORDER BY C.LastName;";
		$data = array();
		$data2 = array();
		if ($result = $mysqli->query($query)) {
		    while($row = $result->fetch_array(MYSQLI_ASSOC)){

		    	$data2= array('CandidateID'=>$row['CandidateID'],
		    		'Photo'=> base64_encode($row['Photo']),
		    		'CandidateName'=>$row['CandidateName'],
		    		'Position'=>$row['Position'],
		    		'PositionID' =>$row['PositionID'],
		    		'Partylist'=>$row['Partylist'],
		    		'PartyID'=>$row['PartyID'],
		    		'College'=>$row['College'],
		    		'CollegeID'=>$row['CollegeID']);

		    	array_push($data, $data2);
		    }
		    print json_encode(array('success'=>true,'msg'=>'','candidates'=>$data));
		    $result->free();
		}
	}
}

function getCandidacyPosition(){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success'=>false,'msg'=>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	}else{
		$query = "SELECT * FROM tbl_positions;";
		$data = array();
		if ($result = $mysqli->query($query)) {
		    while($row = $result->fetch_array(MYSQLI_ASSOC)){
		    	array_push($data, $row);
		    }
		    print json_encode(array('success'=>true,'msg'=>'','positions'=>$data));
		    $result->free();
		}
	}
}
?>

<?php
	function getCandidatesCanvassingByPositionID($positionID,$TermID){
		$mysqli = new mysqli("localhost", "root", "", "voting");
		if ($mysqli->connect_errno) {
		    print json_encode(array('success'=>false,'msg'=>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
		}else{
			$query = "SELECT CONCAT(LastName,', ',FirstName,' ',MiddleName) AS CandidateName,C.CandidateID,
			(SELECT PositionName FROM tbl_positions WHERE PositionID = C.PositionID LIMIT 1) AS Position,C.PositionID,
			(SELECT CollegeName FROM tbl_college WHERE CollegeID=C.CollegeID LIMIT 1) AS College,C.CollegeID,
			(SELECT PartyName FROM tbl_party WHERE PartyID=C.PartyID LIMIT 1) AS Partylist,C.PartyID,
			(SELECT ProgName FROM tbl_acad_programs WHERE ProgID=C.ProgramID LIMIT 1) AS Program,C.ProgramID,
			C.Photo,
			(SELECT COUNT(*) FROM tbl_ballots WHERE CandidateID=C.CandidateID AND TermID=C.TermID) AS VoteCount
			FROM tbl_candidates C WHERE C.PositionID='$positionID' AND C.TermID='$TermID'
			ORDER BY C.LastName";

			$data = array();
			$data2 = array();
			if ($result = $mysqli->query($query)) {
			    while($row = $result->fetch_array(MYSQLI_ASSOC)){

			    	$data2= array('CandidateID'=>$row['CandidateID'],
			    		'Photo'=> base64_encode($row['Photo']),
			    		'CandidateName'=>$row['CandidateName'],
			    		'Position'=>$row['Position'],
			    		'PositionID' =>$row['PositionID'],
			    		'Partylist'=>$row['Partylist'],
			    		'PartyID'=>$row['PartyID'],
			    		'College'=>$row['College'],
			    		'CollegeID'=>$row['CollegeID'],
			    		'Program'=>$row['Program'],
			    		'ProgramID'=>$row['ProgramID'],
			    		'VoteCount'=>$row['VoteCount']);

			    	array_push($data, $data2);
			    }
			    print json_encode(array('success'=>true,'msg'=>'','candidates'=>$data));
			    $result->free();
			}
		}
	}

	function getStatisticsOverview($TermID){
		$mysqli = new mysqli("localhost", "root", "", "voting");
		if ($mysqli->connect_errno) {
		    print json_encode(array('success'=>false,'msg'=>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
		}else{
			$totalVoters = 0;
			$totalNotVoted = 0;
			$totalVoted = 0;

			if ($result = $mysqli->query("SELECT COUNT(*) AS totalVoters FROM tbl_student WHERE TermID=$TermID;")) {
			    if($row = $result->fetch_array(MYSQLI_ASSOC)){
			    	$totalVoters= $row['totalVoters'];
			    }else{
			    	$totalVoters= 0;
			    }
			    $result->free();
			}

			if ($result1 = $mysqli->query("SELECT COUNT(*) AS totalVoted FROM (SELECT DISTINCT StudentID FROM tbl_ballots WHERE TermID=$TermID) tbl_ballots;")) {
			    if($row1 = $result1->fetch_array(MYSQLI_ASSOC)){
			    	$totalVoted= $row1['totalVoted'];
			    }else{
			    	$totalVoters= 0;
			    }
			    $result1->free();
			}

			$totalNotVoted = $totalVoters - $totalVoted;

			print json_encode(array('success'=>true,
				'totalVoters'=>$totalVoters,
				'totalNotVoted'=>$totalNotVoted,
				'totalVoted'=>$totalVoted));
		}
	}

	function getCandidatesFinalCanvassing($TermID){
		$mysqli = new mysqli("localhost", "root", "", "voting");
		if ($mysqli->connect_errno) {
		    print json_encode(array('success'=>false,'msg'=>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
		}else{
			$query = "SELECT CONCAT(LastName,', ',FirstName,' ',MiddleName) AS CandidateName,C.CandidateID,
			P.PositionName AS Position,P.PositionID,
			(SELECT CollegeName FROM tbl_college WHERE CollegeID=C.CollegeID LIMIT 1) AS College,C.CollegeID,
			(SELECT PartyName FROM tbl_party WHERE PartyID=C.PartyID LIMIT 1) AS Partylist,C.PartyID,
			(SELECT ProgName FROM tbl_acad_programs WHERE ProgID=C.ProgramID LIMIT 1) AS Program,C.ProgramID,
			C.Photo,
			(SELECT COUNT(*) FROM tbl_ballots WHERE CandidateID=C.CandidateID AND TermID=C.TermID) AS VoteCount
			FROM tbl_candidates C INNER JOIN tbl_positions P ON C.PositionID = P.PositionID
			WHERE C.TermID='$TermID' ORDER BY  P.SortOrder,VoteCount DESC;";

			$data = array();
			$data2 = array();
			if ($result = $mysqli->query($query)) {
			    while($row = $result->fetch_array(MYSQLI_ASSOC)){

			    	$data2= array('CandidateID'=>$row['CandidateID'],
			    		'Photo'=> base64_encode($row['Photo']),
			    		'CandidateName'=>$row['CandidateName'],
			    		'Position'=>$row['Position'],
			    		'PositionID' =>$row['PositionID'],
			    		'Partylist'=>$row['Partylist'],
			    		'PartyID'=>$row['PartyID'],
			    		'College'=>$row['College'],
			    		'CollegeID'=>$row['CollegeID'],
			    		'Program'=>$row['Program'],
			    		'ProgramID'=>$row['ProgramID'],
			    		'VoteCount'=>$row['VoteCount']);

			    	array_push($data, $data2);
			    }
			    print json_encode(array('success'=>true,'msg'=>'','candidates'=>$data));
			    $result->free();
			}else{
				print json_encode(array('success'=>false,'msg'=>'Error while retrieving Final Canvassing','candidates'=>$data));
			}
		}
	}

	function getCandidateWinners($TermID){
		$mysqli = new mysqli("localhost", "root", "", "voting");
		if ($mysqli->connect_errno) {
				print json_encode(array('success'=>false,'msg'=>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
		}else{
			$query = "SELECT CandidateName,CandidateID,Position,PositionID,College,CollegeID,Partylist,PartyID,Program,ProgramID,MAX(VoteCount) AS VoteCount
						FROM (
							SELECT CONCAT(C.LastName,', ',C.FirstName,' ',C.MiddleName) AS CandidateName,C.CandidateID,
							P.PositionName AS Position,P.PositionID,
							(SELECT CollegeName FROM tbl_college WHERE CollegeID=C.CollegeID LIMIT 1) AS College,C.CollegeID,
							(SELECT PartyName FROM tbl_party WHERE PartyID=C.PartyID LIMIT 1) AS Partylist,C.PartyID,
							(SELECT ProgName FROM tbl_acad_programs WHERE ProgID=C.ProgramID LIMIT 1) AS Program,C.ProgramID,
							C.Photo,
							(SELECT COUNT(*) FROM tbl_ballots WHERE CandidateID=C.CandidateID AND TermID=C.TermID) AS VoteCount
							FROM tbl_candidates C INNER JOIN tbl_positions P ON C.PositionID = P.PositionID
							WHERE C.TermID='$TermID'
							AND
								(
									lcase(replace(P.PositionName, ' ','')) <> 'governor'
								AND
									(
									lcase(replace(P.PositionName, ' ','')) <> 'vicegovernor' AND
									lcase(replace(P.PositionName, ' ','')) <> 'vice-governor'
									)
								)
							ORDER BY  SortOrder,VoteCount DESC
							) AS tbl
						Group By PositionID;";

				/*$query2 = "SELECT CandidateName,CandidateID,Position,PositionID,College,CollegeID,Partylist,PartyID,Program,ProgramID,MAX(VoteCount) AS VoteCount
					FROM (
						SELECT CONCAT(C.LastName,', ',C.FirstName,' ',C.MiddleInitial) AS CandidateName,C.CandidateID,
						P.PositionName AS Position,P.PositionID,
						(SELECT CollegeName FROM tbl_college WHERE CollegeID=C.CollegeID LIMIT 1) AS College,C.CollegeID,
						(SELECT PartyName FROM tbl_party WHERE PartyID=C.PartyID LIMIT 1) AS Partylist,C.PartyID,
						(SELECT ProgName FROM tbl_acad_programs WHERE ProgID=C.ProgramID LIMIT 1) AS Program,C.ProgramID,
						C.Photo,
						(SELECT COUNT(*) FROM tbl_ballots WHERE CandidateID=C.CandidateID AND TermID=C.TermID) AS VoteCount
						FROM tbl_candidates C INNER JOIN tbl_positions P ON C.PositionID = P.PositionID
						WHERE C.TermID='$TermID'
						AND
						(
							lcase(replace(P.PositionName, ' ','')) = 'governor'
						OR
							(
							lcase(replace(P.PositionName, ' ','')) = 'vicegovernor' OR
							lcase(replace(P.PositionName, ' ','')) = 'vice-governor'
							)
						)
						ORDER BY  SortOrder,VoteCount DESC
						) AS tbl
					Group By PositionID,ProgramID";*/

					$query2 = "SELECT CandidateName,CandidateID,Position,PositionID,Program,ProgramID,College,CollegeID,Partylist,PartyID,
								MAX(VoteCount) AS VoteCount
								FROM (
									SELECT CONCAT(C.LastName,', ',C.FirstName,' ',C.MiddleName) AS CandidateName,C.CandidateID,
									P.PositionName AS Position,P.PositionID,P.SortOrder,
									(SELECT CollegeName FROM tbl_college WHERE CollegeID=C.CollegeID LIMIT 1) AS College,C.CollegeID,
									(SELECT PartyName FROM tbl_party WHERE PartyID=C.PartyID LIMIT 1) AS Partylist,C.PartyID,
									(SELECT ProgName FROM tbl_acad_programs WHERE ProgID=C.ProgramID LIMIT 1) AS Program,C.ProgramID,
									C.Photo,
									(SELECT COUNT(*) FROM tbl_ballots WHERE CandidateID=C.CandidateID AND TermID=C.TermID) AS VoteCount
									FROM tbl_candidates C INNER JOIN tbl_positions P ON C.PositionID = P.PositionID
									WHERE C.TermID='$TermID'
									AND
									(
										lcase(replace(P.PositionName, ' ','')) = 'governor'
									OR
										(
										lcase(replace(P.PositionName, ' ','')) = 'vicegovernor' OR
										lcase(replace(P.PositionName, ' ','')) = 'vice-governor'
										)
									)
									) AS tbl
								Group By PositionID,ProgramID
								ORDER BY SortOrder,VoteCount DESC";

			// print_r($query);die();

			$data = array();
			$data2 = array();

			$data3 = array();
			$data4 = array();

			$result = $mysqli->query($query);
			$result2 = $mysqli->query($query2);

			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$data2= array('CandidateID'=>$row['CandidateID'],
					// 'Photo'=> base64_encode($row['Photo']),
					'CandidateName'=>$row['CandidateName'],
					'Position'=>$row['Position'],
					'PositionID' =>$row['PositionID'],
					'Partylist'=>$row['Partylist'],
					'PartyID'=>$row['PartyID'],
					'College'=>$row['College'],
					'CollegeID'=>$row['CollegeID'],
					'Program'=>$row['Program'],
					'ProgramID'=>$row['ProgramID'],
					'VoteCount'=>$row['VoteCount']);

				array_push($data, $data2);
			}

			while($row = $result2->fetch_array(MYSQLI_ASSOC)){
				$data3= array('CandidateID'=>$row['CandidateID'],
					// 'Photo'=> base64_encode($row['Photo']),
					'CandidateName'=>$row['CandidateName'],
					'Position'=>$row['Position'],
					'PositionID' =>$row['PositionID'],
					'Partylist'=>$row['Partylist'],
					'PartyID'=>$row['PartyID'],
					'College'=>$row['College'],
					'CollegeID'=>$row['CollegeID'],
					'Program'=>$row['Program'],
					'ProgramID'=>$row['ProgramID'],
					'VoteCount'=>$row['VoteCount']);

				array_push($data4, $data3);
			}

			print json_encode(array('success'=>true,'msg'=>'','candidates'=>$data,'candidates2'=>$data4));
			$result->free();

		}
	}

	function getVotersActuallyVoted($TermID){
		$mysqli = new mysqli("localhost", "root", "", "voting");
		if ($mysqli->connect_errno) {
				print json_encode(array('success'=>false,'msg'=>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
		}else{
			$query = "SELECT S.*,(SELECT ProgName  FROM tbl_acad_programs WHERE ProgID=S.ProgID LIMIT 1) AS Course FROM tbl_student S WHERE S.StudentID IN(SELECT DISTINCT StudentID FROM tbl_ballots WHERE TermID = '$TermID');";
			$data = array();
			if ($result = $mysqli->query($query)) {
					while($row = $result->fetch_array(MYSQLI_ASSOC)){
							array_push($data, $row);
					}
					print json_encode(array('success'=>true,'msg'=>'','students'=>$data));
					$result->free();
			}else{
				print json_encode(array('success'=>false,'msg'=>'Error while retrieving Voters Canvassing Report','students'=>$data));
			}
		}
	}

	function getVotersNotVoted($TermID){
		$mysqli = new mysqli("localhost", "root", "", "voting");
		if ($mysqli->connect_errno) {
				print json_encode(array('success'=>false,'msg'=>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
		}else{
			$query = "SELECT S.*,(SELECT ProgName  FROM tbl_acad_programs WHERE ProgID=S.ProgID LIMIT 1) AS Course FROM tbl_student S WHERE S.StudentID NOT IN(SELECT DISTINCT StudentID FROM tbl_ballots WHERE TermID = '$TermID');";
			$data = array();
			if ($result = $mysqli->query($query)) {
					while($row = $result->fetch_array(MYSQLI_ASSOC)){
							array_push($data, $row);
					}
					print json_encode(array('success'=>true,'msg'=>'','students'=>$data));
					$result->free();
			}else{
				print json_encode(array('success'=>false,'msg'=>'Error while retrieving Voters Canvassing Report','students'=>$data));
			}
		}
	}

?>

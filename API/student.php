<?php

function insert_student($data){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{
		if ($stmt = $mysqli->prepare('INSERT INTO tbl_student(StudentID,FirstName,LastName,MiddleInitial,Gender,ProgID,TermID,Password) VALUES(?,?,?,?,?,?,?,?)')){
			$stmt->bind_param("ssssssss", $data[0]['Student_ID'],$data[0]['Student_FirstName'],$data[0]['Student_LastName'],$data[0]['Student_MiddleInitial'],$data[0]['Student_Gender'],$data[0]['Student_ProgID'],$data[0]['Student_TermID'],$data[0]['Student_Password']);
			$stmt->execute();
			$stmt->close();
			print json_encode(array('success' =>true,'msg' =>'Record successfully saved'));
		}else{
			print json_encode(array('success' =>false,'msg' =>"Error message: %s\n", $mysqli->error));
		}
	}
}

function update_student($student_id,$data){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{
		if ($stmt = $mysqli->prepare('UPDATE tbl_student SET FirstName=?,LastName=?,MiddleInitial=?,Gender=?,ProgID=?,TermID=?,Password=? WHERE StudentID= "'.$student_id.'" ')){
			$stmt->bind_param("sssssss",$data[0]['Student_FirstName'],$data[0]['Student_LastName'],$data[0]['Student_MiddleInitial'],$data[0]['Student_Gender'],$data[0]['Student_ProgID'],$data[0]['Student_TermID'],$data[0]['Student_Password']);
			$stmt->execute();
			$stmt->close();
			print json_encode(array('success' =>true,'msg' =>'Record successfully updated'));
		}else{
			print json_encode(array('success' =>false,'msg' =>"Error message: %s\n", $mysqli->error));
		}
	}
}

function delete_student($student_id,$TermID){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{
		if($stmt = $mysqli->prepare("DELETE FROM tbl_student WHERE StudentID =? AND TermID=?")){
			$stmt->bind_param("ss", $student_id,$TermID);
			$stmt->execute();
			$stmt->close();
			print json_encode(array('success' =>true,'msg' =>'Record successfully deleted'));
		}else{
			print json_encode(array('success' =>false,'msg' =>"Error message: %s\n", $mysqli->error));
		}
	}
}

function select_all_student($TermID,$page){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{

		$limit = 10;
		$adjacent = 3;

		if($page==1){
		   $start = 0;
		}else{
		  $start = ($page-1)*$limit;
		}

		$query1="SELECT S.ID,S.StudentID,CONCAT(IFNULL(S.LastName,''),', ',IFNULL(S.FirstName,''),' ',IFNULL(S.MiddleInitial,''),'.') AS Fullname,S.LastName,S.FirstName,S.MiddleInitial,S.Gender,S.Password,
				(SELECT ProgName  FROM tbl_acad_programs WHERE ProgID=S.ProgID LIMIT 1) AS Course,S.ProgID,
				(SELECT CollegeCode FROM tbl_college WHERE CollegeID=(SELECT CollegeID  FROM tbl_acad_programs WHERE ProgID=S.ProgID)  LIMIT 1) AS College,S.CollegeID,
				(SELECT CONCAT(ElectionName,' (',SchoolYear,')')  FROM tbl_configuration WHERE TermID =S.TermID LIMIT 1) AS ElectionTerm,S.TermID
				FROM tbl_student S WHERE S.TermID='$TermID' ORDER BY Fullname;";

		$result1 = $mysqli->query($query1);
		$rows = $result1->num_rows;

		$query="SELECT S.ID,S.StudentID,CONCAT(IFNULL(S.LastName,''),', ',IFNULL(S.FirstName,''),' ',IFNULL(S.MiddleInitial,''),'.') AS Fullname,S.LastName,S.FirstName,S.MiddleInitial,S.Gender,S.Password,
				(SELECT ProgName  FROM tbl_acad_programs WHERE ProgID=S.ProgID LIMIT 1) AS Course,S.ProgID,
				(SELECT CollegeCode FROM tbl_college WHERE CollegeID=(SELECT CollegeID  FROM tbl_acad_programs WHERE ProgID=S.ProgID)  LIMIT 1) AS College,S.CollegeID,
				(SELECT CONCAT(ElectionName,' (',SchoolYear,')')  FROM tbl_configuration WHERE TermID =S.TermID LIMIT 1) AS ElectionTerm,S.TermID
				FROM tbl_student S WHERE S.TermID='$TermID' ORDER BY Fullname LIMIT $start, $limit;";

		$mysqli->set_charset("utf8");
		$result = $mysqli->query($query);
		$data = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			array_push($data ,$row);
		}

		$paging = pagination($limit,$adjacent,$rows,$page);

		print json_encode(array('success' =>true,'students' =>$data,'pagination'=>$paging));
	}
}

function get_student($student_id,$TermID){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{
		$query ="SELECT * FROM tbl_student WHERE StudentID='$student_id' AND TermID='$TermID';";
		$result = $mysqli->query($query);
		if($row = $result->fetch_array(MYSQLI_ASSOC)){
			print json_encode(array('success' =>true,'student' =>$row));
		}else{
			print json_encode(array('success' =>false,'msg' =>"No record found!"));
		}
	}
}

function search_student($value,$TermID,$page){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{

		$limit = 10;
		$adjacent = 3;

		if($page==1){
		   $start = 0;
		}else{
		  $start = ($page-1)*$limit;
		}

		$query1="SELECT S.ID,S.StudentID,CONCAT(IFNULL(S.LastName,''),', ',IFNULL(S.FirstName,''),' ',IFNULL(S.MiddleInitial,''),'.') AS Fullname,S.LastName,S.FirstName,S.MiddleInitial,S.Gender,S.Password,
				(SELECT ProgName  FROM tbl_acad_programs WHERE ProgID=S.ProgID LIMIT 1) AS Course,S.ProgID,
				(SELECT CollegeCode FROM tbl_college WHERE CollegeID=(SELECT CollegeID  FROM tbl_acad_programs WHERE ProgID=S.ProgID) LIMIT 1) AS College,S.CollegeID,
				(SELECT CONCAT(ElectionName,' (',SchoolYear,')')  FROM tbl_configuration WHERE TermID =S.TermID LIMIT 1) AS ElectionTerm,S.TermID
				FROM tbl_student S WHERE StudentID LIKE '%$value%' OR S.FirstName LIKE '%$value%' AND  S.TermID=$TermID";

		$result1 = $mysqli->query($query1);
		$rows = $result1->num_rows;

		$query="SELECT S.ID,S.StudentID,CONCAT(IFNULL(S.LastName,''),', ',IFNULL(S.FirstName,''),' ',IFNULL(S.MiddleInitial,''),'.') AS Fullname,S.LastName,S.FirstName,S.MiddleInitial,S.Gender,S.Password,
				(SELECT ProgName  FROM tbl_acad_programs WHERE ProgID=S.ProgID LIMIT 1) AS Course,S.ProgID,
				(SELECT CollegeCode FROM tbl_college WHERE CollegeID=(SELECT CollegeID  FROM tbl_acad_programs WHERE ProgID=S.ProgID)  LIMIT 1) AS College,S.CollegeID,
				(SELECT CONCAT(ElectionName,' (',SchoolYear,')')  FROM tbl_configuration WHERE TermID =S.TermID LIMIT 1) AS ElectionTerm,S.TermID
				FROM tbl_student S WHERE StudentID LIKE '%$value%' OR S.FirstName LIKE '%$value%' AND  S.TermID=$TermID LIMIT $start, $limit;";

		$result = $mysqli->query($query);
		$data = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			array_push($data ,$row);
		}

		$paging = pagination($limit,$adjacent,$rows,$page);

		print json_encode(array('success' =>true,'students' =>$data,'pagination'=>$paging));
	}
}


function login_voter($student_id,$Password){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{
		$query ="SELECT * FROM tbl_student WHERE StudentID=$student_id AND TermID=$TermID;";
		$result = $mysqli->query($query);
		if($row = $result->fetch_array(MYSQLI_ASSOC)){
			print json_encode(array('success' =>true,'student' =>$row));
		}else{
			print json_encode(array('success' =>false,'msg' =>"No record found!"));
		}
	}
}

function generate_password($TermID){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{
		$query ="SELECT * FROM tbl_student WHERE TermID=$TermID;";
		$result = $mysqli->query($query);
		$count =0;
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$password = generatePassword(6,1);
			$sql = 'UPDATE tbl_student SET Password="'.$password.'" WHERE StudentID= "'.$row['StudentID'].'" AND TermID='.$TermID.';';
			if ($stmt = $mysqli->query($sql)){
				$count = $count + 1;
			}
		}
		if($count > 0){
			print json_encode(array('success' =>true,'student' =>$count ,'msg' =>'Successfully generated password for students'));
		}else{
			print json_encode(array('success' =>false,'msg' =>"An unknown error occur while generating password"));
		}
	}
}


function update_password($student_id,$termID,$password){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    return false;
	}else{
		if ($stmt = $mysqli->prepare('UPDATE tbl_student SET Password=? WHERE StudentID= "'.$student_id.'" AND TermID="'.$termID.'"')){
			$stmt->bind_param("s",$password);
			$stmt->execute();
			return true;
		}else{
			return false;
		}
	}
}

function Print_Student(){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{

		$query="SELECT S.ID,S.StudentID,CONCAT(S.LastName,', ',S.FirstName,' ',S.MiddleInitial,'.') AS Fullname,S.LastName,S.FirstName,S.MiddleInitial,S.Gender,S.Password,
				(SELECT ProgName  FROM tbl_acad_programs WHERE ProgID=S.ProgID LIMIT 1) AS Course,S.ProgID,
				(SELECT CollegeCode FROM tbl_college WHERE CollegeID=(SELECT CollegeID  FROM tbl_acad_programs WHERE ProgID=S.ProgID) LIMIT 1) AS College,S.CollegeID,
				(SELECT CONCAT(ElectionName,' (',SchoolYear,')')  FROM tbl_configuration WHERE TermID =S.TermID LIMIT 1) AS ElectionTerm,S.TermID
				FROM tbl_student S;";

		$result = $mysqli->query($query);
		$data = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			array_push($data ,$row);
		}

		print json_encode(array('success' =>true,'students' =>$data));
	}
}

function checkStudent($Fullname){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{
		$query ="Select * from tbl_student Where CONCAT(LastName,', ',FirstName,' ',MiddleName) LIKE '%$Fullname%';";
		$result = $mysqli->query($query);
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				if(count($row) > 1){
				print json_encode(array('success' =>true));
			}else{
				print json_encode(array('success' =>false));
			}
		}

	}
}


function fetch_student($TermID){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{

		$query="SELECT S.ID,S.StudentID,CONCAT(IFNULL(S.LastName,''),', ',IFNULL(S.FirstName,''),' ',IFNULL(S.MiddleInitial,''),'.') AS Fullname,S.Gender,S.Password,
				(SELECT ProgName  FROM tbl_acad_programs WHERE ProgID=S.ProgID LIMIT 1) AS Course,S.ProgID,
				(SELECT CollegeCode FROM tbl_college WHERE CollegeID=(SELECT CollegeID  FROM tbl_acad_programs WHERE ProgID=S.ProgID)  LIMIT 1) AS College,S.CollegeID,
				(SELECT CONCAT(ElectionName,' (',SchoolYear,')')  FROM tbl_configuration WHERE TermID =S.TermID LIMIT 1) AS ElectionTerm,S.TermID
				FROM tbl_student S WHERE S.TermID='$TermID'";

		$mysqli->set_charset("utf8");
		$result = $mysqli->query($query);
		$data = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			array_push($data ,$row);
		}

		print json_encode(array('success' =>true,'students' =>$data));
	}
}

function import_student($data){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
			print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
			return;
	}else{
		foreach ($data as $key) {
				$stmt = $mysqli->prepare('INSERT INTO tbl_student(StudentID,FirstName,LastName,MiddleInitial,Gender,ProgID,TermID) VALUES(?,?,?,?,?,?,?)');
				$stmt->bind_param("sssssss", $key['Student_ID'],$key['Student_FirstName'],$key['Student_LastName'],$key['Student_MiddleInitial'],$key['Student_Gender'],$key['Student_ProgID'],$key['Student_TermID']);
				$stmt->execute();
				$stmt->close();
		}
		print json_encode(array('success' =>true,'msg' =>'Record successfully saved'));
	}
}

function getSelect2Student(){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{

		$query="SELECT S.ID,S.StudentID,CONCAT(IFNULL(S.LastName,''),', ',IFNULL(S.FirstName,''),' ',IFNULL(S.MiddleInitial,''),'.') AS Fullname,S.Gender,S.Password,
				(SELECT ProgName  FROM tbl_acad_programs WHERE ProgID=S.ProgID LIMIT 1) AS Course,S.ProgID,
				(SELECT CollegeCode FROM tbl_college WHERE CollegeID=(SELECT CollegeID  FROM tbl_acad_programs WHERE ProgID=S.ProgID)  LIMIT 1) AS College,S.CollegeID,
				(SELECT CONCAT(ElectionName,' (',SchoolYear,')')  FROM tbl_configuration WHERE TermID =S.TermID LIMIT 1) AS ElectionTerm,S.TermID
				FROM tbl_student S";

		$mysqli->set_charset("utf8");
		$result = $mysqli->query($query);
		$data = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			array_push($data ,$row);
		}

		print json_encode(array('success' =>true,'students' =>$data));

		file_put_contents('student.json', json_encode($data));
	}
}

function search_student2($value,$TermID){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{
		$query1="SELECT S.StudentID,CONCAT(IFNULL(S.LastName,''),', ',IFNULL(S.FirstName,''),' ',IFNULL(S.MiddleInitial,''),'.') AS Fullname
				FROM tbl_student S WHERE StudentID LIKE '%$value%' OR S.FirstName LIKE '%$value%' OR S.LastName LIKE '%$value%'  AND  S.TermID=$TermID";
		$result = $mysqli->query($query1);
		$data = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			array_push($data ,$row);
		}

		print json_encode(array('students' =>$data));
	}
}

?>

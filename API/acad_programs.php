<?php

function insert_acad_programs($data){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{



		if ($stmt = $mysqli->prepare('INSERT INTO tbl_acad_programs(ProgName,ShortName,ProgCode,CollegeID) VALUES(?,?,?,?)')){
			$stmt->bind_param("ssss", $data[0]['program_Name'],$data[0]['short_Name'],$data[0]['program_Code'],$data[0]['college_ID']);
			$stmt->execute();

			print json_encode(array('success' =>true,'msg' =>'Record successfully saved'));
		}else{
			print json_encode(array('success' =>false,'msg' =>"Error message: %s\n", $mysqli->error));
		}
	}
}

function update_acad_programs($Prog_ID,$data){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{
		if ($stmt = $mysqli->prepare('UPDATE tbl_acad_programs SET ProgName=?,ShortName=?,ProgCode=?,CollegeID=? WHERE ProgID="'.$Prog_ID.'"')){
			$stmt->bind_param("ssss", $data[0]['program_Name'],$data[0]['short_Name'],$data[0]['program_Code'],$data[0]['college_ID']);
			$stmt->execute();

			print json_encode(array('success' =>true,'msg' =>'Record successfully updated'));
		}else{
			print json_encode(array('success' =>false,'msg' =>"Error message: %s\n", $mysqli->error));
		}
	}
}

function delete_acad_programs($Prog_ID){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{
		if($stmt = $mysqli->prepare("DELETE FROM tbl_acad_programs WHERE ProgID =?")){
			$stmt->bind_param("s", $Prog_ID);
			$stmt->execute();
			$stmt->close();
			print json_encode(array('success' =>true,'msg' =>'Record successfully deleted'));
		}else{
			print json_encode(array('success' =>false,'msg' =>"Error message: %s\n", $mysqli->error));
		}
	}
}

function select_all_acad_programs($page){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{

		$limit = 10;
		$adjacent = 3;

		if($page==1){
		   $start = 0;
		}else{
		  $start = ($page-1)*$limit;
		}

		$query1 ="SELECT * FROM tbl_acad_programs tblAP LEFT JOIN tbl_college tblC ON tblC.CollegeID = tblAP.CollegeID;";

		$result1 = $mysqli->query($query1);
		$rows = $result1->num_rows;

		$query ="SELECT * FROM tbl_acad_programs tblAP LEFT JOIN tbl_college tblC ON tblC.CollegeID = tblAP.CollegeID LIMIT $start, $limit;";
		$mysqli->set_charset("utf8");
		$result = $mysqli->query($query);
		$data = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			array_push($data,$row);
		}

		$paging = pagination($limit,$adjacent,$rows,$page);
		print json_encode(array('success' =>true,'programs' =>$data,'pagination'=>$paging));
	}
}

function select_For_ComboCourse(){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{
		$query ="SELECT * FROM tbl_acad_programs tblAP LEFT JOIN tbl_college tblC ON tblC.CollegeID = tblAP.CollegeID;";
		$result = $mysqli->query($query);
		$data = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			array_push($data,$row);
		}
		print json_encode(array('success' =>true,'programs' =>$data));
	}
}

function get_acad_programs($Prog_ID){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{
		$query ="SELECT * FROM tbl_acad_programs tblAP LEFT JOIN tbl_college tblC ON tblC.CollegeID = tblAP.CollegeID WHERE tblAP.ProgID=$Prog_ID;";
		$result = $mysqli->query($query);
		if($row = $result->fetch_array(MYSQLI_ASSOC)){
			print json_encode(array('success' =>true,'program' =>$row));
		}else{
			print json_encode(array('success' =>false,'msg' =>"No record found!"));
		}
	}
}

function search_acad_programs($value,$page){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{
		$query ="SELECT * FROM tbl_acad_programs tblAP LEFT JOIN tbl_college tblC ON tblC.CollegeID = tblAP.CollegeID WHERE tblAP.ProgName LIKE '%$value%' OR tblAP.ShortName LIKE '%$value%' OR tblAP.ProgCode LIKE '%$value%';";
		$result = $mysqli->query($query);
		$data = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			array_push($data ,$row);
		}
		print json_encode(array('success' =>true,'programs' =>$data));
	}
}

function Print_Courses(){
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{

		$query="SELECT * FROM tbl_acad_programs tblAP LEFT JOIN tbl_college tblC ON tblC.CollegeID = tblAP.CollegeID;";

		$result = $mysqli->query($query);
		$data = array();
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			array_push($data ,$row);
		}

		print json_encode(array('success' =>true,'programs' =>$data));
	}
}

function checkCourses($ProgName){
	$newval = ltrim($ProgName);
	$mysqli = new mysqli("localhost", "root", "", "voting");
	if ($mysqli->connect_errno) {
	    print json_encode(array('success' =>false,'msg' =>"Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error));
	    return;
	}else{
		$query ="Select * from tbl_acad_programs Where ProgName = '$ProgName';";
		$result = $mysqli->query($query);
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				if(count($row) > 1){
				print json_encode(array('success' =>true));
			}else{
				print json_encode(array('success' =>false));
			}
		}

	}
}

?>

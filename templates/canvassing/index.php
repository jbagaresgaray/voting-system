<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Canvassing Login</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../../lib/bootstrap/css/bootstrap.css">

    <!-- Add custom CSS here -->
    <link rel="stylesheet" href="../../css/canvass_signin.css" />
    <link rel="stylesheet" href="../../lib/font-awesome/css/font-awesome.min.css">
</head>

<body>

    <div class="container">

      <form class="form-signin">
        <h2 class="form-signin-heading">
            <img src="../../img/logo.png" alt="Voting System" width="280px">
        </h2>
        <input type="text" class="form-control" placeholder="Username" name="username" id="username" required autofocus>
        <input type="password" class="form-control" placeholder="Password" name="password" id="password" required>
        <span class="help-inline"></span>
        <div class='form-group'>
            <div class='controls with-icon-over-input'>
                <input value="" placeholder="IP ADDRESS" class="form-control" data-rule-required="true" name="server" id="server" type="text" />
                <span class="help-inline"></span>
            </div>
        </div>
        <a href="javascript:login();"class="btn btn-lg btn-primary btn-block" type="submit">Sign in</a>
      </form>

    </div> <!-- /container -->

    <div class="modal modal-static fade" id="processing-modal" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-center">
                        <img src="../../img/loading.gif" class="icon" />
                        <h4>Processing...</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="errModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-danger">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">X</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">ERROR</h4>
                </div>
                <div class="modal-body">
                    <h1>WARNING: An error occured!</h1>
                    <blockquote>
                        <p id="error_message"></p>
                    </blockquote>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="../../lib/jquery/jquery-2.0.0.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="../../js/canvass_signin.js"></script>
    <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Print | Voting System</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../../lib/bootstrap/css/bootstrap.css">

    <!-- Add custom CSS here -->
    <link rel="stylesheet" href="../../css/final.css" />
    <link rel="stylesheet" href="../../lib/font-awesome/css/font-awesome.min.css">
</head>

<body>

    <!-- Sidebar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img src="../../img/logo.png" alt="Voting System" style="width:100px;">
            </a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="javascript:window.print()"><i class="fa fa-print"></i> Print</a>
                </li>
                <li><a href="canvassing.php"><i class="fa fa-times"></i> Close</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="text-center">
                    <h3>
                        <img src="../../img/logo.jpg" alt="Voting System" style="width:70px;">
                        Collegio de Sta. Ana de Victorias
                        <br>
                        <strong>Commission on Election</strong>
                    </h3>
                </div>
                <br>
                <div class="invoice-title">
                    <h3>
                        ELECTION CANVASS REPORT
                    </h3>
                    <h4 class="pull-right">
                        <span id="date_now"></span>
                    </h4>
                </div>
                <hr>
                <div class="row">
                    <div class="col-xs-6">
                        <address>
                            <strong>No. of Registered Voters:</strong>
                            <span id="announcement-voters-heading"></span>
                        </address>
                        <address>
                            <strong>No of Voters Actually Voted:</strong>
                            <span id="announcement-voted-heading"></span>
                        </address>
                        <address>
                            <strong>Total Valid Ballots Counted:</strong>
                            <span id="announcement-voteds-heading"></span>
                        </address>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div id="print_content"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-xs-offset-2">
                <div class="col-xs-3">

                    <hr>
                    <center>
                        <strong>VICE-CHAIRMAN</strong>
                        <br>
                        <small>(Signature over printed name)</small>
                    </center>
                </div>
                <div class="col-xs-3">
                    <hr>
                    <center>
                        <strong>CHAIRMAN</strong>
                        <br>
                        <small>(Signature over printed name)</small>
                    </center>
                </div>
                <div class="col-xs-3">
                    <hr>
                    <center>
                        <strong>MEMBER</strong>
                        <br>
                        <small>(Signature over printed name)</small>
                    </center>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- /#page-wrapper -->


    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="../../lib/jquery/jquery-2.0.0.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="../../js/ranking.js"></script>
    <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
</body>

</html>
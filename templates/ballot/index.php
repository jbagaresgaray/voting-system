<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Ballots | Voting System</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet"  href="../../lib/jquery.mobile-1.4.2/jquery.mobile-1.4.2.css">
	<link rel="stylesheet" href="../../lib/font-awesome/css/font-awesome.css" />
	<link rel="stylesheet" href="../../css/sample.css">
	<script type="text/javascript" src="../../lib/jquery/jquery-2.0.0.min.js"></script>
	<script src="../../js/index.js"></script>
	<script src="../../lib/jquery.mobile-1.4.2/jquery.mobile-1.4.2.js"></script>
</head>
<body>
<div data-role="page" class="jqm-demos jqm-home">

	<div data-role="header" class="jqm-header">
		<h2><img src="../../img/logo.png" alt="Voting System"></h2>
	</div><!-- /header -->

	<div role="main" class="ui-content ">
        <form role="form">
            <input id="username" type="text" placeholder="username">
            <input id="password" type="password" placeholder="password">
            <input id="server" type="text" placeholder="IP ADDRESS">
            <a href="javascript:login();" class="ui-btn ui-btn-c ui-corner-all">Sign in</a>
        </form>
	</div><!-- /content -->

	<div data-role="popup" id="errModal" data-overlay-theme="b" data-theme="b" data-dismissible="false" style="max-width:400px;">
	    <div data-role="header" data-theme="a">
	    <h1>ERROR</h1>
	    </div>
	    <div role="main" class="ui-content">
	        <h3 class="ui-title">WARNING: An error occurred!</h3>
            <blockquote>
                <p id="error_message"></p>
            </blockquote>
	        <a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-b" data-rel="back">OK</a>
	    </div>
	</div>
</div><!-- /page -->

<script type="text/javascript" src="../../js/login.js"></script>
</body>
</html>

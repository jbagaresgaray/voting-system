<!DOCTYPE html>
<html>

<head>
    <title>Sign in | Voting System</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta content='text/html;charset=utf-8' http-equiv='content-type'>
    <meta content='Flat administration template for Twitter Bootstrap. Twitter Bootstrap 3 template with Ruby on Rails support.' name='description'>
    <link href='../../meta_icons/favicon.ico' rel='shortcut icon' type='image/x-icon'>
    <link href='../../meta_icons/apple-touch-icon.png' rel='apple-touch-icon-precomposed'>
    <link href='../../meta_icons/apple-touch-icon-57x57.png' rel='apple-touch-icon-precomposed' sizes='57x57'>
    <link href='../../meta_icons/apple-touch-icon-72x72.png' rel='apple-touch-icon-precomposed' sizes='72x72'>
    <link href='../../meta_icons/apple-touch-icon-114x114.png' rel='apple-touch-icon-precomposed' sizes='114x114'>
    <link href='../../meta_icons/apple-touch-icon-144x144.png' rel='apple-touch-icon-precomposed' sizes='144x144'>
    <link href="../../css/bootstrap-3/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
    <link href="../../css/light-theme.css" media="all" id="color-settings-body-color" rel="stylesheet" type="text/css" />
    <link href="../../css/theme-colors.css" media="all" rel="stylesheet" type="text/css" />
    <link href="../../css/demo.css" media="all" rel="stylesheet" type="text/css" />

    <link rel="icon" type="image/png" href="../../img/logo.jpg" />
    <!--[if lt IE 9]>
      <script src="assets/javascripts/ie/html5shiv.js" type="text/javascript"></script>
      <script src="assets/javascripts/ie/respond.min.js" type="text/javascript"></script>
    <![endif]-->
</head>

<body class='contrast-green login contrast-background'>
    <div class='middle-container'>
        <div class='middle-row'>
            <div class='middle-wrapper'>
                <div class='login-container-header'>
                    <div class='container'>
                        <div class='row'>
                            <div class='col-sm-12'>
                                <div class='text-center'>
                                    <img src="../../img/logo.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='login-container'>
                    <div class='container'>
                        <div class='row'>
                            <div class='col-sm-4 col-sm-offset-4'>
                                <h1 class='text-center title'>Sign in</h1>
                                <form class='validate-form'>
                                    <div class='form-group'>
                                        <div class='controls with-icon-over-input'>
                                            <input value="" placeholder="Username" class="form-control" data-rule-required="true" name="username" id="username" type="text" />
                                            <span class="help-inline"></span>
                                            <i class='icon-user text-muted'></i>
                                        </div>
                                    </div>
                                    <div class='form-group'>
                                        <div class='controls with-icon-over-input'>
                                            <input value="" placeholder="Password" class="form-control" data-rule-required="true" name="password" id="password" type="password" />
                                            <span class="help-inline"></span>
                                            <i class='icon-lock text-muted'></i>
                                        </div>
                                    </div>
                                    <div class='form-group'>
                                        <div class='controls with-icon-over-input'>
                                            <input value="" placeholder="IP ADDRESS" class="form-control" data-rule-required="true" name="server" id="server" type="text" />
                                            <span class="help-inline"></span>
                                            <i class='icon-globe text-muted'></i>
                                        </div>
                                    </div>
                                    <a href="javascript:login();" class='btn btn-block'>Sign in</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='login-container-footer'>
                    <div class='container'>
                        <div class='row'>
                            <div class='col-sm-12'>
                                <div class='text-center'>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal modal-static fade" id="processing-modal" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="text-center">
                            <img src="../../img/loading.gif" class="icon" />
                            <h4>Processing...</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="errModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-header-danger">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">X</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">ERROR</h4>
                    </div>
                    <div class="modal-body">
                        <h1>WARNING: An error occured!</h1>
                        <blockquote>
                            <p id="error_message"></p>
                        </blockquote>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="../../lib/jquery/jquery-2.0.0.min.js" type="text/javascript"></script>
    <script src="../../lib/jquery/jquery.mobile.custom.min.js" type="text/javascript"></script>
    <script src="../../lib/jquery/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="../../lib/jquery/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../lib/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="../../lib/bootstrap-3/bootstrap.js" type="text/javascript"></script>
    <script src="../../lib/plugins/modernizr/modernizr.min.js" type="text/javascript"></script>
    <script src="../../lib/plugins/retina/retina.js" type="text/javascript"></script>
    <script src="../../lib/theme.js" type="text/javascript"></script>
    <script src="../../lib/demo.js" type="text/javascript"></script>
    <script src="../../lib/plugins/validate/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../lib/plugins/validate/additional-methods.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/main_login.js"></script>

    <!-- Mirrored from www.bublinastudio.com/flattybs3/sign_in.php by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 13 Dec 2013 05:10:09 GMT -->

</html>

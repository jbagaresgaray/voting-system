<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Voting System</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../../lib/bootstrap/css/bootstrap.css">

    <!-- Add custom CSS here -->
    <link rel="stylesheet" href="../../css/final.css" />
    <link rel="stylesheet" href="../../lib/font-awesome/css/font-awesome.min.css">
    <link href="../../lib/jasny-bootstrap/css/jasny-bootstrap.css" rel="stylesheet" type="text/css" />

    <link rel="icon" type="image/png" href="../../img/logo.jpg" />
</head>

<body>

    <!-- Sidebar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="main.php">
                <img src="../../img/logo.png" alt="Voting System" style="width:100px;">
            </a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                </li>
                <li><a href="student-list.php"><i class="fa fa-times"></i> Close</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
            <div class="jumbotron text-center">
              <h1>Import Student</h1>
              <p><small>Please follow the right format of the CSV file to be imported</small></p>
            </div>
            <p>
              <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                <span class="input-group-addon btn btn-default btn-file">
                  <span class="fileinput-new">Select file</span>
                  <span class="fileinput-exists">Change</span>
                  <input type="file" name="file_import" id="file_import"></span>
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
              </div>
              <div class="form-group">
                  <label class="control-label col-sm-2 col-xs-12">Election Term:</label>
                  <div class="col-sm-8 col-xs-12">
                      <select class="form-control" id="cboElectionTerm">
                      </select>
                  </div>
                  <button onclick="import_file()" type="button" class="col-sm-2 col-xs-12 btn btn-md btn-success">Import Student</button>
              </div>
            </p>
            </div>
        </div>
        <br><br><br>
        <div class="row">
            <div class="col-xs-12">
              <output id=list></output>
              <table class="table table-striped">
               <thead>
                    <tr role="row">
                        <th>
                            Student ID
                        </th>
                        <th>
                            First Name
                        </th>
                        <th>
                            Last Name
                        </th>
                        <th>
                            Middle Initial
                        </th>
                        <th>
                            Gender
                        </th>
                        <th>
                            Course ID
                        </th>
                    </tr>
                </thead>
              </table>
              <table class="table table-striped" id="CSVTable">
              <br>
            </div>
        </div>
    </div>
    <!-- /#page-wrapper -->

    <div class="modal modal-static fade" id="processing-modal" role="dialog" aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="text-center">
                            <img src="../../img/loading.gif" class="icon" />
                            <h4>Processing...</h4>
                        </div>
                        <span class="help-inline">Note: Importing data may take some time.. Please wait..</span>
                    </div>
                </div>
            </div>
        </div>


    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="../../lib/jquery/jquery-2.0.0.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap/js/bootstrap.js"></script>
    <script src="../../lib/jasny-bootstrap/js/jasny-bootstrap.js" type="text/javascript" /></script>
    <script type="text/javascript" src="../../lib/csvtotable/js/jquery.csvToTable.js"></script>
    <script type="text/javascript" src="../../lib/jquery.csv-0.71.min.js"></script>
    <script type="text/javascript" src="../../js/student-import.js"></script>
    <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
</body>

</html>

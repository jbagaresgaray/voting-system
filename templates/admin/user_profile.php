<!DOCTYPE html>
<html>

<!-- Mirrored from www.bublinastudio.com/flattybs3/user_profile.php by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 13 Dec 2013 05:06:55 GMT -->

<head>
    <title>User profile | Voting System</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta content='text/html;charset=utf-8' http-equiv='content-type'>
    <meta content='Flat administration template for Twitter Bootstrap. Twitter Bootstrap 3 template with Ruby on Rails support.' name='description'>
    <link href='../../meta_icons/favicon.ico' rel='shortcut icon' type='image/x-icon'>
    <link href='../../meta_icons/apple-touch-icon.png' rel='apple-touch-icon-precomposed'>
    <link href='../../meta_icons/apple-touch-icon-57x57.png' rel='apple-touch-icon-precomposed' sizes='57x57'>
    <link href='../../meta_icons/apple-touch-icon-72x72.png' rel='apple-touch-icon-precomposed' sizes='72x72'>
    <link href='../../meta_icons/apple-touch-icon-114x114.png' rel='apple-touch-icon-precomposed' sizes='114x114'>
    <link href='../../meta_icons/apple-touch-icon-144x144.png' rel='apple-touch-icon-precomposed' sizes='144x144'>
    <link href="../../css/bootstrap-3/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
    <link href="../../css/light-theme.css" media="all" id="color-settings-body-color" rel="stylesheet" type="text/css" />
    <link href="../../css/theme-colors.css" media="all" rel="stylesheet" type="text/css" />
    <link href="../../css/demo.css" media="all" rel="stylesheet" type="text/css" />

    <link rel="icon" type="image/png" href="../../img/logo.jpg" />
    <!--[if lt IE 9]>
      <script src="assets/javascripts/ie/html5shiv.js" type="text/javascript"></script>
      <script src="assets/javascripts/ie/respond.min.js" type="text/javascript"></script>
    <![endif]-->
</head>

<body class='contrast-green without-footer'>
    <header>
        <nav class='navbar navbar-default'>
            <a class='navbar-brand' href='main.php'>
                <img src="../../img/logo.png" width="130">
            </a>
            <a class='toggle-nav btn pull-left' href='#'>
                <i class='icon-reorder'></i>
            </a>
            <ul class='nav'>
                <li class='dropdown dark user-menu'>
                    <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                        <span id="current_user" class='user-name'></span>
                        <b class='caret'></b>
                    </a>
                    <ul class='dropdown-menu'>
                        <li>
                            <a href='user_profile.php'>
                                <i class='icon-user'></i>
                                Profile
                            </a>
                        </li>
                        <li class='divider'></li>
                        <li>
                            <a href='about.php'>
                                <i class='icon-info-sign'></i>
                                About
                            </a>
                        </li>
                        <li>
                            <a href='index.php'>
                                <i class='icon-signout'></i>
                                Sign out
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>
    <div id='wrapper'>
        <div id='main-nav-bg'></div>
        <nav id='main-nav'>
            <div class='navigation'>
                <div class='search'>
                    <form action='' method='get'>
                        <div class='search-wrapper'>
                            <input value="" class="search-query form-control" placeholder="Search..." autocomplete="off" name="q" type="text" />
                            <button class='btn btn-link icon-search' name='button' type='submit'></button>
                        </div>
                    </form>
                </div>
                <ul class='nav nav-stacked' id="LIST">
                    <li class='hidden'>
                        <a href='main.php' class="in">
                            <i class='icon-dashboard'></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='user-account-list.php'><i class='icon-user'></i>
                          <span>Users Account</span>
                        </a>
                    </li>
                    <!-- <li class='hidden'>
                        <a href='college-list.php'>
                            <i class='icon-group'></i>
                            <span>College</span>
                        </a>
                    </li> -->
                    <li class='hidden'>
                        <a href='student-list.php'>
                            <i class='icon-male'></i>
                            <span>Students</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='partylist-list.php'>
                            <i class='icon-star'></i>
                            <span>Party List</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='candidates-list.php'>
                            <i class='icon-group'></i>
                            <span>Candidates</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='position-list.php'>
                            <i class='icon-reorder'></i>
                            <span>Electoral Position</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='academic-program-list.php'>
                            <i class='icon-certificate'></i>
                            <span>Academic Program</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='config-list.php'>
                            <i class='icon-cogs'></i>
                            <span>Election Configuration</span>
                        </a>
                    </li>
            </div>
        </nav>
        <section id='content'>
            <div class='container'>
                <div class='row' id='content-wrapper'>
                    <div class='col-xs-12'>
                        <div class='row'>
                            <div class='col-sm-12'>
                                <div class='page-header'>
                                    <h1 class='pull-left'>
                                        <i class='icon-user'></i>
                                        <span>User profile</span>
                                    </h1>
                                </div>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-sm-3 col-lg-2'>
                                <div class='box'>
                                    <div class='box-content'>
                                        <img class="img-responsive" src="../../img/photo.jpg" />
                                    </div>
                                </div>
                            </div>
                            <div class='col-sm-9 col-lg-10'>
                                <div class='box'>
                                    <div class='box-content box-double-padding'>
                                        <form class='form' style='margin-bottom: 0;'>
                                            <fieldset>
                                                <div class='col-sm-4'>
                                                    <div class='lead'>
                                                        <i class='icon-github text-contrast'></i>
                                                        Account Information
                                                    </div>
                                                </div>
                                                <div class='col-sm-7 col-sm-offset-1'>
                                                    <div class='form-group'>
                                                        <label>Username</label>
                                                        <input class='form-control' id='txtusername' name="txtusername" placeholder='Username' type='text'>
                                                    </div>
                                                    <div class='form-group'>
                                                        <label>Password</label>
                                                        <input class='form-control' id='txtPassword' name="txtPassword" placeholder='Password' type='Password'>
                                                    </div>
                                                    <hr class='hr-normal'>
                                                </div>
                                            </fieldset>
                                            <hr class='hr-normal'>
                                            <fieldset>
                                                <div class='col-sm-4'>
                                                    <div class='lead'>
                                                        <i class='icon-user text-contrast'></i>
                                                        Personal Information
                                                    </div>
                                                </div>
                                                <div class='col-sm-7 col-sm-offset-1'>
                                                    <div class='form-group'>
                                                        <label>First name</label>
                                                        <input class='form-control' id='txtfirstname' name="txtfirstname" placeholder='First name' type='text'>
                                                    </div>
                                                    <div class='form-group'>
                                                        <label>Last name</label>
                                                        <input class='form-control' id='txtlastname' name="txtlastname" placeholder='Last name' type='text'>
                                                    </div>
                                                   <div class='form-group'>
                                                        <label>Middle name</label>
                                                        <input class='form-control' id='txtmiddlename' name="txtmiddlename" placeholder='Middle name' type='text'>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <!-- <div class='form-actions form-actions-padding' style='margin-bottom: 0;'>
                                                <div class='text-right'>
                                                    <div class='btn btn-primary btn-lg'>
                                                        <i class='icon-save'></i>
                                                        Save
                                                    </div>
                                                </div>
                                            </div> -->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer id='footer'>
                    <div class='footer-wrapper'>
                        <div class='row'>
                            <div class='col-sm-6 text'>
                                Copyright © 2013 Your Project Name
                            </div>
                            <div class='col-sm-6 buttons'>
                                <a class="btn btn-link" href="http://www.bublinastudio.com/flatty">Preview</a>
                                <a class="btn btn-link" href="https://wrapbootstrap.com/theme/flatty-flat-administration-template-WB0P6NR1N">Purchase</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </section>
    </div>

    <script src="../../lib/jquery/jquery-2.0.0.min.js" type="text/javascript"></script>
    <script src="../../lib/jquery/jquery.mobile.custom.min.js" type="text/javascript"></script>
    <script src="../../lib/jquery/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="../../lib/jquery/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../lib/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="../../lib/bootstrap-3/bootstrap.js" type="text/javascript"></script>
    <script src="../../lib/plugins/modernizr/modernizr.min.js" type="text/javascript"></script>
    <script src="../../lib/plugins/retina/retina.js" type="text/javascript"></script>
    <script src="../../lib/theme.js" type="text/javascript"></script>
    <script src="../../lib/demo.js" type="text/javascript"></script>
    <script src="../../lib/plugins/validate/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../lib/plugins/validate/additional-methods.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/profile.js"></script>
</body>

</html>

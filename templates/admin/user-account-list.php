<!DOCTYPE html>
<html>

<head>
    <title>Users Account | Voting System</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta content='text/html;charset=utf-8' http-equiv='content-type'>
    <meta content='Flat administration template for Twitter Bootstrap. Twitter Bootstrap 3 template with Ruby on Rails support.' name='description'>
    <link href='../../meta_icons/favicon.ico' rel='shortcut icon' type='image/x-icon'>
    <link href='../../meta_icons/apple-touch-icon.png' rel='apple-touch-icon-precomposed'>
    <link href='../../meta_icons/apple-touch-icon-57x57.png' rel='apple-touch-icon-precomposed' sizes='57x57'>
    <link href='../../meta_icons/apple-touch-icon-72x72.png' rel='apple-touch-icon-precomposed' sizes='72x72'>
    <link href='../../meta_icons/apple-touch-icon-114x114.png' rel='apple-touch-icon-precomposed' sizes='114x114'>
    <link href='../../meta_icons/apple-touch-icon-144x144.png' rel='apple-touch-icon-precomposed' sizes='144x144'>
    <link href="../../css/bootstrap-3/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
    <link href="../../css/light-theme.css" media="all" id="color-settings-body-color" rel="stylesheet" type="text/css" />
    <link href="../../css/theme-colors.css" media="all" rel="stylesheet" type="text/css" />
    <link href="../../css/demo.css" media="all" rel="stylesheet" type="text/css" />

    <link rel="icon" type="image/png" href="../../img/logo.jpg" />
    <!--[if lt IE 9]>
      <script src="assets/javascripts/ie/html5shiv.js" type="text/javascript"></script>
      <script src="assets/javascripts/ie/respond.min.js" type="text/javascript"></script>
    <![endif]-->
</head>

<body class='contrast-green without-footer'>
    <header>
        <nav class='navbar navbar-default'>
            <a class='navbar-brand' href='main.php'>
                <img src="../../img/logo.png" width="130">
            </a>
            <a class='toggle-nav btn pull-left' href='#'>
                <i class='icon-reorder'></i>
            </a>
            <ul class='nav'>
                <li class='dropdown dark user-menu'>
                    <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                        <span id="current_user" class='user-name'></span>
                        <b class='caret'></b>
                    </a>
                    <ul class='dropdown-menu'>
                        <li>
                            <a href='user_profile.php'>
                                <i class='icon-user'></i>
                                Profile
                            </a>
                        </li>
                        <li class='divider'></li>
                        <li>
                            <a href='about.php'>
                                <i class='icon-info-sign'></i>
                                About
                            </a>
                        </li>
                        <li>
                            <a href='javascript:logout();'>
                                <i class='icon-signout'></i>
                                Sign out
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>
    <div id='wrapper'>
        <div id='main-nav-bg'></div>
        <nav id='main-nav'>
            <div class='navigation'>
                <div class='search'>
                    <form action='http://www.bublinastudio.com/flattybs3/search_results.php' method='get'>
                        <div class='search-wrapper'>
                            <input value="" class="search-query form-control" placeholder="Search..." autocomplete="off" name="q" type="text" />
                            <button class='btn btn-link icon-search' name='button' type='submit'></button>
                        </div>
                    </form>
                </div>
                <ul class='nav nav-stacked' id="LIST">
                    <li class='hidden'>
                        <a href='main.php'>
                            <i class='icon-dashboard'></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='user-account-list.php' class="in"><i class='icon-user'></i>
                          <span>Users Account</span>
                        </a>
                    </li>
                    <!-- <li class='hidden'>
                        <a href='college-list.php'>
                            <i class='icon-group'></i>
                            <span>College</span>
                        </a>
                    </li> -->
                    <li class='hidden'>
                        <a href='student-list.php'>
                            <i class='icon-male'></i>
                            <span>Students</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='partylist-list.php'>
                            <i class='icon-star'></i>
                            <span>Party List</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='candidates-list.php'>
                            <i class='icon-group'></i>
                            <span>Candidates</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='position-list.php'>
                            <i class='icon-reorder'></i>
                            <span>Electoral Position</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='academic-program-list.php'>
                            <i class='icon-certificate'></i>
                            <span>Academic Program</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='config-list.php'>
                            <i class='icon-cogs'></i>
                            <span>Election Configuration</span>
                        </a>
                    </li>
            </div>
        </nav>
        <section id="content">
            <div class="container">
                <div class="row" id="content-wrapper">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header">
                                    <h1 class="pull-left">
                                        <i class="icon-user"></i>
                                        <span>User Accounts</span>
                                    </h1>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="box bordered-box green-border" style="margin-bottom:0;">
                                    <div class="box-header green-background">
                                        <div class="title">List of Accounts</div>
                                    </div>
                                    <div class="box-content box-no-padding">
                                        <div class="responsive-table">
                                            <div class="scrollable-area">
                                                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline" role="grid">
                                                    <div class="row datatables-top">
                                                        <div class="col-sm-12 text-right">
                                                            <div class="dataTables_filter" id="DataTables_Table_0_filter">
                                                                <label>Search:
                                                                    <input id="searchValue" name="searchValue" type="text" aria-controls="DataTables_Table_0" style="width: 200px;" class="form-control input-sm" placeholder="Search... ">
                                                                    <!-- <input onclick="javascript:search_user()" class="btn btn-sm btn-success" value="search" type="button"> -->
                                                                    <button onclick="javascript:commandtoClearUserAccount()" data-toggle="modal" data-target="#usersModal"  class="btn btn-sm btn-primary">Add User</button>
                                                                    <a href="account-print.php" class="btn btn-sm btn-primary"><i class="icon-print"></i> Print</a>
                                                                    <button onclick="javascript:refresh()" class="btn btn-sm btn-primary" type="button"><i class="icon-refresh"></i> Refresh</button>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <table class="data-table table table-bordered table-striped dataTable" style="margin-bottom:0;" id="tbl_user_account">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting">
                                                                    Name
                                                                </th>
                                                                <th class="sorting">
                                                                    Username
                                                                </th>
                                                                <th class="sorting">
                                                                    User Group
                                                                </th>
                                                                <th role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 128px;"></th>
                                                            </tr>
                                                        </thead>

                                                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                                                        </tbody>
                                                    </table>
                                                    <div class="row datatables-bottom">
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_info" id="DataTables_Table_0_info">Showing 1 to 10 of 20 entries</div>
                                                        </div>
                                                        <div class="col-sm-6 text-right">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                            <!-- <ul class="pagination pagination-sm">
                                                                    <li class="prev disabled"><a href="#">← Previous</a>
                                                                    </li>
                                                                    <li class="active"><a href="#">1</a>
                                                                    </li>
                                                                    <li><a href="#">2</a>
                                                                    </li>
                                                                    <li class="next"><a href="#">Next → </a>
                                                                    </li>
                                                                </ul> -->
                                                                <div id="Userpagination" cellspacing="0"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </br>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="box bordered-box green-border" style="margin-bottom:0;">
                                    <div class="box-header green-background">
                                        <div class="title">User Groups</div>
                                    </div>
                                    <div class="box-content box-no-padding">
                                        <div class="responsive-table">
                                            <div class="scrollable-area">
                                                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline" role="grid">
                                                    <div class="row datatables-top">
                                                        <div class="col-sm-12 text-right">
                                                            <div class="dataTables_filter" id="DataTables_Table_0_filter">
                                                                <label>Search:
                                                                    <input id="searchValue1" name="searchValue1" type="text" aria-controls="DataTables_Table_0" style="width: 200px;" class="form-control input-sm" placeholder="Search... ">
                                                                    <!-- <input onclick="javascript:search_user_group()" class="btn btn-sm btn-success" value="search" type="button"> -->
                                                                    <button onclick="javascript:commandToClearUserGroup()" data-toggle="modal" data-target="#userGroupModal" class="btn btn-sm btn-primary">Add User Group</button>
                                                                     <a href="UserGroup-print.php" class="btn btn-sm btn-primary"><i class="icon-print"></i> Print</a>
                                                                    <button onclick="javascript:refresh()" class="btn btn-sm btn-primary" type="button"><i class="icon-refresh"></i> Refresh</button>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <table class="data-table table table-bordered table-striped dataTable" style="margin-bottom:0;" id="tbl_user_group" aria-describedby="DataTables_Table_0_info">
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="sorting">
                                                                    Group Name
                                                                </th>
                                                                <th class="sorting">
                                                                    Description
                                                                </th>
                                                                <th role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 128px;"></th>
                                                            </tr>
                                                        </thead>

                                                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                                                        </tbody>
                                                    </table>
                                                    <div class="row datatables-bottom">
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_info" id="DataTables_Table_0_info">Showing 1 to 10 of 20 entries</div>
                                                        </div>
                                                        <div class="col-sm-6 text-right">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                            <!-- <ul class="pagination pagination-sm">
                                                                    <li class="prev disabled"><a href="#">← Previous</a>
                                                                    </li>
                                                                    <li class="active"><a href="#">1</a>
                                                                    </li>
                                                                    <li><a href="#">2</a>
                                                                    </li>
                                                                    <li class="next"><a href="#">Next → </a>
                                                                    </li>
                                                                </ul> -->
                                                               <div id="Grouppagination" cellspacing="0"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="modal modal-static fade" id="processing-modal" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="text-center">
                            <img src="../../img/loading.gif" class="icon" />
                            <h4>Processing...</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="usersModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="box">
                            <div class="box-header blue-background">
                                <div class="title">Users Information</div>
                            </div>
                            <div class="box-content">
                                <form class="form form-horizontal" style="margin-bottom: 0;" method="post" action="#" accept-charset="UTF-8">
                                    <input id="authenticity_token" name="authenticity_token" type="hidden">
                                    <div class="form-group">
                                        <label class="control-label col-sm-3 col-xs-12">User Group:</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select class="form-control" id="cboUserGroup">
                                            </select>
                                             <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3 col-xs-12">First Name :</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <input id="txtFirstName" name="txtFirstName" class="form-control" placeholder="First Name" type="text">
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3 col-xs-12">Middle Name :</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <input id="txtMiddleName" name="txtMiddleName" class="form-control" placeholder="Middle Name" type="text">
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3 col-xs-12">Last Name :</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <input id="txtLastName" name="txtLastName" class="form-control" placeholder="Last Name" type="text">
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3 col-xs-12">Username :</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <input id="txtUsername" name="txtUsername" class="form-control" placeholder="Username" type="text">
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3 col-xs-12">Password :</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <input id="txtPassword" name="txtPassword" class="form-control" placeholder="Password" type="Password">
                                            <p style="font-size: 10px;color: #000;">Password should contain minimum of 6 characters</p>
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3 col-xs-12">Confirm Password :</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <input id="txtPassword2" name="txtPassword2" class="form-control" placeholder="Confirm Password" type="Password">
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <a href="javascript:saveUserAccount();" type="button" id="btn-save" class="btn btn-primary"><i class="icon-save"></i> Save changes</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="userGroupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="box">
                            <div class="box-header blue-background">
                                <div class="title">Users Group</div>
                            </div>
                            <div class="box-content">
                                <div class="form-group">
                                    <input id="authenticationGroupID" name="authenticationGroupID" type="hidden">
                                    <p>
                                        <strong>Group Name</strong>
                                    </p>
                                    <input id="txtGroupName" name="txtGroupName" class="form-control mention" placeholder="Group Name" style="margin-bottom: 0;" type="text">
                                    <span class="help-inline"></span>
                                    <p>
                                        <strong>Description</strong>
                                    </p>
                                    <input id="txtDesc" name="txtDesc" class="form-control mention" placeholder="Description" style="margin-bottom: 0;" type="text">
                                    <span class="help-inline"></span>
                                    <p>
                                        <strong>Privileges</strong>
                                    </p>
                                    <div class="checkbox">
                                        <label>
                                          <input id="chckDashBoard" name="chckDashBoard" value="1" type="checkbox"> <strong>DashBoard</strong>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input id="chckUserAcc" name="chckUserAcc" value="1" type="checkbox"> <strong>Users Account</strong>
                                        </label>
                                    </div>
                                   <!--  <div class="checkbox">
                                        <label>
                                          <input id="chckCollege" name="chckCollege" value="1" type="checkbox"> <strong>College</strong>
                                        </label>
                                    </div> -->
                                    <div class="checkbox">
                                        <label>
                                          <input id="chckStudent" name="chckStudent" value="1" type="checkbox"> <strong>Student</strong>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input id="chckParty" name="chckParty" value="1" type="checkbox"> <strong>Party List</strong>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input id="chckCandidates" name="chckCandidates" value="1" type="checkbox"> <strong>Candidates</strong>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input id="chckElectoral" name="chckElectoral" value="1" type="checkbox"> <strong>Electoral Position</strong>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input id="chckAcademic" name="chckAcademic" value="1" type="checkbox"> <strong>Academic Program</strong>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input id="chckElection" name="chckElection" value="1" type="checkbox"> <strong>Election Configuration</strong>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <a href="javascript:saveUserGroup();" type="button" id="btn-saveUserGroup" class="btn btn-primary"><i class="icon-save"></i> Save changes</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="errModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-header-danger">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">X</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">ERROR</h4>
                    </div>
                    <div class="modal-body">
                        <h1>WARNING: An error occured!</h1>
                        <blockquote>
                            <p id="error_message"></p>
                        </blockquote>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <script src="../../lib/jquery/jquery-2.0.0.min.js" type="text/javascript"></script>
    <script src="../../lib/jquery/jquery.mobile.custom.min.js" type="text/javascript"></script>
    <script src="../../lib/jquery/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="../../lib/jquery/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../lib/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="../../lib/bootstrap-3/bootstrap.js" type="text/javascript"></script>
    <script src="../../lib/plugins/modernizr/modernizr.min.js" type="text/javascript"></script>
    <script src="../../lib/plugins/retina/retina.js" type="text/javascript"></script>
    <script src="../../lib/theme.js" type="text/javascript"></script>
    <script src="../../lib/demo.js" type="text/javascript"></script>
    <script src="../../lib/plugins/validate/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../lib/plugins/validate/additional-methods.js" type="text/javascript"></script>

    <script src="../../lib/tablesorter/jquery.tablesorter.js" type="text/javascript" ></script>
    <script src="../../lib/tablesorter/tables.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/users.js"></script>


</body>

</html>

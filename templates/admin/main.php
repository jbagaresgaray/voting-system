<!DOCTYPE html>
<html>

<head>
    <title>Dashboard | Voting System</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta content='text/html;charset=utf-8' http-equiv='content-type'>
    <meta content='Flat administration template for Twitter Bootstrap. Twitter Bootstrap 3 template with Ruby on Rails support.' name='description'>
    <link href='../../meta_icons/favicon.ico' rel='shortcut icon' type='image/x-icon'>
    <link href='../../meta_icons/apple-touch-icon.png' rel='apple-touch-icon-precomposed'>
    <link href='../../meta_icons/apple-touch-icon-57x57.png' rel='apple-touch-icon-precomposed' sizes='57x57'>
    <link href='../../meta_icons/apple-touch-icon-72x72.png' rel='apple-touch-icon-precomposed' sizes='72x72'>
    <link href='../../meta_icons/apple-touch-icon-114x114.png' rel='apple-touch-icon-precomposed' sizes='114x114'>
    <link href='../../meta_icons/apple-touch-icon-144x144.png' rel='apple-touch-icon-precomposed' sizes='144x144'>
    <link href="../../css/bootstrap-3/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
    <link href="../../css/light-theme.css" media="all" id="color-settings-body-color" rel="stylesheet" type="text/css" />
    <link href="../../css/theme-colors.css" media="all" rel="stylesheet" type="text/css" />
    <link href="../../css/demo.css" media="all" rel="stylesheet" type="text/css" />

    <link rel="icon" type="image/png" href="../../img/logo.jpg" />
    <!--[if lt IE 9]>
      <script src="assets/javascripts/ie/html5shiv.js" type="text/javascript"></script>
      <script src="assets/javascripts/ie/respond.min.js" type="text/javascript"></script>
    <![endif]-->
</head>

<body class='contrast-green without-footer'>
    <header>
        <nav class='navbar navbar-default'>
            <a class='navbar-brand' href='main.php'>
                     <img src="../../img/logo.png" width="130">
            <a class='toggle-nav btn pull-left' href='#'>
                <i class='icon-reorder'></i>
            </a>
            <ul class='nav'>
                <li class='dropdown dark user-menu'>
                    <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                        <span id="current_user" class='user-name'></span>
                        <b class='caret'></b>
                    </a>
                    <ul class='dropdown-menu'>
                        <li>
                            <a href='user_profile.php'>
                                <i class='icon-user'></i>
                                Profile
                            </a>
                        </li>
                        <li class='divider'></li>
                        <li>
                            <a href='about.php'>
                                <i class='icon-info-sign'></i>
                                About
                            </a>
                        </li>
                        <li>
                            <a href='javascript:logout();'>
                                <i class='icon-signout'></i>
                                Sign out
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>
    <div id='wrapper'>
        <div id='main-nav-bg'></div>
        <nav id='main-nav'>
            <div class='navigation'>
                <ul class='nav nav-stacked' id="LIST">
                    <li class='hidden'>
                        <a href='main.php' class="in">
                            <i class='icon-dashboard'></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='user-account-list.php'><i class='icon-user'></i>
                          <span>Users Account</span>
                        </a>
                    </li>
                    <!-- <li class='hidden'>
                        <a href='college-list.php'>
                            <i class='icon-group'></i>
                            <span>College</span>
                        </a>
                    </li> -->
                    <li class='hidden'>
                        <a href='student-list.php'>
                            <i class='icon-male'></i>
                            <span>Students</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='partylist-list.php'>
                            <i class='icon-star'></i>
                            <span>Party List</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='candidates-list.php'>
                            <i class='icon-group'></i>
                            <span>Candidates</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='position-list.php'>
                            <i class='icon-reorder'></i>
                            <span>Electoral Position</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='academic-program-list.php'>
                            <i class='icon-certificate'></i>
                            <span>Academic Program</span>
                        </a>
                    </li>
                    <li class='hidden'>
                        <a href='config-list.php'>
                            <i class='icon-cogs'></i>
                            <span>Election Configuration</span>
                        </a>
                    </li>

            </div>
        </nav>
        <section id="content">
            <div class="container">
                <div class="row" id="content-wrapper">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header">
                                    <h1 class="pull-left">
                                        <i class='icon-dashboard'></i>
                                        <span>Dashboard</span>
                                    </h1>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <i class="icon-thumbs-up icon-5x"></i>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <p class="announcement-heading" id="announcement-voted-heading">0</p>
                                                <p class="announcement-text">Voted!</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <i class="icon-thumbs-down icon-5x"></i>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <p class="announcement-heading" id="announcement-notvoted-heading">0</p>
                                                <p class="announcement-text">Not Voted</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <i class="icon-group icon-5x"></i>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <p class="announcement-heading" id="announcement-voters-heading">0</p>
                                                <p class="announcement-text">Total Voters</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="Configure" class="row hidden">
                            <div class="col-sm-12">
                                <div class="box">
                                    <div class="box-header green-background">
                                        <div class="title">Election Configuration</div>
                                    </div>
                                    <div class="box-content">
                                        <div class="row">
                                            <div class="col-lg-9">
                                                <h3>Current Election Term:
                                                    <span class="active_term" id="lblElectionTerm"></span>
                                                </h3>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="text-right">
                                                    <div class="dataTables_filter">
                                                        <a data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-primary"><i class="icon-check"></i> Configure</a>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="box">
                            <div class="box-header blue-background">
                                <div class="title">Election Configuration</div>
                                <div class="actions">
                                    <a class="btn box-remove btn-xs btn-link" href="#">
                                    </a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form class="form form-horizontal" style="margin-bottom: 0;" method="post" action="#" accept-charset="UTF-8">
                                    <input name="authenticity_token" type="hidden">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 col-xs-12">Election Term:</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select class="form-control" id="cboElectionTerm">
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <a href="javascript:activate_term();" type="button" id="btn-save" class="btn btn-primary"><i class="icon-check"></i> Activate</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>

    <script src="../../lib/jquery/jquery-2.0.0.min.js" type="text/javascript"></script>
    <script src="../../lib/jquery/jquery.mobile.custom.min.js" type="text/javascript"></script>
    <script src="../../lib/jquery/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="../../lib/jquery/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../lib/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="../../lib/bootstrap-3/bootstrap.js" type="text/javascript"></script>
    <script src="../../lib/plugins/modernizr/modernizr.min.js" type="text/javascript"></script>
    <script src="../../lib/plugins/retina/retina.js" type="text/javascript"></script>
    <script src="../../lib/theme.js" type="text/javascript"></script>
    <script src="../../lib/demo.js" type="text/javascript"></script>
    <script src="../../lib/plugins/validate/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../lib/plugins/validate/additional-methods.js" type="text/javascript"></script>

    <script type="text/javascript" src="../../js/dashboard.js"></script>
</body>

</html>

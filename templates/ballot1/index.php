<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, width=500px">
    <title>Voting System</title>
    <link rel="stylesheet" href="../../lib/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../../css/signin.css" />

</head>

<body>

    <div class="container">
        <div class="row colored">
            <div id="contentdiv" class="contcustom">
                <span class="fa fa-thumbs-o-up bigicon"></span>
                <h2>Login</h2>
                <form role="form">
                    <input id="username" type="text" placeholder="username" onkeypress="check_values();">
                    <input id="password" type="password" placeholder="password" onkeypress="check_values();">
                    <input id="server" type="text" placeholder="IP ADDRESS">
                    <a href="javascript:login();" id="button1" class="btn btn-default wide hidden">
                        <span class="fa fa-check med"></span>
                    </a>
                    <span id="lock1" class="fa fa-lock medhidden redborder"></span>
            </div>
        </div>
    </div>

    <div class="modal fade" id="errModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-danger">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">X</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">ERROR</h4>
                </div>
                <div class="modal-body">
                    <h1>WARNING: An error occured!</h1>
                    <blockquote>
                        <p id="error_message"></p>
                    </blockquote>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-static fade" id="processing-modal" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-center">
                        <img src="../../img/loading.gif" class="icon" />
                        <h4>Processing...
                            <button type="button" class="close" style="float: none;" data-dismiss="modal" aria-hidden="true">X</button>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- /container -->

    <script type="text/javascript" src="../../lib/jquery/jquery-2.0.0.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="../../js/login.js"></script>
    <script type="text/javascript">
    function check_values() {
        if ($("#username").val().length != 0 && $("#password").val().length != 0) {
            $("#button1").removeClass("hidden").animate({
                left: '250px'
            });;
            $("#lock1").addClass("hidden").animate({
                left: '250px'
            });;
        }
    }
    </script>
</body>

</html>

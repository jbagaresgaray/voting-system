<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, width=500px">
    <title>Voting System</title>
    <link rel="stylesheet" href="../../lib/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../../css/ballot.css" />

</head>

<body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Voting System</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Summary</a>
                    </li>
                    <li><a href="javascript:logout();">Logout</a>
                    </li>
                    <li><a href="#">Help</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li class="nav-profile">
                        <div class="image">
                            <a href="javascript:;">
                                <img src="../../img/photo.jpg" alt="" />
                            </a>
                        </div>
                        <div class="info">
                            <span id="current_user"></span>
                            <small id="current_user_course"></small>
                        </div>
                    </li>
                </ul>
                <ul class="nav nav-sidebar">
                    <li class="nav-header">Candidate Positions</li>
                </ul>
                <ul class="nav nav-sidebar" id="lstPositions">
                </ul>
            </div>
            <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2 main">

                <h1 class="page-header" id="lblPosition"></h1>
                <div class="row placeholders" id="candidate_list">
                </div>
            </div>

            <div class="col-sm-4 col-md-2 sidebar2" data-scrollbar="true" data-height="100%" style="overflow: auto; width: 265px; height: 100%;">
                <ul class="nav nav-sidebar">
                    <li class="nav-header">Selected Candidate Positions</li>
                </ul>
                <ul class="nav nav-sidebar" id="my_ballot">
                </ul>
            </div>
        </div>
    </div>

    <div class="navbar navbar-inverse navbar-fixed-bottom">
        <div class="container">
            <div class="col-md-6">
                <center>
                    <a href="#" class="navbar-btn btn-lg btn-success btn" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-thumbs-o-up"></i> SUBMIT BALLOT
                    </a>
                </center>
            </div>
            <div class="col-md-6">
                <center>
                    <a class="navbar-btn btn-lg btn-danger btn" data-toggle="modal" data-target="#removeModal">
                        <i class="fa fa-times"></i> CLEAR BALLOT
                    </a>
                </center>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">X</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Submit Ballot</h4>
                </div>
                <div class="modal-body">
                    <h1>Are you sure to submit this ballot?</h1>
                    <blockquote>
                        <p>Thank you for your participation.</p>
                    </blockquote>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a href="javascript:submit_ballot();" class="btn btn-primary">Submit</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">X</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">CONGRATULATIONS</h4>
                </div>
                <div class="modal-body">
                    <h1>THANK YOU !!!</h1>
                    <blockquote>
                        <p id="success_message"></p>
                    </blockquote>
                </div>
                <div class="modal-footer">
                    <a href="javascript:success_vote();" class="btn btn-default">OK</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="removeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-danger">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">X</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Clear Ballot</h4>
                </div>
                <div class="modal-body">
                    <h1>Are you sure to Clear your ballot?</h1>
                    <blockquote>
                        <p>Clearing the list of your ballot will restart you to choose again.</p>
                    </blockquote>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                    <a href="javascript:clear_ballot();" class="btn btn-primary">YES</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="errModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-danger">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">X</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">ERROR</h4>
                </div>
                <div class="modal-body">
                    <h1>WARNING: An error occured!</h1>
                    <blockquote>
                        <p id="error_message"></p>
                    </blockquote>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-static fade" id="processing-modal" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-center">
                        <img src="../../img/loading.gif" class="icon" />
                        <h4>Processing...
                            <button type="button" class="close" style="float: none;" data-dismiss="modal" aria-hidden="true">X</button>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="../../lib/jquery/jquery-2.0.0.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="../../js/ballot_old.js"></script>
</body>

</html>